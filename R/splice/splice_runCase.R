# Libraries ----

source(file.path(getwd(),'R','splice','splice_edaFunctions.R'))
source(file.path(getwd(),'R','splice','splice_utils.R'))

# Directories ----

survOutDir = dir_fun(analysis_outDir %F% "survRes")
splVarPlotDir = dir_fun(analysis_outDir %F% "splVarPlots")
evidenceDiagPlots = dir_fun(analysis_outDir %F% "evDiagPlots")
corrOutPath = dir_fun(analysis_outDir %F% "corrPlots")

# 1) EVIDENCE DIAGNOSTICS ----

# Diagnostic plots on the evidence
evTypes = c("PSI","Counts")
for (evt in evTypes) {
  if (eval(parse(text=paste0("runBranch_",evt)))) {
    files <- list.files(path = evidencePath, full.names = T) %>% str_subset(evt)
    
    lapply(files, function(file) {
      evT = readRDS(file)
      name = str_extract(file,"[^/]+(?=.gzip$)")
      exportPath = dir_fun(file.path(evidenceDiagPlots,evt))
      # Plot without limits
        plotEvidenceDist(evT, exportPath, name, evLimits = NULL,  equality=c(F,F), isoPerEvent = "both", ext = imgExt)
      # In case of PSI plot with limits 0-1
      if (evt=="PSI") {
        plotEvidenceDist(evT, exportPath, name, evLimits = c(0,1),equality=c(F,F), isoPerEvent = "both", ext = imgExt)
      }
    })
  }
}





# 2) SURVIVAL ANALYSIS ----

# ********** PSI based analysis *************

if (runBranch_PSI) {
  # Setup parameters
  minThreshes = c(0,0.1,0.2)
  for (mt in minThreshes) {
    for (genFilter in c(TRUE,FALSE)) {
      expName = paste0("PSI_pres-filt_",mt,"gen0Filt_",genFilter)
      covariateParams = list(
        evidType = "PSI",
        sumSplVar = aggregateSplVarByType,
        sumSplVarPars = list(
          evValueMinFilter = mt,
          byPresence = TRUE,
          sumSplVarFunc = sumSplVar_divGeneExp,
          sumSplVarFuncPars = list(
            "genNormByPt" = genNormByPt,
            "aggFuncWithinGenes" = sum,
            "aggFuncAcrossGenes" = sum,
            "filter0exp" = genFilter
          )
        )
      )
      otherParams = list(
        min_prop = c(0.1,0.2)
      )
      # Run the analysis
      runSplSurvivalAnal(expName, survOutDir, covariateParams, otherParams, pts, samples, evidenceSplitPath, events, spliceVariants)
    }
  }
}

# ********** Count based analysis *************
if (runBranch_Counts) {
  
  # Set up parameters
  expName = "Counts-defaultSum"
  covariateParams = list(
    evidType = "Counts",
    sumSplVar = aggregateSplVarByType,
    sumSplVarPars = list(
      normEvidence = list(
        "withinSamp" = list(
          "normTibble" = genNorm,
          "normFunc" = normWithGenExp
          )
        )
      )
    )
  otherParams = list(
    min_prop = c(0.1,0.2)
  )
  # Run the analysis
  runSplSurvivalAnal(expName, survOutDir, covariateParams, otherParams, pts, samples, evidenceSplitPath, events, spliceVariants)
  
  # Set up parameters
  expName = "Counts-avg"
  covariateParams = list(
    evidType = "Counts",
    sumSplVar = aggregateSplVarByType,
    sumSplVarPars = list(
      normEvidence = list(
        "withinSamp" = list(
          "normTibble" = genNorm,
          "normFunc" = normWithGenExp
        )
      ),
      sumSplVarFuncPars = list(
        "aggFunc" = mean
      )
    )
  )
  otherParams = list(
    min_prop = c(0.1,0.2)
  )
  # Run the analysis
  runSplSurvivalAnal(expName, survOutDir, covariateParams, otherParams, pts, samples, evidenceSplitPath, events, spliceVariants)
}


# ********** Meta covariates (libsize and gen expression) analysis *************

min_props = c(0.1,0.2)
aggFuncs = c("mean" =  mean, "median" = median, "median-gt0" = function(x)(median(x[x>0])))
for (mp in min_props) {
  for (i in 1:length(aggFuncs)) {
    metaST = createSurvTable_meta(pts, samples , expCounts, aggAcrossSamples = aggFuncs[[i]])
    surv_plot_wrapper(df = metaST, min_prop = mp,
                      savepath = file.path(survOutDir,"Meta",paste(names(aggFuncs)[i],mp,sep='-')), ext = "svg")
  }
}





# 3) SPLICE VARIANT BASED PLOTS ----

avgs = c("median" = median, "mean" = mean)
for (i in 1:length(avgs)) {
  params = list(
    "setupName" = names(avgs)[i],
    "evType" = "Counts",
    "avgFunc" = avgs[[i]],
    "avgFuncParams" = list(
      "na.rm" = TRUE
      )
  )
  makeSplVarPlots(evidencePath, evidenceSplitPath, splVarPlotDir,spliceVariants, samples, params)
}


minThreshes = c(0,0.1,0.2)
for (mt in minThreshes) {
  params = list(
    "setupName" = paste0("Pres",mt),
    "evType" = "PSI",
    "avgFunc" = function(x, minPresThresh) {return(sum(!is.na(x) & x>minPresThresh))},
    "avgFuncParams" = list(
      "minPresThresh" = mt
    )
  )
  makeSplVarPlots(evidencePath, evidenceSplitPath, splVarPlotDir,spliceVariants, samples, params, plotSplVarDens = FALSE)
}

minThreshes = c(0,0.1,0.2)
for (mt in minThreshes) {
  params = list(
    "setupName" = paste0("PresAvg",mt),
    "evType" = "PSI",
    "avgFunc" = function(x, minPresThresh) {return(sum(!is.na(x) & x>minPresThresh)/length(x))},
    "avgFuncParams" = list(
      "minPresThresh" = mt
    )
  )
  makeSplVarPlots(evidencePath, evidenceSplitPath, splVarPlotDir,spliceVariants, samples, params, plotGeneWiseScatter = FALSE)
}





# 4) CORRELATION PLOTS ----

if (runBranch_Counts) {
  plotCorrelations(
    evidencePath, evidenceSplitPath, "Counts", dir_fun(file.path(corrOutPath,"CountsSum")), expCounts,
    spliceVariants, samples,
    genSumFunc = function(x){sum(x, na.rm=T)},
    ptSumFunc  = sum,
    xtitle = "Read support"
  )
  plotCorrelations(
    evidencePath, evidenceSplitPath, "Counts", dir_fun(file.path(corrOutPath,"SplVarNum_byCount")), expCounts,
    spliceVariants, samples,
    genSumFunc = function(x) {sum(!is.na(x))},
    ptSumFunc  = sum,
    xtitle = "# spl.var"
  )
  
}

if (runBranch_PSI) {
  plotCorrelations(
    evidencePath, evidenceSplitPath, "PSI", dir_fun(file.path(corrOutPath,"SplVarNum_byPSI")), expCounts,
    spliceVariants, samples,
    genSumFunc = function(x) {sum(!is.na(x))},
    ptSumFunc  = sum,
    xtitle = "# spl.var"
  )
  plotCorrelations(
    evidencePath, evidenceSplitPath, "PSI", dir_fun(file.path(corrOutPath,"SplVarNum_byPSI_zeroFilt")), expCounts,
    spliceVariants, samples,
    genSumFunc = function(x) {sum(!is.na(x))},
    ptSumFunc  = sum,
    xtitle = "# spl.var",
    zeroFilterOnExpCounts = TRUE
  )
}
