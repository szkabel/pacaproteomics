# Cao et al. survivals

# Histograms ----
# Check the distribution of survivals
bind_rows(
  cao_surv %>% mutate(filterOption = "original", category=vital_status),
  cao_surv %>% filter(in105) %>% mutate(filterOption = "in105", category=vital_status),
  cao_surv %>% filter(in105) %>% mutate(filterOption = "in105-by-normInc",category = paste(vital_status,"norm_inc:",normal_included_for_the_study)),
  cao_surv %>% filter(in105, normal_included_for_the_study=="yes") %>% mutate(filterOption = "onlyNormal", category = paste(vital_status,"norm_inc:",normal_included_for_the_study))
) %>% mutate(category = factor(category, levels = c("Deceased","Living","Deceased norm_inc: no","Deceased norm_inc: yes","Living norm_inc: no","Living norm_inc: yes"))) %>%
      mutate(filterOption = factor(filterOption, levels = c("original","in105","in105-by-normInc","onlyNormal"))) %>% 
  ggplot() + aes(y = follow_up_days, fill=category) + geom_histogram() +
  scale_fill_manual(values = c(rgb(248/255,118/255,109/255),
                               rgb(0/255,191/255,196/255),
                               rgb(min(1,248/255*1.2),min(1,118/255*1.2),min(1,109/255*1.2)),
                               rgb(248/255*0.8,118/255*0.8,109/255*0.8),
                               rgb(min(1,0/255*1.2),min(1,191/255*1.2),min(1,196/255*1.2)),
                               rgb(0/255*0.8,191/255*0.8,196/255*0.8))) +
  facet_wrap(~filterOption)
ggsave(path = save_root("cao"), filename = "FollowUpDays.pdf")

# Try to pick a cutoff, where the n-numbers are roughly equal below or above
cutOffDays = 400
# 16 to 15 division with this

survCutoff = sprintf("%d days",cutOffDays)
cao_clinProt = cao_surv %>% filter(pt %in% unique(cao$pt)) %>% 
                mutate(surv = if_else(follow_up_days >= cutOffDays,"over",
                                                  if_else(vital_status =="Deceased",'under','exclude'))) %>%
  select(pt,surv)
cao_clinProt %>% table() %>% apply(2,sum) %>% write.csv(file = save_root("cao") %F% "surv_n.txt")

# PROTEIN SURVIVALS ----
save_path = "cao/surv_prot"
cao.surv = surv_plots(cao %>% filter(in105, pt %in% unique(cao_clinProt$pt)) %>% getFull(),
                      save_path,cao_clinProt)

df = cao.surv %>% left_join(cao_desc, by = c("id" = "hgnc_symbol"))
proportionPlot(df, sigLim = .05, savePath = save_path, objType = "protein", width = 5/2.54)
# When checking this with all in105 patients there were over 600 significant proteins in NP and ~500 in AP.
# But keep in mind, that they were much more sample in NP. 
# WOOOW! Cool.

# Spread plots.
# NP 
spreadWrapper(df, tisstype="NPS", nmax = 10, savePath = save_path, survCutoff=survCutoff)

# AP
spreadWrapper(df, tisstype="APS", nmax = 10, savePath = save_path, survCutoff=survCutoff)

# All
spreadWrapper(df, tisstype="ALL", nmax = 15, savePath = save_path, survCutoff=survCutoff)

tissueComps = bind_rows(
  tibble(tt1= "NPS", tt2 = ""), tibble(tt1= "APS", tt2 = "")
)
tissueComps %<>% mutate(plot_annot = recode(tt1, !!!long_names)) %>% mutate(plot_annot = str_c("Survival effect in ",plot_annot))

createVolcPlot(tissueComps, 
               cao.surv %>% select(id, tt, value = over_under, pval = pval), 
               cao_desc, idType = "hgnc_symbol", 
               pgTitle = "Survival in PDAC tissue types by protein expression<br />(Cao et al.)", 
               fname = "surv_prot_volc", filepath = "cao/surv_volc", multiSelect = T
              )

# PATHWAY SURVIVALS ----
save_path <- "cao/surv_path"
cao.pws.surv = surv_plots(
          cao.pws %>% 
            left_join(cao %>% distinct(pt,in105)) %>%
            filter(in105, pt %in% unique(cao_clinProt$pt)) %>%
            filter(str_starts(id, "R-HSA-")) %>% filter(genes.found >= 5, keep) %>% 
            getFull(),
          save_path,cao_clinProt)


df <- cao.pws.surv %>% select(-data)
df %<>% left_join(pws.desc)

proportionPlot(df, sigLim = .05, savePath = save_path, objType = "pathway", width = widthNarrow)

propPlotByRootNode(df,savePath = save_path)

spreadWrapper(df, tisstype="NPS", nmax = 10, savePath = save_path, isProt = F, survCutoff=survCutoff)

spreadWrapper(df, tisstype="APS", nmax = 10, savePath = save_path, isProt = F, survCutoff=survCutoff)

spreadWrapper(df, tisstype="ALL", nmax = 15, savePath = save_path, isProt = F, width=15/2.54, survCutoff=survCutoff)


# TODO: go through on objects before createReacomeVolc, whether the left_join(pws.edges %>%  etc. could be placed INTO the function)
df_prep <- cao.pws.surv %>% 
  select(id, tt, pval = pval, value = over_under) %>% 
  filter(!is.na(pval)) %>% 
  left_join(pws.edges %>% select(name,end), by=c("id"="end"))
createReactomeVolc(tissueComps,
                   df_prep,
                   pws.edges, 
                   pgTitle = "Survival in PDAC tissue types by pathway expression<br>(Cao et al.)",
                   fname = "surv_path_volc",
                   fpath = "cao/surv_volc", multiSelect = T)

# Survival analysis on single proteins ----
plotKM_cao = function(cao, cao_surv, protId, selectedTT, cutType = "median", saveDir = NA, ext = "png",...) {
  myProtdat = cao %>% filter(id == protId, tt == selectedTT, in105) %>% left_join(cao_surv %>% select(-in105)) %>% 
    filter(!is.na(follow_up_days)) %>% 
    mutate(event = if_else(vital_status == 'Deceased',T,F))
  
  if (cutType == "median") {
    # median-cut
    myProtdat = myProtdat %>% mutate(value = if_else(value > median(value),"high","low"))
  } else {
    myProtdat = surv_cutpoint(time = "follow_up_days", event = "event", minprop = .33, data = myProtdat, variables = "value") %>%
      surv_categorize() 
  }
  spl = surv_fit(Surv(time = follow_up_days, event = event) ~ value, data = myProtdat) %>% 
    ggsurvplot(pval = T, risk.table = T, legend.title="", risk.table.y.text = T, palette = "lancet") +
    ggtitle(paste0(protId," (",selectedTT,") - ",cutType," cut"))
  if (!is.na(saveDir)) {
    switch(ext,
           "png" = png(paste(saveDir,paste(protId,"KM",selectedTT,paste0(cutType,'.png'),sep="_"),sep="/"),...),
           "svg" = svg(paste(saveDir,paste(protId,"KM",selectedTT,paste0(cutType,'.svg'),sep="_"),sep="/"),...),
           "pdf" = pdf(paste(saveDir,paste(protId,"KM",selectedTT,paste0(cutType,'.pdf'),sep="_"),sep="/"),...)
    )
    print(spl)
    dev.off()
  } else {
    print(spl)
  }
}

width = 5
height = 5 
ext = "pdf"
# STANDARDIZED PLOTS
plotKM_cao(cao, cao_surv, "AP2M1", "APS", "optimal",save_root("cao/KM"), ext = ext, width=width, height=height)
plotKM_cao(cao, cao_surv, "AP2M1", "NPS", "optimal",save_root("cao/KM"), ext = ext, width=width, height=height)
plotKM_cao(cao, cao_surv, "ESYT1", "APS", "optimal",save_root("cao/KM"), ext = ext, width=width, height=height)
plotKM_cao(cao, cao_surv, "ESYT1", "NPS", "optimal",save_root("cao/KM"), ext = ext, width=width, height=height)
plotKM_cao(cao, cao_surv, "NCEH1", "APS", "optimal",save_root("cao/KM"), ext = ext, width=width, height=height)
plotKM_cao(cao, cao_surv, "NCEH1", "NPS", "optimal",save_root("cao/KM"), ext = ext, width=width, height=height)


# Extra checks for the survival plots ----
# to be commented in the final version

tmp = bind_rows(cao %>% filter(pt %in% (cao_clinProt %>% pull(pt))),
                cao_clinProt %>% mutate(surv = if_else(surv == "under",20,35)) %>% rename(value = surv) %>% mutate(id = "SURVIVAL", tt = "APS"),
                cao_clinProt %>% mutate(surv = if_else(surv == "under",20,35)) %>% rename(value = surv) %>% mutate(id = "SURVIVAL", tt = "NPS")
                )

plotProteinHeatmap(tmp %>% select(-in105), protFilter = c("GPNMB","PNLIPRP2","PCTP","PNLIP","CAPS","SURVIVAL"), desc = cao_desc %>% rename(id = hgnc_symbol))
plotProteinHeatmap(tmp %>% select(-in105), protFilter = c("DEFA1B","SST","TAMM41","LRRC15","SURVIVAL"), desc = cao_desc %>% rename(id = hgnc_symbol))
# Both of these make sense
