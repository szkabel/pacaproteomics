surv_plot = function(df, df.desc, name){
  cat("Initializing dataframe\n")
  # plan(multisession(workers = 7))
  suppressPackageStartupMessages(library(gridExtra))
  suppressPackageStartupMessages(library(grid))
  
  # add terms, rename vars, nest data
  df %<>%
    mutate(tt_flip = if_else(str_detect(tt, "<"), str_sub(tt,4,5) %P% ">" %P% str_sub(tt,1,2), tt)) %>%
    select(-tt) %>%
    {tryCatch(group_by(., tt_flip,db,id), error = function(e) group_by(., tt_flip, id))} %>%
    nest %>%
    left_join(df.desc, by = "id")
  
  #create survfits
  df$surv <- map(df$data, function(x) surv_fit(Surv(Survival_Days, OS, type = "right") ~ category, data = x))
  
  #get max-y range for dot-plotting
  max_y_range <- max(sapply(df$data,function(x) max(x$value)))
  if(is.null(df$db)) df$db <- NA
  
  cat("Saving plots\n")
  # plan(multisession(workers = 7))
  mcmapply(function(id, surv, data, term, db, tt_flip, name) {
    
    dotsize = 2*(max(data$value) - min(data$value))/max_y_range
    if(is.null(data$fdr)) data$fdr <- NA
    mean_fdr <- tryCatch(mean(data$fdr, na.rm = T) %>% round(3), error = function(e) NA)
    
    plot_exp <-
      ggplot(data)+
      geom_dotplot(aes(y = value, x=1, fill = as.factor(category)),
                   stackdir = "center", binaxis = "y", binwidth = .1, dotsize = dotsize) +
      labs(title = "",
           y = if_else(!is.na(mean_fdr), "Enrichment score (mean fdr: " %P% mean_fdr %P% ")", "Enrichment score:"),
           x = "") +
      guides(fill=FALSE) + # remove legend
      scale_x_discrete(expand = c(.5,.5))  +# give some extra-space for the box
      theme_minimal() +
      theme(panel.grid.major.x = element_blank(),
            panel.grid.major.y = element_blank(),
            panel.grid.minor.x = element_blank(),
            panel.grid.minor.y = element_blank(),
            axis.text.x.bottom = element_blank(),
            axis.text.y.left = element_text(size = 9, family = "ArialMT", color = "black"),
            axis.title.y.left = element_text(size = 9, family = "ArialMT", color = "black"),
            plot.title = element_blank(),
            plot.margin = unit(c(0,0,.2,0), "in"),
            axis.line.y.left = element_line(colour = "black", size = .2),
            axis.line.x.bottom = element_line(colour = "black", size = .2),
      )
    
    plot <- ggsurvplot(surv,
                       risk.table = T,
                       pval = T,
                       pval.coord = c(0,.05),
                       title = "",
                       legend.labs = c("High", "Low"),
                       pval.size = 7/2.83466799,
                       fontsize = 9/2.83466799, # fontsize in mm ->
                       font.family = "ArialMT",
                       size = .4,
                       censor.size = 2.5)
    
    # extract kaplan-meier
    plot_kap <- plot[[1]] +
      theme(axis.text.y.left = element_text(size = 9, family = "ArialMT"),
            axis.text.x.bottom = element_text(size = 9, family = "ArialMT"),
            axis.title.y.left = element_text(size = 9, family = "ArialMT"),
            axis.title.x.bottom = element_blank(),
            plot.title = element_blank(),
            legend.title = element_blank(),
            legend.text  = element_text(size = 6, family = "ArialMT"),
            legend.background = element_blank(),
            legend.position = c(.9,.9),
            legend.box.spacing = unit(0, "in"),
            plot.margin = unit(c(0,0,0,0), "in"),
            line = element_line(size = .2)
      )
    # plot_kap
    
    # extract risk table
    plot_kaptab <- plot[[2]] +
      theme(axis.text.y.left = element_text(size = 9, family = "ArialMT"),
            axis.text.x.bottom = element_text(size = 9, family = "ArialMT"),
            axis.title.y.left = element_blank(),
            axis.title.x.bottom = element_text(size = 9, family = "ArialMT"),
            plot.title = element_text(size = 9, family = "ArialMT"),
            plot.margin = unit(c(0,0,0,.15), "in"),
            line = element_line(size = .2)
      )
    # plot_kaptab
    
    title = textGrob(str_to_sentence(term) %>% str_trunc(width = 45, ellipsis = "...") %P%
                       "\nEnriched in " %P% str_sub(tt_flip, 1,2) %P% " over " %P% str_sub(tt_flip, 4,5),
                     vjust = 0.5,
                     gp = gpar(fontfamily = "ArialMT", fontsize = 9))
    # combine the three plots
    
    grid <- grid.arrange(title, plot_exp, plot_kap, plot_kaptab,
                         widths = unit(c(1,2.5), "in"),
                         heights =  unit(c(.5,2,1), "in"),
                         layout_matrix = rbind(c(1,1),
                                               c(2,3),
                                               c(2,4)),
                         padding = unit(0, "cm"))
    
    flip_dir <- str_replace(tt_flip, ">", "_large_") %>% str_replace("<", "_small_")
    pathTrg <- wd %F% "res/plots/" %F% name %F% flip_dir %F% db
    if(is.na(db)) pathTrg <- wd %F% "res/plots" %P% name %F% flip_dir
    fileName <- make.names(id)
    if(!dir.exists(pathTrg)) dir.create(pathTrg, recursive = T)
    ggsave(grid,
           path = pathTrg ,
           filename = fileName %P% ".pdf",
           device = "pdf",
           width = unit(4, "in"),
           height = unit(3.5, "in"))
    cat(id %T% pathTrg %P% "\n")
    return("")
  }, df$id, df$surv, df$data, df$term, df$db, df$tt_flip, name, mc.cores = 7)
  return("")
}, 


### create km-plots----
init()
path <- wd %F% "res/surv/"
files <-
  list.files(path)
files

# df <- suppressMessages(read_csv(path %F% "surv.pws.csv")) %>% filter(pval < .05) 
# surv_plot(df, pws %>% rename(term = name) %>% ungroup %>% distinct(term, id), "pws")

# df <- suppressMessages(read_csv(path %F% "surv.pws_log.csv")) %>% filter(pval < .05) 
# surv_plot(df, pws.log %>% rename(term = name) %>% ungroup %>% distinct(term, id), "pws_log")

df <- suppressMessages(read_csv(path %F% "surv.pws_log_full_b.csv")) %>% filter(pval < .05) 
surv_plot(df, pws.log %>% rename(term = name) %>% ungroup %>% distinct(term, id), "pws_log_full_b")

df <- suppressMessages(read_csv(path %F% "surv.logMed.csv")) %>% filter(pval < .05) 
surv_plot(df, desc %>% select(id,  term) %>% distinct, "logMed")

df %>% left_join(desc %>% select(id,  term) %>% distinct) %>% filter(str_starts(term, "DHCR"))# 
# df <- suppressMessages(read_csv(path %F% "surv.logMed.csv")) %>% filter(pval < .05) 
# surv_plot(df, desc, "logMed")
# 
# 
# df <- suppressMessages(
#   read_csv(path %F% "surv.pathSpia.csv")) %>% 
#   filter(pval < .05) %>% 
#   select(-c(name:pnde)) %>%
#   select(-c(ppert:kegglink))
# df
# surv_plot(df, spia %>% distinct(id,term=name), "spia")
# 
# 
# df %<>% mutate(tt = if_else(str_detect(tt, "<"), str_sub(tt,4,5) %P% ">" %P% str_sub(tt, 1,2), tt)) 
# lapply(df$tt %>% unique, function(x) {
#   df %>% 
#     filter(tt == x) %>% 
#     group_by(tt, db, id, n_val, cut, pval, coef, n_high, n_low) %>% 
#     nest %>% 
#     filter(pval <.05) %>% 
#     arrange(desc(abs(coef))) %>% 
#     left_join(spia %>% distinct(id,term=name)) %>% 
#     mutate(dir = if_else(coef>0, "protects", "exposes")) %>% 
#     View(x)
# })



rm(path, files)


