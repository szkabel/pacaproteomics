## script to get kaplan-meier p-values for the proteins & pathway enrichments. 
## needs expression matrix to work = pts as rows, proteins/enrichment values as columns
# generally, modify, and nest data to be purred through -- easier to apply by tt/tt_db basis
# join pt-survival info
# filter by n of NAs 
# impute NA-values
# remove rows (nests/groups) that no longer have any valid observations
# find optimal categorization with surv-cut
# categorize based on the cut
# get k-m logrank pvalueueues
# join with data-description matrix and view.



kaplans = function(df, min_n_val = 6, impute0 = F, path = wd %F% "res", name = "temp.csv", shuffle = F) {
  beep_on_error(beep(expr = {
    cat("Processing " %P% name %P% "\n")
    tic("Filter df by n of values present")
    df$n_val<- sapply(df$data, function(x) sum(!is.na(x$value)))
    df %<>% filter(n_val >= min_n_val)
    toc()
    
    tic("Join clinical data & impute 0s")
    tmp <- clin.prot %>% select(pt, Survival_Days, OS)
    # if(shuffle) tmp %<>% mutate(pt = sample(pt))
    df$data <- mclapply(df$data, function(x) {
      if(shuffle) tmp %>% mutate(pt = sample(pt)) %>% left_join(x, by = "pt")
      else tmp %>% left_join(x, by = "pt")
    }, mc.cores = 6)
    if(impute0) df$data <- mclapply(df$data, function(x) mutate(x, value = replace_na(value, 0)), mc.cores = 4)
    toc()
    
    tic("Get survivals")
    df$cox <- mclapply(df$data, function(x) 
      tryCatch(coxph(Surv(Survival_Days, OS, type = "right") ~ value, data=x, ties = "exact"), error = function(e) NA),
      mc.cores = 6)
    toc()
    
    tic("Clean data")
    df$cox_pval <- lapply(df$cox, function(x) tryCatch(summary(x) %>% {.$coef[5]}, error = function(e) NA)) %>% unlist
    df$cox_coef <- lapply(df$cox, function(x) tryCatch(summary(x) %>% {.$coef[1]}, error = function(e) NA)) %>% unlist
    df$cox <- NULL
    toc()
    
    tic("Save cat-data")
    savePath = wd %F% "res" %F% path
    if(!dir.exists(savePath)) dir.create(savePath, recursive = T)
    df %>% 
      unnest_legacy(cols = data) %>% 
      write_csv(path = savePath %F% name)
    df %<>% select(-data)
    toc()
    
    df %<>% ungroup
    
    gc()
    return(df)
  }))
}



# 2. logfullb ---- 
kaplans(log.full.b %>%
          group_by(tt, id) %>%
          nest,
        min_n_val = 13, impute0 = F, path = "surv", name = "surv.prot_logFullB.csv")
gc()

kaplans(log.full.b %>%
          group_by(tt, id) %>%
          nest,
        min_n_val = 13, impute0 = F,  shuffle = T, path = "surv", name = "surv.prot_logFullB_shuffle.csv")
gc()



# pathways by tissue enrichment ----
# 5. logFullB-full ----
kaplans(pws.logFullB %>% filter(keep) %>% 
          left_join(pws.nodes.combat %>% select(id = end, db) %>% distinct) %>% 
          rename(fdr = "pval") %>% 
          group_by(id, db, tt) %>% 
          nest,
        min_n_val = 13, impute0 = F, path = "surv", name = "surv.pws_logFullB.csv")
gc()


kaplans(pws.logFullB %>% filter(keep) %>% 
          left_join(pws.nodes.combat %>% select(id = end, db) %>% distinct) %>% 
          rename(fdr = "pval") %>% 
          group_by(id, db, tt) %>% 
          nest,
        min_n_val = 13, impute0 = F, shuffle = T, path = "surv", name = "surv.pws_logFullB_shuffle.csv")
gc()













# 2. logfullb ---- 
kaplans(logCombat %>%
          group_by(tt, id) %>%
          nest,
        min_n_val = 13, impute0 = F, path = "surv", name = "surv.prot_logCombat.csv")
gc()

kaplans(logCombat %>%
          group_by(tt, id) %>%
          nest,
        min_n_val = 13, impute0 = F,  shuffle = T, path = "surv", name = "surv.prot_logCombat_shuffle.csv")
gc()



# pathways by tissue enrichment ----
# 5. logFullB-full ----
kaplans(pws.combat %>% filter(keep) %>% 
          left_join(pws.nodes.combat %>% select(id = end, db) %>% distinct) %>% 
          rename(fdr = "pval") %>% 
          group_by(id, db, tt) %>% 
          nest,
        min_n_val = 13, impute0 = F, path = "surv", name = "surv.pws_logCombat.csv")
gc()


kaplans(pws.combat %>% filter(keep) %>% 
          left_join(pws.nodes.combat %>% select(id = end, db) %>% distinct) %>% 
          rename(fdr = "pval") %>% 
          group_by(id, db, tt) %>% 
          nest,
        min_n_val = 13, impute0 = F, shuffle = T, path = "surv", name = "surv.pws_logCombat_shuffle.csv")
gc()





# get stuff ---- 
path <- wd %F% "res/surv/"
files <- list.files(path) 
files

df <- mapply(function(x) {
  df <- suppressMessages(read_csv(path %F% x))
  df %<>% mutate(value = if_else(str_detect(tt, "<"), -value, value),
                 tt = str_replace(tt, "<", ">"))
  groups <- names(df) %>% {.[str_detect(.,"id|tt|n_val|cut|pval|coef|n_high|n_low")]}
  df <- group_by_at(df, all_of(groups)) %>% nest
}, files, USE.NAMES = T, SIMPLIFY = F)
names(df) %<>% str_extract("(?<=surv\\.).+(?=\\.csv)")

df %<>% 
  bind_rows(.id = "file") %>% ungroup %>% 
  select(-data)
df

df %<>% 
  separate(file, into = c("type", "norm", "shuffle"), fill = "right", sep = "_") %>% 
  mutate(shuffle = if_else(is.na(shuffle), "orig", shuffle)) %>% 
  rename(pval = cox_pval, coef = cox_coef) %>% 
  arrange(type, id, tt)
df



df
survs <- 
  left_join(df, pws.desc) %>% 
  mutate(db = if_else(is.na(db), "uniprot", db))
survs %>% distinct(db)
survs

rm(path, files, kaplans, df)
