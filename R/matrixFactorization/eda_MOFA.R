## Functions ----
# Impute data

hk_wrapper <- function(df, hk_id, prefix, path = "imp"){
  df.full <- df %>% getFull
  n_distinct(df.full$id) %>% {cat("Unique ids left:", ., "\n")}
  df 
  
  # Find all HK-values in df
  tmp <- 
    hk_id %>% 
    left_join(df.full) %>% 
    mutate(xaxis =str_replace(tt,"H","A") %>% factor(levels = c("CP", "AP", "NP", "AS", "NS")), 
           tt = recode(tt, !!!long_names_row))  %>% 
    filter(!is.na(value)) 
  tmp$id %>% n_distinct %>% {cat("Unique HK ids found:", ., "\n")}
  
  # Plot EDA boxplot 
  tmp %>% 
    ggplot() +
    geom_boxplot(aes(x = xaxis, y = value, fill = tt), outlier.size = 0, outlier.alpha = 0, notch = T) +
    scale_fill_manual(values = col_pal_row, aesthetics = "fill") +
    base_theme +
    theme(legend.position = "bottom", legend.justification = "center") +
    labs(x = NULL, fill = "Tissue type", y = "Pathway enrichment value")
  ggsave(path = save_root(path), filename = prefix %_% "hk-box.svg", width = widthWide, height = heightLarge)
  
  # Plot EDA boxplot 
  plot <- 
    tmp %>% 
    mutate(pt_tt = str_c(pt, tt, sep = "_")) %>% 
    ggplot() +
    geom_boxplot(aes(x = pt_tt, y = value, fill = tt), outlier.size = 0, outlier.alpha = 0, notch = T) +
    scale_fill_manual(values = col_pal_row, aesthetics = "fill") +
    base_theme +
    theme(legend.position = "bottom", legend.justification = "center") +
    labs(x = NULL, fill = "Tissue type", y = "Pathway enrichment value")
  suppressMessages(ggsave(plot = plot, path = save_root(path), filename = prefix %_% "hk-box2.svg", width = widthWide, height = heightLarge)
  
  # Normalize with to median and sd 
  tmp
  tmp %<>% pull(id) %>% unique
  tmp
  
  df %<>% 
    nest(data = -pt) %>%  # group by pt
    mutate(data = map(data, function(x) {
      hk_values <- x %>% filter(id %in% tmp) %>% pull(value)
      hk_med <- median(hk_values)
      hk_sd <- sd(hk_values)
      x %>% mutate(new_value = (value - hk_med)/hk_sd)
    })) %>% 
    unnest(data)
  
  # Print normalization effect
  df %>%
    pivot_longer(-c(pt,tt,id)) %>% 
    ggplot() +
    geom_density(aes(x = value, color = pt), alpha = .5) +
    facet_rep_wrap(name~tt, nrow = 2)
  ggsave(path = save_root(path), filename = prefix %_% "hk_norm.svg", width = widthWide, height = heightLarge)
  
  df %<>% select(pt, tt, id, value = new_value)
  return(df)
}

pca_wrapper <- function(df, name, path = "imp", short_names = T, depth = 10) {
  tmp <-        
    df %>% 
    select(id, pt, tt, value) %>% 
    getFull() %>% 
    pivot_wider(names_from = id, values_from = value) 
  tmp <- prcomp(tmp %>% unite(pt_tt, c(pt, tt)) %>% column_to_rownames("pt_tt"))
  path <- save_root(path %F% name)
  lapply(2:depth, function(x){
    tmp %>% printPca(1, x, name = name, savepath = path, legX = .90, legY = .70, short_names = short_names)
  })
  lapply(3:depth, function(x){
    tmp %>% printPca(2, x, name = name, savepath = path, legX = .90, legY = .70, short_names = short_names)
  })
  lapply(3:depth, function(x){
    tmp %>% printPca(2, x, name = name, savepath = path, legX = .90, legY = .70, short_names = short_names)
  })
  return("PCAs printed")
}

createAndTrainMofa <- function(mofa.list, setupName) {
  mofa.obj <-  create_mofa(mofa.list)
  
  savePath <- setupName
  data_opts <- get_default_data_options(mofa.obj)
  model_opts <- get_default_model_options(mofa.obj)
  train_opts <- get_default_training_options(mofa.obj)
  model_opts$num_factors <- 50
  # train_opts$convergence_mode <- "medium"
  train_opts$convergence_mode <- "fast"
  no_drop <- F
  c(data_opts, model_opts, train_opts, "no_drop" = no_drop) %>% enframe %>% 
    mutate(value = map_chr(value, function(x) ifelse(is.character(x), str_c(x,collapse = ", "), as.character(x)))) %>% 
    write_tsv(save_root(savePath) %F% "settings.txt")
  
  mofa.obj %<>% prepare_mofa(data_options = data_opts, model_options = model_opts, training_options = train_opts)
  mofa.obj %<>% run_mofa(outfile = save_root(savePath) %F% "data.hdf5")
  return(mofa.obj)
}

imputeMofa <- function(mofa.obj, mofaVersion = "original") {
  # Replacing NaNs with NAs
  mofa.obj@intercepts = lapply(mofa.obj@intercepts, function(x) {
    lapply(x, function(y) {
      y[is.na(y)] = 0
      return(y)
    })
  } )
  if (mofaVersion != "original") {
    mofa.obj %<>% impute(add_intercept = F)
  } else {
    mofa.obj %<>% impute()
  }

  imp <- get_imputed_data(mofa.obj, as.data.frame = T) %>%  as_tibble
  return(imp)
}

predictMofa <- function(mofa.obj, mofaVersion = "original") {
  # Replacing NaNs with NAs
  mofa.obj@intercepts = lapply(mofa.obj@intercepts, function(x) {
    lapply(x, function(y) {
      y[is.na(y)] = 0
      return(y)
    })
  } )
  if (mofaVersion != "original") {
    pred = predict(mofa.obj,add_intercept = F)
  } else {
    pred = predict(mofa.obj)
  }
  
  tibbleFormPred = lapply(seq_along(pred), 
    function(i,v,viewNames) {
      lapply(seq_along(v[[i]]),
        function(j,g,groupNames,viewName) {
          g[[j]] %>% as_tibble(rownames = "feature") %>% 
            pivot_longer(cols=-feature, names_to = "sample", values_to="value") %>% 
            mutate(group=groupNames[[j]], view=viewName)
        },
        g = v[[i]], groupNames=names(v[[i]]), viewName = viewNames[[i]])
    },
    v = pred, viewNames=names(pred)) %>% 
    flatten() %>% bind_rows()
  
  return(tibbleFormPred)
}

sampleRename <- function(id,...) {
  paste0(str_sub(id,5,6),"_",str_sub(id,1,3))
}

# Not working yet
protRename <- function(id,desc.joint) {
  id %>% as_tibble() %>% rename(id = value) %>% left_join(desc.joint) %>% pull(short_name) %>% suppressMessages()
}

# Note: we need both Z and W in both cases, because of the possible transpositions
visualizeGroupFactorAssoc <- function(    mofa.obj, 
                                          savePath,
                                          desc, 
                                          protAx,
                                          zRename = function(x,...){x},
                                          wRename = function(x,...){x},
                                          zRenamePars = NULL,
                                          wRenamePars = NULL) {
  
  ext = ".png"
  
  W = lapply(mofa.obj@expectations$W,as_tibble,rownames="id") %>%
        bind_rows(.id="view") %>% pivot_longer(-c(id,view)) %>% 
        mutate(name = paste0(str_sub(name,1,6),sprintf('%02d',as.double(str_sub(name,7,str_length(name))))))
  Z = lapply(mofa.obj@expectations$Z,as_tibble,rownames="id") %>% bind_rows(.id="group") %>% pivot_longer(-c(group,id)) %>% 
    mutate(name = paste0(str_sub(name,1,6),sprintf('%02d',as.double(str_sub(name,7,str_length(name))))))
  
  # Plot Z heatmaps
  limV = sd(Z$value)*5
  zplot <- Z %>%
    mutate(id2=zRename(id,zRenamePars)) %>%
    ggplot() +
    geom_tile(aes(fill=value, x = id2, y = name)) +
    scale_fill_distiller(limits=c(-limV,limV), palette = "RdBu") +
    theme(axis.text.x = element_text(angle=90))
  ggsave(path = save_root(savePath %F% "figs"), filename = paste0("Z-heatmap",ext), width = widthWide, height = heightLarge)
  
  # Ok, ask Juho about this tomorrow, too late now
  tgtDir = save_root(savePath %F% "Z-vals")
  if (!file.exists(tgtDir)){
    dir.create(tgtDir)
  }
  factZ = Z %>% nest(!name)
  for (i in 1:dim(factZ)[1]) {
    write_tsv(as.data.frame(factZ$data[i]),save_root(savePath) %F% "Z-vals" %F% paste0(factZ$name[i],'.tsv'))
  }
  
  # Plot Z distribution
  Z %>% ggplot() +
    geom_histogram(aes(x=value)) + facet_wrap(~name)
  ggsave(path = save_root(savePath %F% "figs"), filename = paste0("Z-histogram",ext), width = figWidth, height = figHeight)
  
  # Plot W heatmaps
  limV = sd(W$value)*5
  wplot <- W %>%
    mutate(id2=wRename(id,wRenamePars)) %>%
    ggplot() +
    geom_tile(aes(fill=value, x = name, y = id2)) +
    scale_fill_distiller(limits=c(-limV,limV), palette = "RdBu") +
    theme(axis.text.x = element_text(angle=90))
  ggsave(path = save_root(savePath %F% "figs"), filename = paste0("W-heatmap",ext), width = figWidth, height = figHeight)
  
  tgtDir = save_root(savePath %F% "W-vals")
  if (!file.exists(tgtDir)){
    dir.create(tgtDir)
  }
  factW = W %>% nest(!name)
  for (i in 1:dim(factW)[1]) {
    write_tsv(as.data.frame(factW$data[i]),save_root(savePath) %F% "W-vals" %F% paste0(factW$name[i],'.tsv'))
  }
  
  # Plot W distribution
  W %>% ggplot() +
    geom_histogram(aes(x=value)) + facet_wrap(~name)
  ggsave(path = save_root(savePath %F% "figs"), filename = paste0("W-histogram",ext), width = figWidth, height = figHeight)
  
  tgtDir = save_root(savePath %F% "Prot-weights")
  if (!file.exists(tgtDir)){
    dir.create(tgtDir)
  }
  if (protAx == "Z") {
    factProt = Z
  } else if (protAx == "W") {
    factProt = W
  }
  factProt %<>% left_join(desc) %>% nest(!name)
  for (i in 1:dim(factProt)[1]) {
    write_tsv(as.data.frame(factProt$data[i]),save_root(savePath) %F% "Prot-weights" %F% paste0(factProt$name[i],'.tsv'))
  }
  
}

runMofaCase <- function(mofa.list, 
                        savePath, 
                        setup, 
                        mofaVersion,
                        errorTbl,
                        desc,
                        protAx,
                        zRename = function(x,...){x}, wRename = function(x,...){x}) {
  # Run mofa
  mofa.obj = createAndTrainMofa(mofa.list, savePath)
  
  # Do imputation
  pred = predictMofa(mofa.obj,mofaVersion)
  
  # Calculate errors
  errors = calcReconError(mofa.list,pred,save_root(savePath))
  
  # Save out error results
  errorTbl %<>% bind_rows(
    as_tibble(errors) %>% mutate(caseId = setup, mofaVersion= mofaVersion) )
  
  # Plot and save figures
  visualizeGroupFactorAssoc(mofa.obj, savePath, desc, protAx, zRename = zRename, wRename = wRename)
  
  save(mofa.obj,file = save_root(savePath) %F% "MOFA_obj.RData")
  
  return(errorTbl)
}

### START ----

# Helper to start this clean
#.rs.restartR()
source('D:/Abel/Research/Projects/Juho/PancreaticCancer/src/Proteomics/R/init.R')
source('D:/Abel/Research/Projects/Juho/PancreaticCancer/src/Proteomics/R/init_env_spec_vars.R')

# Prerequistic to run this from scratch.
#source('D:/Abel/Research/Projects/Juho/PancreaticCancer/src/Proteomics/R/imp_raw.R')
#source('D:/Abel/Research/Projects/Juho/PancreaticCancer/src/Proteomics/R/imp_logMed.R')
#Note there were issues with creating logMed.comb
#source('D:/Abel/Research/Projects/Juho/PancreaticCancer/src/Proteomics/R/eda_ctrl_pancreas.R')
#source('D:/Abel/Research/Projects/Juho/PancreaticCancer/src/Proteomics/R/eda_openMS.R')
# The 4 source file above have been run and stored under
load(wd %F% "Data/openms/210218_mofaPreparation.RData")


## Libs ----
# And to make clean, the new version of GFA which handles missing values
# BiocManager::install("MOFA2")
# devtools::install_github("bioFAM/MOFA", build_opts = c("--no-resave-data"))
# devtools::install_github("bioFAM/MOFA2", build_opts = c("--no-resave-data"))
# Note: Procedure for Conda installation and Windows (I had the Conda already installed as Anaconda Navigator).
# and some tools already in the base environment.
# Open CMD -> type in activate base
# then pip install mofapy2
# But then it doesn't solve everything.
# Check out also: https://github.com/bioFAM/MOFA/issues/50
# So in the end, what was the right thing. Create a conda env from R as described in the link
# And then activate that environment externally in a conda prompt, and install mofapy2 there.

library(MOFA2)
library(reticulate)

#use_python("/Library/Frameworks/Python.framework/Versions/3.8/bin/python3.8", required = TRUE) # Using a specific python binary
# python3.8 pip -m install mofapy2

use_python("C:/Users/szkabel/Anaconda3/python.exe", required = TRUE) # Using a specific python binary
use_condaenv("mofa2_env", conda="C:/Users/szkabel/Anaconda3/Scripts/conda.exe", required = TRUE)
#mofaVersion = "unnormalized"
mofaVersion = "original"

baseTgtDir = "mofa"

### MOFA ----
ids <- joint.raw %>% filter(src == "lund") %>% distinct(id) %>% pull(id) 

errorTbl = tibble( caseId = character(), 
                    mofaVersion=character(), 
                    RMSE=double(), MSE = double(), Fro = double(), nofZeroDiff = double(), nofnonZeroDiff = double())

# ids %>% length
# mofa.list <-
#   joint %>%
#   filter(id %in% ids, !batch %in% c(5,6), src %in% c("kushner", "lund")) %>%
#   mutate(group = str_c(tt, batch, sep = "_"), sample = str_c(pt, tt, sep = "_")) %>%
#   # mutate(group = str_c(tt, batch, pt, sep = "_"), sample = str_c(pt, tt, sep = "_")) %>%
#   select(id, group, sample, value) %>%
#   complete(nesting(group, sample), id) %>%
#   split(.$group) %>% lapply(function(x) {
#       x %>% select(-group) %>%
#         pivot_wider(names_from = sample, values_from=value ) %>%
#         column_to_rownames("id") %>% as.matrix
#     })
# mofa.list %>% lapply(function(x) x[1:10,])
# mofa.list %>% sapply(function(x) dim(x))

### CASES ----

## 1)  joint__grp-tt+batch_views-no ----
# *************************************************************

# Documentation
setup = "joint__grp-tt+batch__views-no"
setupName = paste0(setup,'____','mofa-', mofaVersion)

# Creation of input data
mofa.list <-
  joint %>%
  filter(id %in% ids, !batch %in% c(5,6), src %in% c("kushner", "lund")) %>%
  mutate(group = str_c(tt, batch, sep = "_"), sample = str_c(pt, tt, sep = "_")) %>%
  select(id, group, sample, value) %>%
  complete(nesting(group, sample), id) %>% 
  mutate(view = 1) %>% 
  select(sample, group, feature = id, view, value)

# Run case
savePath = baseTgtDir %F% setupName
errorTbl = runMofaCase(mofa.list, savePath, setup, mofaVersion, errorTbl, desc.joint, "W", zRename = sampleRename)

## 2)  joint__grp-tt_views-no ----
# *************************************************************

# Documentation
setup = "joint__grp-tt__views-no"
setupName = paste0(setup,'____','mofa-', mofaVersion)

# Creation of input data
mofa.list <-
  joint %>%
  filter(id %in% ids, !batch %in% c(5,6), src %in% c("kushner", "lund")) %>%
  mutate(group = tt, sample = str_c(pt, tt, sep = "_")) %>%
  select(id, group, sample, value) %>%
  complete(nesting(group, sample), id) %>% 
  mutate(view = 1) %>% 
  select(sample, group, feature = id, view, value)

# Run case
savePath = baseTgtDir %F% setupName
errorTbl = runMofaCase(mofa.list, savePath, setup, mofaVersion, errorTbl, desc.joint, "W", zRename = sampleRename)


## 3)  joint__grp-no__views-tt ----
# *************************************************************

# Documentation
setup = "joint__grp-no__views-tt"
setupName = paste0(setup,'____','mofa-', mofaVersion)

# Creation of input data
mofa.list <-
  joint %>%
  filter(id %in% ids, !batch %in% c(5,6), src %in% c("kushner", "lund")) %>%
  mutate(group = 1, sample = str_c(pt, tt, sep = "_")) %>%
  select(id, group, sample, value,tt) %>%
  complete(nesting(group, sample,tt), id) %>% 
  rename(view = tt) %>% 
  select(feature = sample, group, sample = id, view, value)

# Run case
savePath = baseTgtDir %F% setupName
errorTbl = runMofaCase(mofa.list, savePath, setup, mofaVersion, errorTbl, desc.joint, "Z", wRename = sampleRename)

## 4)  joint.hk__grp-tt+batch_views-no ----
# *************************************************************

# Documentation
setup = "joint.hk__grp-tt+batch__views-no"
setupName = paste0(setup,'____','mofa-', mofaVersion)

# Creation of input data
mofa.list <-
  joint.hk %>%
  filter(id %in% ids, !batch %in% c(5,6), src %in% c("kushner", "lund")) %>%
  mutate(group = str_c(tt, batch, sep = "_"), sample = str_c(pt, tt, sep = "_")) %>%
  select(id, group, sample, value) %>%
  complete(nesting(group, sample), id) %>% 
  mutate(view = 1) %>% 
  select(sample, group, feature = id, view, value)

# Run case
savePath = baseTgtDir %F% setupName
errorTbl = runMofaCase(mofa.list, savePath, setup, mofaVersion, errorTbl, desc.joint, "W", zRename = sampleRename)

## 5)  joint__grp-tt_views-no ----
# *************************************************************

# Documentation
setup = "joint.hk__grp-tt__views-no"
setupName = paste0(setup,'____','mofa-', mofaVersion)

# Creation of input data
mofa.list <-
  joint.hk %>%
  filter(id %in% ids, !batch %in% c(5,6), src %in% c("kushner", "lund")) %>%
  mutate(group = tt, sample = str_c(pt, tt, sep = "_")) %>%
  select(id, group, sample, value) %>%
  complete(nesting(group, sample), id) %>% 
  mutate(view = 1) %>% 
  select(sample, group, feature = id, view, value)

# Run case
savePath = baseTgtDir %F% setupName
errorTbl = runMofaCase(mofa.list, savePath, setup, mofaVersion, errorTbl, desc.joint, "W", zRename = sampleRename)


## 6)  joint__grp-no__views-tt ----
# *************************************************************

# Documentation
setup = "joint.hk__grp-no__views-tt"
setupName = paste0(setup,'____','mofa-', mofaVersion)

# Creation of input data
mofa.list <-
  joint.hk %>%
  filter(id %in% ids, !batch %in% c(5,6), src %in% c("kushner", "lund")) %>%
  mutate(group = 1, sample = str_c(pt, tt, sep = "_")) %>%
  select(id, group, sample, value,tt) %>%
  complete(nesting(group, sample,tt), id) %>% 
  rename(view = tt) %>% 
  select(feature = sample, group, sample = id, view, value)

# Run case
savePath = baseTgtDir %F% setupName
errorTbl = runMofaCase(mofa.list, savePath, setup, mofaVersion, errorTbl, desc.joint, "Z", wRename = sampleRename)


errorTbl %>% as.data.frame() %>% write_tsv(save_root("mofa") %F% paste0("reconstructionErrors_",mofaVersion,".tsv"))
  
# mofa.list <-
#   joint.hk %>%
#   # filter(id %in% ids, !batch %in% c(5,6)) %>%
#   filter(id %in% ids, !batch %in% c(5,6), src %in% c("kushner", "lund")) %>%
#   select(id, tt, pt, value)%>%
#   complete(nesting(pt, tt), id) %>%
#   split(.$tt) %>%
#   lapply(function(x) {
#     unite(x, sample, pt, tt, sep = "_") %>% pivot_wider(names_from = id, values_from=value ) %>%
#       column_to_rownames("sample") %>% as.matrix
#     })
# mofa.list %>% lapply(function(x) x[,1:10])


# views_names(mofa.obj)
# mofa.obj
# plot_data_heatmap(mofa.obj,
#                   view = "1",         # view of interest
#                   factor = 1,             # factor of interest
#                   features = 50,          # number of features to plot (they are selected by weight)
#                   
#                   # extra arguments that are passed to the `pheatmap` function
#                   cluster_rows = TRUE, cluster_cols = FALSE,
#                   show_rownames = TRUE, show_colnames = FALSE
# )
# 
# 
# 
# if(no_drop) mofa.obj <-  load_model(file = save_root(savePath) %F% "data.hdf5", remove_inactive_factors = F, remove_outliers = F)
# 
# imp %>% 
#   select(feature, id = sample, value) %>% 
#   separate(feature, into = c("pt", "tt")) 
# imp
#   
# imp %>% pivot_wider(names_from = c(pt, tt))
# imp %>% filter(pt %in% sprintf("P%02d", 1:14)) %>% pivot_wider(names_from= c(pt,tt))
# 
# imp %>% 
#   mutate(tt = if_else(is.na(tt), "LF", tt)) %>% 
#   distinct(pt, tt) %>% 
#   split(.$tt) %>% 
#   lapply(function(x) unique(x$pt))
# imp
# 
# imp %<>% left_join(joint %>% select(id, pt, tt, value_old = value))
# imp %<>% left_join(distinct(joint, pt, tt, batch, src))
# imp
# 
# imp %>% 
#   mutate(diff = (value_old-value)^2) %>% 
#   summarise(mean(diff, na.rm = T), sum(diff==0,na.rm=T), sum(diff!=0,na.rm=T), sum(diff,na.rm=T),  sqrt(sum(diff,na.rm=T))) %>% 
#   pivot_longer(everything()) %T>% print %>%
#   write_tsv(file = save_root(savePath) %F% "error.tsv")
# 
# # imp %<>%  
# #   mutate(value = value-min(value)+1) %>% 
# #   group_by(tt, pt) %>% 
# #   mutate(value = log2(value)-log2(median(value))) %>% 
# #   ungroup
# 
# imp %>% pca_wrapper("imp", path = savePath)
# imp %>% filter(!and(pt == "P08", tt== "NS")) %>% pca_wrapper("imp.no8", path = savePath)
# imp  
# imp.hk <- hk_wrapper(imp %>% select(id, pt, tt, value), hk_id = hk_emp[1:500,"id"], savePath)
# imp.hk %>% pca_wrapper("imp.hk", path = savePath)
# 
# tmp1 <- imp %>% filter(src == "lund")   %>% select(id, tt, pt, value, batch) %>% {left_join(combat(., use.model = T), select(., -value))}
# tmp2 <- imp %>% filter(src == "kushner")%>% select(id, tt, pt, value, batch) %>% {left_join(combat(., use.model = F), select(., -value))}
# imp.comb <- bind_rows("lund" = tmp1, "kush" = tmp2, .id = "src")
# rm(tmp1, tmp2)
# 
# imp.comb
# imp.comb %>% pca_wrapper("imp.comb", path = savePath)
# 
# imp.comb %>% filter(src == "lund") %>% pca_wrapper("imp.comb.lund", path = savePath)
# imp.comb %>% filter(src == "lund") %>% 
#   filter(!and(pt == "P08", tt== "NS")) %>% 
#   pca_wrapper("imp.comb.lund.no8", path = savePath)
# 
# imp.comb.hk <- hk_wrapper(imp.comb %>% select(id, pt, tt, value), hk_id = hk_emp[1:500,"id"], savePath)
# imp.comb.hk %>% pca_wrapper("imp.comb.hk", path = savePath)
# imp.comb.hk %>% filter(!and(pt == "P08", tt== "NS")) %>% pca_wrapper("imp.comb.hk_no8", path = savePath)
# imp.comb.hk %>% 
#   filter(tt != "CP") %>% 
#   filter(!and(pt == "P08", tt== "NS")) %>% pca_wrapper("imp.comb.hk.lund.no8", path = savePath)
# 
# 
# 
# left_join(imp.comb %>% rename(combat = value),
#           imp.comb.hk %>% rename(combat.hk = value)) %>% 
#   group_by(pt, tt) %>% nest %>% 
#   mutate(correlations = map_dbl(data, ~cor(x= .$combat, y= .$combat.hk, use="complete.obs"))) %>%
#   ggplot() + 
#   geom_col(aes(x = pt, y = correlations, fill = tt)) + 
#   scale_fill_manual(values = col_pal) +
#   facet_wrap(~tt)
# 
# left_join(imp.comb %>% rename(combat = value),
#           imp.comb.hk %>% rename(combat.hk = value)) %>% 
#   ggplot() + 
#   geom_point(aes(x = combat, y = combat.hk, fill = tt)) + 
#   scale_fill_manual(values = col_pal) +
#   facet_wrap(~tt)
# 
# 
# rm(list = ls() %>% str_subset("opts"))
# rm(savePath, ids, no_drop, mofa.list, mofa.obj)


# {
#   mofa.obj <- createMOFAobject(mofa.list$matrixForm)
#   DataOptions <- getDefaultDataOptions()
#   ModelOptions <- getDefaultModelOptions(mofa.obj)
#   ModelOptions$numFactors <- 30
#   TrainOptions <- getDefaultTrainOptions()
#   TrainOptions$seed <- 2018
#   TrainOptions$DropFactorThreshold <- 0.1# Automatically drop factors that explain less than 1% of variance in all omics
#   TrainOptions$tolerance <- 0.01 # Automatically drop factors that explain less than 1% of variance in all omics
#   }
# ModelOptions
# DataOptions
# TrainOptions
# 
# mofa.prep <- prepareMOFA(
#   mofa.obj, 
#   DataOptions =  DataOptions,
#   ModelOptions =  ModelOptions,
#   TrainOptions = TrainOptions
# )
# mofa.run <- runMOFA(mofa.prep)

# Add metadata to the plot
# factor1 <- sort(getFactors(mofa.run,"LF1")[,1])
# order_samples <- names(factor1)
# df <- data.frame(
#   row.names = order_samples,
#   factor = factor1
# )
# df
# df %<>% 
#   {mutate(., sample = rownames(.))} %>% 
#   separate(sample, into = c("tt", "pt"))

# plotDataHeatmap(
#   object = mofa.run, 
#   view = "view_1", 
#   factor = "LF4", 
#   features = 10, 
#   transpose = T, 
#   show_colnames=T, 
#   show_rownames=TRUE, # pheatmap options
#   cluster_cols = FALSE, 
#   annotation_col=df # pheatmap options
# )

# mofa.imp <- impute(mofa.run)
# mofa.imp
# mofa.imp %>% View

# mofa.list$imp <- lapply(viewNames(mofa.run), function(x) {
#   getImputedData(mofa.imp, view=x)[[1]] %>%
#     as_tibble(rownames = "sample") %>%
#     select(sample, everything())
# })

# mofa.list
# mofa.list$imp[[1]]
# imp <-
#   mofa.list %>%
#   select(imp) %>%
#   unnest(imp) %>%
#   pivot_longer(-sample, names_to = "id") %>%
#   separate(sample, into = c("pt", "tt"))
# imp

# mofa.run %>% str 
# lapply(mofa.run@Expectations$W, function(x) {
#   x %*% t(mofa.run@Expectations$Z) %>%
#     as_tibble(rownames = "sample") %>% 
#     select(id, everything())
# }) %>%  bind_rows() 
# 
# View(mofa.run)

# df <- lapply(mofa.imp@Expectations$W, function(x) {
#   x %*% t(mofa.imp@Expectations$Z) %>%
#     as_tibble(rownames = "sample") %>% 
#     select(sample, everything())
# }) %>%  bind_rows() 
# df %<>% 
#   pivot_longer(-sample) 


