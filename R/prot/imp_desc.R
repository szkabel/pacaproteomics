# GET DESCRIPTIONS ----

# Call the function actually
desc = downloadEnsemblDescriptions(raw %>% distinct(id))
# NOTE! This is not exactly the same result as before, as 
# the function above returns a single entry per query item. That item is briefly the one that
# has most of the requested variables.

# Also, rename the uniprotswissprot to id so that other codes do not need to be tweaked
desc %<>% rename(id = uniprotswissprot)


# Cheat-sheet lines for checking available filters
# listFilters(ensembl) %>% DT::datatable()
# listAttributes(ensembl) %>% DT::datatable()
