# Create log-median normalized values----
logMed <-
  raw %>%
  group_by(tt, pt) %>% 
  mutate(value = log2(value)-log2(median(value))) %>%  #in groups 
  mutate(id = str_extract(id, "^[:alnum:]+")) %>%
  group_by(src, id, tt, pt, batch) %>% 
  summarise(value = mean(value)) %>%  #duplicates b/c short ids -> summarise
  ungroup

logMed %>% 
  filter(pt == "P06", id == "Q9NTJ5", tt == "NP") %>% print.AsIs # correct value 2.746111


ttComparisons = list(c("NP","AP"),c("NS","AS"), c("NP","NS"), c("AP","AS"), c("AP","CP"), c("NP","CP"))
logMed.volc = makeVolc(logMed, ttComparisons)


df <- logMed
df %>% pca_wrapper(name = "logMed", path = "prot_pca")
df %>% filter(src == "pdac") %>% pca_wrapper(name = "logMed.pdac", path = "prot_pca")
df %>% filter(src == "cp") %>% pca_wrapper(name = "logMed.cp", path = "prot_pca")

df <- logMed %>% group_by(src) %>% summarize(id = unique(id), .groups = "drop")
df %<>% group_by(id) %>% mutate(n=n()) %>% ungroup %>% mutate(is_uniq = if_else(n == 1, "Unique", "Shared")) 
df %<>% mutate(is_uniq = factor(is_uniq, levels = c("Unique", "Shared")))
df %>% 
  ggplot() + 
  geom_bar(aes(x = src, fill = is_uniq))+
  scale_y_continuous(expand = expansion(mult = 0))+
  base_theme +
  labs(y = "Number of ids", x = "Source", fill = "Type")
ggsave(path = save_root("prot_combat"), filename = "logMed_unique.svg", height = heightNorm, width = widthNorm)
rm(df)
