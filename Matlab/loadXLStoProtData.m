%loadXLStoProtData

if ~exist('PD','var')
    PD = ProtData();
end

% Here one should add more rows
%batchID = 1;
%loadBatch1;
%PD.addTable(dataMatrix,protIDs,columnIDs);

batchID = 2;
loadBatch2;
PD.addTable(dataMatrix,protIDs,columnIDs);

% This is querying info from Uniprot and KEGG.
PD.refreshAllFieldsInProteinLabels();