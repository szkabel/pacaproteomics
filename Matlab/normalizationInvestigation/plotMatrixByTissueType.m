%This scripts creates a plot where the corresponding proteins from
%different patients and tissue states are plotted against each other.

[relAbuM,~,sampleLabs] = PD.fetchData('normType','RelAbu');

[~,~,~,~,~,~,flatMatrixOKL,flatYlabOKL,flatIdxOKL] = groupMatrixByCol(relAbuM,sampleLabs,{'tissueType','state'},'groupingOrder',{'ascend','descend'},'keepLevel',1);
totalMax = log2(min(relAbuM));
[~,~,~,~,~,hierarchIdx,~,~,~] = groupMatrixByCol(relAbuM,sampleLabs,{'tissueType','state'},'groupingOrder',{'ascend','descend'},'keepLevel',2);
[~,~,~,~,~,patientGroupIdx,~,~,~] = groupMatrixByCol(relAbuM,sampleLabs,{'patientID'});
%NOTE THIS IS BURNT IN, but the sample labels are not!
tissueOrder = {'PANCREAS','STROMA'};

for i=1:length(flatMatrixOKL)
    figure;
    [~,abuMatrixAxes] = plotmatrix(log2(flatMatrixOKL{i}));
    suptitle(['Log2 of RELATIVE abundancies in ' tissueOrder{i}]);    
    for j=1:size(flatMatrixOKL{i},2)
        abuMatrixAxes(1,j).Title.String = flatYlabOKL{i}{j};
    end
    %Gray out background a bit to highlight self-relation in Neo vs.
    %healthy
    for j=1:size(abuMatrixAxes,1)
        for k=1:size(abuMatrixAxes,2)
            if j~=k
                axes(abuMatrixAxes(j,k));
                hold on;
                plot([0 totalMax],[0 totalMax]);
            end
            origIdxR = flatIdxOKL{i}(j);
            origIdxC = flatIdxOKL{i}(k);
            if ~any(cellfun(@all,cellfun(@ismember,repmat({[origIdxR origIdxC]},1,length(hierarchIdx{i})),hierarchIdx{i},'UniformOutput',false))) ...
                && ~any(cellfun(@all,cellfun(@ismember,repmat({[origIdxR origIdxC]},1,length(patientGroupIdx)),patientGroupIdx,'UniformOutput',false)))
                abuMatrixAxes(j,k).Color = [.9 .9 .9];
            end
        end
    end
end
