function normalizedMatrix = normalizeByTissueType(matrix,colLab,aggrType,groupingFields)
    [~,~,~, ~,~,~, reorgMatOKL,~,idxOKL] = groupMatrixByCol(matrix,colLab,groupingFields,'keepLevel',length(groupingFields));

    nofGroups = length(idxOKL);
    groupAggr = cell(1,nofGroups);

    %Collect the grouping wise aggregations
    for i=1:nofGroups
        currM = reorgMatOKL{i};
        switch aggrType
            case 'median'
                groupAggr{i} = nanmedian(currM,2);
            case 'mean'
                groupAggr{i} = nanmean(currM,2);
        end
    end

    normalizedMatrix = nan(size(matrix));
    % Apply the aggregation on the correct column
    for i=1:nofGroups        
        normalizedMatrix(:,idxOKL{i}) = matrix(:,idxOKL{i}) - groupAggr{i};
    end
end