tgtDir = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\byTissueNormalization';
methods = {'mean','median'};

[baseMatrix,protLabs,colLab] = PD.fetchData();

protLabsUniprotWOiso = cell(size(protLabs));

for i=1:length(protLabsUniprotWOiso)
    protLabsUniprotWOiso{i} = protLabs{i}.uniProtWOisoform;        
end


for i=1:length(methods)
    normalizedMatrix = normalizeByTissueType(baseMatrix,colLab,methods{i},{'tissueType','state'});

    currTgtDir = fullfile(tgtDir,methods{i});
    if ~exist(currTgtDir,'dir'), mkdir(currTgtDir); end
    
    for j=1:length(colLab)
        currVals = normalizedMatrix(:,j);
        filterIdx = ~isnan(currVals);
        T = table(protLabsUniprotWOiso(filterIdx),currVals(filterIdx));
        writetable(T,fullfile(currTgtDir,[colLab{j}.print() '.txt']),'WriteVariableNames',false);
        resDir = fullfile(currTgtDir,colLab{j}.print());
        if ~exist(resDir,'dir'), mkdir(resDir); end
    end
end

