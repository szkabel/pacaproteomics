%Fetch matrix
%[fullM, protIDs, colLabs] = PD.fetchData('batchIds',[2],'afterFilterImpute','keepFullRows');
rng(201210121)
tgtDir = [ driveLetter{1} ':\Abel\Research\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\CompDeconvolution'];

%Group the samples by patients
[reorgM , ylabs , orgIdx ,...
 reorgMC, ylabsC, orgIdxC] = groupMatrixByCol(fullM,colLabs,{'patientID'});
nofPatients = length(reorgMC);

% Check still that the original PCA works.
pcaSamplesNew(fullM,'legend',colLabs,'dims',[1 2],'title','Original PCA');

% Compose new, artificial samples by mixing the values (for simplicity take
% the average of values
bulkMatrix = cell(1,nofPatients);
newLabs = cell(nofPatients,1);
mixingCoeffs = cell(1,nofPatients);
for i=1:nofPatients
    mixingCoeffs{i} = rand(size(reorgMC{i},2),1);
    mixingCoeffs{i} = mixingCoeffs{i}/sum(mixingCoeffs{i}); % Normalize to sum up t 1
    bulkMatrix{i} = reorgMC{i}*mixingCoeffs{i};
    newLabs{i} = SampleLabel(colLabs{orgIdxC{i}(1)}.patientID,'Mixed','Artificial',NaN);
end
bulkMatrix = cell2mat(bulkMatrix);
mixingMatrix = cell2mat(mixingCoeffs);

extMatrix = [fullM bulkMatrix];

pcaSamplesNew(extMatrix,'legend',[colLabs;newLabs],'dims',[1 2],'title','PCA with the mixed samples');

% Aaand here starts the show (can we do this back?)
M = bulkMatrix;

%Make it a reasonable non-negative task by adding the minimum
globMin = min(M(:));
M = M - globMin;

% Re-implement the paper's suggestion
% Here we have 4 tissue types
k = 4;
%[W0,H0] = nnmf(M,k,'Algorithm','mult','Options',statset('MaxIter',10),'Replicates',20);
%[G,S] = nnmf(M,k,'W0',W0,'H0',H0,'Algorithm','als',...
%    'Options', statset('MaxIter',10^6));
[G,S] = nnmf(M,k,'Algorithm','mult','Replicates',50,...
                 'Options',statset('MaxIter',10^6));

%Pfuh, looks way too fast to be true.
%Reconstruct the samples
%Check for rank
R = rank(G);

reconSamples = cell(1,R*nofPatients);
reconSampLabs = cell(R*nofPatients,1);
for i=1:R
    for j=1:nofPatients
        reconSamples{(i-1)*nofPatients + j} = G(:,i)*S(i,j);
        reconSampLabs{(i-1)*nofPatients + j} = SampleLabel(colLabs{orgIdxC{j}(1)}.patientID,sprintf('%d',i),'Reconstructed',NaN);
    end
end

reconMat = cell2mat(reconSamples);
%Do the median normalization here as well (didn't really help)
%reconMat = reconMat - median(reconMat,1);

fullReconMatrix = [extMatrix reconMat];
plotMatrix(fullReconMatrix,[colLabs;newLabs;reconSampLabs],'title','orig-mixed-recon sep');
pcaSamplesNew(fullReconMatrix,'legend',[colLabs;newLabs;reconSampLabs],'dims',[1 2],'title','PCA orig-mixed-recon sep');
pcaSamplesNew(fullReconMatrix,'legend',[colLabs;newLabs;reconSampLabs],'dims',[2 3],'title','PCA orig-mixed-recon sep');

pcaSamplesNew([fullM reconMat],'legend',[colLabs;reconSampLabs],'dims',[1 2],'title','PCA orig-recon sep');
pcaSamplesNew([fullM reconMat],'legend',[colLabs;reconSampLabs],'dims',[2 3],'title','PCA orig-recon sep');


%Try not to get this into separation by samples
nonDistinguishedReconLabs = cell(R,1);
for i=1:R
    nonDistinguishedReconLabs{i} = SampleLabel(0,sprintf('%d',i),'Reconstructed',NaN);
end
fullReconMatrixMerged = [extMatrix G];
plotMatrix(fullReconMatrixMerged,[colLabs;newLabs;nonDistinguishedReconLabs],'title','orig-mixed-recon merged');
pcaSamplesNew(fullReconMatrixMerged,'legend',[colLabs;newLabs;nonDistinguishedReconLabs],'dims',[1 2],'title','PCA orig-mixed-recon merged');
pcaSamplesNew(fullReconMatrixMerged,'legend',[colLabs;newLabs;nonDistinguishedReconLabs],'dims',[2 3],'title','PCA orig-mixed-recon merged');

pcaSamplesNew([fullM G],'legend',[colLabs;nonDistinguishedReconLabs],'dims',[1 2],'title','PCA orig-recon merged');
pcaSamplesNew([fullM G],'legend',[colLabs;nonDistinguishedReconLabs],'dims',[2 3],'title','PCA orig-recon merged');

%Check the protein enrigchment from the components
for i=1:R
    f = fopen(fullfile(tgtDir,sprintf('GeneFactor_%02d.tsv',i)),'w');
    for j=1:size(G,1), fprintf(f,'%s\t%f\n',protIDs{j}.uniProtWOisoform,G(j,i)); end
    fclose(f);
end

%Check the mixing matrix distribution
D = 1-pdist2(mixingMatrix,S,'correlation');
edgeList = zeros(numel(D),3);
for i=1:size(D,1)
    for j=1:size(D,2)
        edgeList(size(D,2)*(i-1)+j,1) = i;
        edgeList(size(D,2)*(i-1)+j,2) = j;
        edgeList(size(D,2)*(i-1)+j,3) = D(i,j);
    end
end
match = maxWeightMatching(edgeList);
figure;
subplot(2,1,1);
imagesc(S);
title('Reconstructed mixing');
subplot(2,1,2);
imagesc(mixingMatrix(match,:));
title('GT mixing');

