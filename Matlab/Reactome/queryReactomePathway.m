function listOfProteins = queryReactomePathway(pathwayID)
%Query Reactome Pathway

%curl -X GET "https://reactome.org/ContentService/data/participants/191273" -H "accept: application/json"

    import matlab.net.*
    import matlab.net.http.*
    r = RequestMessage;   

    %Fetch the pathway name:
    fprintf('Querying pathway proteins from Reactome...\n');
    uri = URI(['https://reactome.org/ContentService/data/participants/' pathwayID ]);
    resp = send(r,uri);

    disp('stop');
end
