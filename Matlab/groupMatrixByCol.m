function [reorgMatrix,ylabs,reorgIdx,reorgMatrixCell,yLabsCell,hierarchIdx,flatMatrixOKL,flatYlabOKL,flatIdxOKL] ...
                        = groupMatrixByCol(inputMatrix,colLabels,groupingFields,varargin)
% GroupMatrixByCol This function groups our matrix based on its column
% prorperties. The column properties are specified by SampleLabel objects
% (or any other structures). The function operaties in hierarchical manner,
% e.g. it first groups the data according to the first grouping field and
% THEN WITHIN thoes groups it further creates subgroups based on the second
% grouping field etc.
% OKL stands for on the kept level

    p = inputParser();
    p.addParameter('groupingOrder',repmat({'none'},1,length(groupingFields)));
    p.addParameter('keepLevel',length(groupingFields));
    if ~isempty(varargin)
        p.parse(varargin{:})
    else
        p.parse();
    end
    keepLevel = p.Results.keepLevel;
    groupingOrder = p.Results.groupingOrder;

    nofCols = length(colLabels);    
    depth = length(groupingFields);
            
    idxArrays = orderByLevel(groupingFields,groupingOrder,1:nofCols,colLabels);    
    reorgIdx = idxArrays;    
    for i=1:depth
        reorgIdx = [reorgIdx{:}];
    end
    yLabOriginal = cell(1,nofCols);
    for i=1:nofCols, yLabOriginal{i} = colLabels{i}.print(); end    
    reorgMatrix = inputMatrix(:,reorgIdx);
    ylabs = yLabOriginal(reorgIdx);
        
    [hierarchIdx,reorgMatrixCell,yLabsCell] = keepIdxFromBottom(idxArrays,inputMatrix,yLabOriginal,depth,keepLevel);
    flatIdxOKL = hierarchIdx;
    flatMatrixOKL = reorgMatrixCell;
    flatYlabOKL = yLabsCell;
    for i=1:keepLevel-1
        flatIdxOKL = [flatIdxOKL{:}];
        flatMatrixOKL = [flatMatrixOKL{:}];
        flatYlabOKL = [flatYlabOKL{:}];
    end    
    
end

function idxArrays = orderByLevel(groupingFields,ordering,idxArray,colLabels)
    if isempty(groupingFields)
        %exit from the recursion
        idxArrays = idxArray;        
        return;
    end
    currField = groupingFields{1};
    fieldVals = cell(1,length(colLabels));
    for j=1:length(colLabels)
        fieldVals{j} = colLabels{j}.(currField);
    end
    if isnumeric(fieldVals{1})
        uniVals = unique(cell2mat(fieldVals));
    else
        uniVals = unique(fieldVals);
    end
    idxArrays = cell(1,length(uniVals));
    if ~strcmp(ordering{1},'none')        
        if iscell(uniVals)
            uniVals = sort(uniVals);
            if strcmp(ordering{1},'descend')
                uniVals = uniVals(end:-1:1);
            end
        else
            uniVals = sort(uniVals,ordering{1});
        end
    end
    for j=1:length(uniVals)
        if iscell(uniVals)
            matchIdx = ismember(fieldVals,uniVals{j});
        else
            matchIdx = cell2mat(fieldVals)==uniVals(j);
        end
        idxArrays{j} = orderByLevel(groupingFields(2:end),ordering(2:end),idxArray(matchIdx),colLabels(matchIdx));
    end
end

function [topIdxArrays,reorgMatrixCell,yLabsCell] = keepIdxFromBottom(idxArrays,inputMatrix,yLabOriginal,depth,keepLevel)
    if keepLevel>0
        topIdxArrays = cell(size(idxArrays));
        reorgMatrixCell = cell(size(idxArrays));
        yLabsCell = cell(size(idxArrays));
        for i=1:length(idxArrays)
            [topIdxArrays{i},reorgMatrixCell{i},yLabsCell{i}] = keepIdxFromBottom(idxArrays{i},inputMatrix,yLabOriginal,depth-1,keepLevel-1);
        end
    else
        topIdxArrays = idxArrays;
        for i=1:depth
            topIdxArrays = [topIdxArrays{:}];
        end
        reorgMatrixCell = inputMatrix(:,topIdxArrays);
        yLabsCell = yLabOriginal(topIdxArrays);
    end
end