classdef ProtData < handle
    
    properties %(Access = 'private')
        %This should contain only raw values (i.e. abundancies in our case)
        dataMatrix = [];
        
        %Cellarray of SampleLabels objects describing the data in
        %correspondance with the COLUMNS of dataMatrix
        colLabels = {};
        
        %Cellarray of ProteinLabels objects describing the proteins in
        %correspondance with the ROWS of the dataMatrix
        rowLabels = {};                        
    end
    
    methods
        function obj = ProtData()
            %Default constructor, creates nothing, data should be added
            %with the addTable method
        end
        
        function addTable(obj,newMatrix,rowIDs,colLabels)
            %addTable: extends the data with the corresponding fields
            %NOTE: use dedicated functions to create the input for these
            %The rowIDs are simple UniProt accession codes while the
            %colLabels needs to be a full object matching the column size
            %of newMatrix
            
            %First step: determine where to store the new data in the
            %matrix (i.e. if there are matching rows already)                                    
            currMatrixSize = size(obj.dataMatrix);
            
            uniProtIDs = obj.getProtLabels('uniProtAccession');
            NN = length(rowIDs);
            newIdxArray = zeros(1,NN);
            for i=1:NN
                [~,newIdxArray(i)] = ismember(rowIDs{i},uniProtIDs);
            end
            newIdx = find(newIdxArray == 0);
            nofNewElements = length(newIdx);
            
            % Extending the ProteinLabel cellarray
            newProtLabels = cell(nofNewElements,1);
            for j=1:nofNewElements
                newProtLabels{j} = ProteinLabel(rowIDs{newIdx(j)});
            end
            obj.rowLabels(end+1:end+nofNewElements,1) = newProtLabels;
            newIdxArray(newIdx) = currMatrixSize(1)+1:currMatrixSize(1)+nofNewElements;
                                                
            % Extending the matrix accordingly
            extMatrix = [[obj.dataMatrix; nan(nofNewElements,currMatrixSize(2))], nan(currMatrixSize(1)+nofNewElements,currMatrixSize(2)+length(colLabels))];
            newColIdx = currMatrixSize(2)+1:currMatrixSize(2)+length(colLabels);
            extMatrix(newIdxArray,newColIdx) = newMatrix;
            
            obj.dataMatrix = extMatrix;
            
            %Extending the column labels too
            obj.colLabels(end+1:end+length(colLabels),1) = colLabels;
                        
        end
        
        function accessCodes = getProtLabels(obj,labelType)
            %returns a cellarray matching with the corresponding labeltype
            %for the currently stored rowLabels. NOTE: the cellarray may
            %contain further cellarrays in case of multiple entries
            
            N = length(obj.rowLabels);
            
            accessCodes = cell(N,1);
            for i=1:N
                accessCodes{i} = obj.rowLabels{i}.(labelType);
            end
        end
        
        function idlist = getStoredSampleCategory(obj,categoryType)            
            
            if strcmp(categoryType,'tissueType') || strcmp(categoryType,'state')
                idlist = {};
            else
                idlist = [];
            end            
            for i=1:length(obj.colLabels)
                idlist = union(idlist,obj.colLabels{i}.(categoryType));
            end
            
        end                                       
        
        function refreshAllFieldsInProteinLabels(obj)
            listOfProtProperties = ProteinLabel.queryPublicFields();
            h = waitbar(0,sprintf('Querying information on protein %s...\n[%d/%d]','',0,length(obj.rowLabels)));
            for i=1:length(obj.rowLabels)
                toQuery = false(1,length(listOfProtProperties));
                for j=1:length(listOfProtProperties)                    
                    if strcmp(obj.rowLabels{i}.(listOfProtProperties{j}),ProteinLabel.NAstring)
                        toQuery(j) = true;
                    end
                end     
                
                if ishandle(h), waitbar(i/length(obj.rowLabels),h,sprintf('Querying information on protein %s...\n[%d/%d]',obj.rowLabels{i}.uniProtAccession,i,length(obj.rowLabels))); end
                if any(toQuery)
                    obj.rowLabels{i}.fillInFields(listOfProtProperties(toQuery));
                end
            end
            if ishandle(h), close(h); end
        end                
        
        function [fetchedMatrix,rL,cL] = fetchData(obj,varargin)
            % The biggest function of ALL, the retrieval of the data in
            % various ways. Returns a matrix and corresponding rL
            % (rowLabels) and cL (columnLabels)
            % The pipeline of this function IS:
            %
            %   1) Apply normalization technique on the WHOLE data (well usually by column)
            %   2) Do data imputation if required
            %   3) Filtering for the required samples
            %   4) Do after filtering imputation
            %
            % Filtering method for columns: the specified parameters are
            % connected with AND logical operation. The default is asking
            % for everything except for treatment where NO treatment is the
            % default. The specified lists are however with OR relation
            % naturally (i.e. if you query for patient 1-2 then for sure it
            % gets everything which is patient 1 OR 2). Treatment may be
            % handled differently later as that is a less defined field.
            % Currently similarly to other fields OR relation is used and
            % it is enough if any of the treatments applied on the sample
            % is the member of the query.
            %
            % Filtering for Proteins is based on a selected ID type
            % specified in the protIDType field and a list of protein IDs
            % given in the protIDs field. It may happen that multiple IDs
            % are connected for the same entry in ProteinLabel. In this
            % case the given row is retrieved if the queried sequence is
            % matching with ANY of it's stored values. However the query
            % can be still only be done by SINGLE values.
            
            %% INPUT PARSING
            
            p = inputParser();
            p.addParameter('patientIDs',obj.getStoredSampleCategory('patientID'));
            p.addParameter('batchIDs',obj.getStoredSampleCategory('batchID'));
            p.addParameter('normType','log-Med',...
                @(x) any(validatestring(x,{...
                        'none','log-Med','log-RelAbu','log-Area',...
                        'Med-log','RelAbu-log','Area-log',...
                        'Med','RelAbu','Area','log'...                        
                    })));
            p.addParameter('tissueTypes',obj.getStoredSampleCategory('tissueType'));
            p.addParameter('states',obj.getStoredSampleCategory('state'));            
            p.addParameter('treatments','NO'); %The default is to fetch samples with NO treatments
            p.addParameter('protIDtype','uniProtAccession',...
                @(x) any(validatestring(x,ProteinLabel.queryPublicFields())));
            p.addParameter('protIDs',obj.getProtLabels('uniProtAccession'));
            p.addParameter('imputeMethod','none',...
                @(x) any(validatestring(x,{...
                        'none','keepFullRows','keepFullCols','removeEmptyRows','removeEmptyCols'...
                        'zero','colmean','rowmean','colmedian','rowmedian'...
                    })));
            p.addParameter('afterFilterImpute','none',...
            @(x) any(validatestring(x,{...
                    'none','keepFullRows','keepFullCols','removeEmptyRows','removeAlmostEmptyRows','removeEmptyCols'...
                    'zero','colmean','rowmean','colmedian','rowmedian'...
                })));
            p.addParameter('almostEmptyValue',5);
            
            if ~isempty(varargin)
                p.parse(varargin{:});
            else
                p.parse();
            end                        
            
            %% NORMALIZAZTION
            
            fullMatrix = obj.dataMatrix;
            l2fm = log2(fullMatrix);
            %Calculating areas
            %The area of the sections needs to be inserted to the
            %colLabels
            areas = zeros(1,length(obj.colLabels));
            for j=1:length(areas)
                areas(j) = sum(obj.colLabels{j}.sampleAreas);
            end
            
            switch p.Results.normType
                case 'none'
                    normMatrix = fullMatrix;
                case 'log-Med'                    
                    normMatrix = l2fm - nanmedian(l2fm);                
                case 'log-RelAbu'
                    normMatrix = l2fm./nansum(l2fm); %Needs testing if it eats it
                case 'log-Area'                    
                    normMatrix = l2fm./areas;
                case 'Med-log'
                    medC = fullMatrix - nanmedian(fullMatrix);
                    normMatrix = log2(medC);
                case 'RelAbu-log'
                    relAbu = fullMatrix./nansum(fullMatrix);
                    normMatrix = log2(relAbu);
                case 'Area-log'                    
                    areaDiv = fullMatrix./areas;
                    normMatrix = log2(areaDiv);
                case 'Med'
                    normMatrix = fullMatrix - nanmedian(fullMatrix);
                case 'RelAbu'
                    normMatrix = fullMatrix./nansum(fullMatrix);
                case 'Area'
                    normMatrix = fullMatrix./areas;                
                case 'log'
                    normMatrix = log2(fullMatrix);                
            end
            
            
           %% DATA IMPUTATION  #1
           keptProts = 1:length(obj.rowLabels);
           keptSamples = 1:length(obj.colLabels);
            switch p.Results.imputeMethod
                case 'none'
                    imputedMatrix = normMatrix;
                case 'keepFullRows'
                    filterArray = ~any(isnan(normMatrix),2);
                    imputedMatrix = normMatrix(filterArray,:);
                    keptProts(~filterArray) = [];
                case 'keepFullCols'
                    filterArray = ~any(isnan(normMatrix),1);
                    imputedMatrix = normMatrix(:,filterArray);
                    keptSamples(~filterArray) = [];
                case 'removeEmptyRows'
                    filterArray = ~all(isnan(normMatrix),2);
                    imputedMatrix = normMatrix(filterArray,:);
                    keptProts(~filterArray) = [];
                case 'removeAlmostEmptyRows'
                    filterArray = sum(~isnan(normMatrix),2)>p.Results.almostEmptyValue;
                    imputedMatrix = normMatrix(filterArray,:);
                    keptProts(~filterArray) = [];                    
                case 'removeEmptyCols'
                    filterArray = ~all(isnan(normMatrix),1);
                    imputedMatrix = normMatrix(:,filterArray);
                    keptSamples(~filterArray) = [];
                case 'zero'
                    imputedMatrix = normMatrix;
                    imputedMatrix(isnan(normMatrix)) = 0;
                case 'colmean'
                    imputedMatrix = normMatrix;
                    dimAgg = nanmean(normMatrix,1);
                    for j=1:length(dimAgg), imputedMatrix(isnan(normMatrix(:,j)),j) = dimAgg(j); end
                case 'colmedian'
                    imputedMatrix = normMatrix;
                    dimAgg = nanmedian(normMatrix,1);
                    for j=1:length(dimAgg), imputedMatrix(isnan(normMatrix(:,j)),j) = dimAgg(j); end
                case 'rowmean'
                    imputedMatrix = normMatrix;
                    dimAgg = nanmean(normMatrix,2);
                    for j=1:length(dimAgg), imputedMatrix(j,isnan(normMatrix(j,:))) = dimAgg(j); end
                case 'rowmedian'
                    imputedMatrix = normMatrix;
                    dimAgg = nanmedian(normMatrix,2);
                    for j=1:length(dimAgg), imputedMatrix(j,isnan(normMatrix(j,:))) = dimAgg(j); end
            end
            
            %% FILTERING
            
            % First check for matching proteins based on the provided IDs
            % Have to handle cases for multiple entries
            storedProtIDs = obj.getProtLabels(p.Results.protIDtype);
            storedProtIDs = storedProtIDs(keptProts);
            queriedProtIDs = p.Results.protIDs;
            if ~all(cellfun(@ischar,storedProtIDs))
                protMatchArray = false(length(storedProtIDs),1);
                for i=1:length(storedProtIDs)
                    protMatchArray(i) = any(ismember(storedProtIDs{i},queriedProtIDs));
                end
            else
                protMatchArray = ismember(storedProtIDs,queriedProtIDs);
            end            
            
            % Then do filtering on the columns. Treatment is handled
            % separately
            sampleMatchArray = true(1,length(keptSamples));
            paramsToIterateThrough = {'patientIDs','batchIDs','tissueTypes','states'};
            for i=1:length(paramsToIterateThrough)
                queried = p.Results.(paramsToIterateThrough{i});
                for j=1:length(sampleMatchArray)
                    %end-1 so as to get rid of the trailing s
                    storedVal = obj.colLabels{keptSamples(j)}.(paramsToIterateThrough{i}(1:end-1));
                    if ~ismember(storedVal,queried)
                        sampleMatchArray(j) = false;
                    end
                end
            end
            
            %Treatment separately
            queried = p.Results.treatments;
            for j=1:length(sampleMatchArray)                
                if ~iscell(queried) && ischar(queried) && strcmp(queried,'NO')
                    if ~isempty(obj.colLabels{j}.treatments)
                        sampleMatchArray(j) = false;
                    end
                elseif ~any(ismember(obj.colLabels{keptSamples(j)}.treatments,queried))
                    %Currently treatments are checked similarly to other
                    %fields. NOTE: it's enough if ANY of the applied
                    %treatments is in the query list.                    
                    sampleMatchArray(j) = false;                    
                end
            end
            
            keptProts  = keptProts(protMatchArray);
            keptSamples = keptSamples(sampleMatchArray);
            filteredMatrix = imputedMatrix(protMatchArray,sampleMatchArray);
            
            %% DATA IMPUTATION  #2           
            switch p.Results.afterFilterImpute
                case 'none'
                    postImputedMatrix = filteredMatrix;
                case 'keepFullRows'
                    filterArray = ~any(isnan(filteredMatrix),2);
                    postImputedMatrix = filteredMatrix(filterArray,:);
                    keptProts(~filterArray) = [];
                case 'keepFullCols'
                    filterArray = ~any(isnan(filteredMatrix),1);
                    postImputedMatrix = filteredMatrix(:,filterArray);
                    keptSamples(~filterArray) = [];
                case 'removeEmptyRows'
                    filterArray = ~all(isnan(filteredMatrix),2);
                    postImputedMatrix = filteredMatrix(filterArray,:);
                    keptProts(~filterArray) = [];
                case 'removeAlmostEmptyRows'
                    filterArray = sum(~isnan(filteredMatrix),2)>p.Results.almostEmptyValue;
                    postImputedMatrix = filteredMatrix(filterArray,:);
                    keptProts(~filterArray) = [];                    
                case 'removeEmptyCols'
                    filterArray = ~all(isnan(filteredMatrix),1);
                    postImputedMatrix = filteredMatrix(:,filterArray);
                    keptSamples(~filterArray) = [];
                case 'zero'
                    postImputedMatrix = filteredMatrix;
                    postImputedMatrix(isnan(filteredMatrix)) = 0;
                case 'colmean'
                    postImputedMatrix = filteredMatrix;
                    dimAgg = nanmean(filteredMatrix,1);
                    for j=1:length(dimAgg), postImputedMatrix(isnan(filteredMatrix(:,j)),j) = dimAgg(j); end
                case 'colmedian'
                    postImputedMatrix = filteredMatrix;
                    dimAgg = nanmedian(filteredMatrix,1);
                    for j=1:length(dimAgg), postImputedMatrix(isnan(filteredMatrix(:,j)),j) = dimAgg(j); end
                case 'rowmean'
                    postImputedMatrix = filteredMatrix;
                    dimAgg = nanmean(filteredMatrix,2);
                    for j=1:length(dimAgg), postImputedMatrix(j,isnan(filteredMatrix(j,:))) = dimAgg(j); end
                case 'rowmedian'
                    postImputedMatrix = filteredMatrix;
                    dimAgg = nanmedian(filteredMatrix,2);
                    for j=1:length(dimAgg), postImputedMatrix(j,isnan(filteredMatrix(j,:))) = dimAgg(j); end
            end
            
            % Store row labels
            rL = obj.rowLabels(keptProts);
            
            cL = obj.colLabels(keptSamples);
            
            fetchedMatrix = postImputedMatrix;
            
        end                                        
               
    end        
end