classdef SampleLabel
    
    properties
        % bool to indicate if this is a real data column OR a metadata
        % column such as protein information. In case it is a metaColumn it
        % should store an extra field specifying what type of meta info it
        % is (e.g. Coverage, #Peptides etc.)
        % Reserved for further usage.
        metaCol;
                
        patientID;
        tissueType;
        state;
        treatments;
        batchID;                
        
        metaColType;
        
        %Each sample was originally a set of dissected tissue sections, the
        %area of this should come here
        sampleAreas;
                
    end
    
    methods
        function obj = SampleLabel(patientID,tissueType,state,batchID,varargin)
            p = inputParser();
            p.addParameter('treatments',{});
            p.addParameter('metaCol',false);
            p.addParameter('metaColType','');
            p.addParameter('sampleAreas',[]);
            
            %Constructor, maybe change this later for parse args
            obj.patientID = patientID;
            obj.tissueType = tissueType;
            obj.state = state;
            obj.batchID = batchID;
            
            p.parse(varargin{:});
            obj.metaCol = p.Results.metaCol;
            obj.metaColType = p.Results.metaColType;
            obj.treatments = p.Results.treatments;
            obj.sampleAreas = p.Results.sampleAreas;
        end
        
        function str = print(obj,formatSpec)
            if nargin < 2
                formatSpec = 'plain';
            end
            
            switch formatSpec
                case 'plain'
                    str = [upper(obj.state(1)) upper(obj.tissueType(1)) '-P' num2str(obj.patientID) ];
                case 'full'
                    str = [obj.state ' ' obj.tissueType '; '...
                           'patient ' num2str(obj.patientID) '; '...
                           'batch ' num2str(obj.batchID) '; '...
                           'treatments: ' strjoin(obj.treatments,' ')];
                case 'stateAndType-short'
                    str = [upper(obj.state(1)) upper(obj.tissueType(1))];
            end
        end
    end
end

