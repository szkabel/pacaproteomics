%batchID should come from outside

%NOTE: the script is s  pecific for the sheet that we load! REDO THIS BY COPY
%PASTE ALL THE TIME! THIS IS REQUIRED TO MANUALLY CHECK ALL INFO IN THIS
%FILE TO BE CORRESPONDANT WITH THE ACTUAL DATA.

xlsPath = 'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\';
xlsName = '190903_Pancreas_cancer_Proteins_list.xlsx';
sheetName = 'Raw data';

%Load in everything, maybe the identified objects can be used for filling
%out those rows.
T_loaded = readtable(fullfile(xlsPath,xlsName),'Sheet',sheetName);

protIDs = T_loaded.Accession;

nofSamples = 20;
columnIDs = cell(nofSamples,1);
dataMatrix = zeros(length(protIDs),nofSamples);

%Go through one-by-one on the 20 samples
i = 1;

%Neoplastic parenchyma
injectedAreas = [...
    452407.7794 ...
    747676.3183 ...
    277169.7338 ...
    894814.6012 ...
    545963.3967 ...
 ];
for j=1:5
    dataMatrix(:,i) = T_loaded.(['NP_P' num2str(j) '_abundance']);
    columnIDs{i} = SampleLabel(j,'parenchyma','neoplastic',batchID,'sampleAreas',injectedAreas(j));
    i = i + 1;
end

%Healthy parenchyma
injectedAreas = [...
    689076.9702 ...
    1274600.887 ...
    444777.6602 ...
    555573.2277 ...
    730418.6042 ...
 ];
for j=1:5
    dataMatrix(:,i) = T_loaded.(['HA_P' num2str(j) '_abundance']);
    columnIDs{i} = SampleLabel(j,'parenchyma','healthy',batchID,'sampleAreas',injectedAreas(j));
    i = i + 1;
end

%Neoplastic stroma
injectedAreas = [...
    455110.6093 ...
    428993.5621 ...
    0 ...
    641299.8812 ...
    625124.1617 ...
 ];
for j=[1 2 4 5]
    dataMatrix(:,i) = T_loaded.(['NS_P' num2str(j) '_abundance']);
    columnIDs{i} = SampleLabel(j,'stroma','neoplastic',batchID,'sampleAreas',injectedAreas(j));
    i = i + 1;
end

%Healhty stroma
injectedAreas = [...
    505828.23
    449213.2116
    605578.5005
    613834.8574
    666405.9459
 ];
for j=1:5
    dataMatrix(:,i) = T_loaded.(['HS_P' num2str(j) '_abundance']);
    columnIDs{i} = SampleLabel(j,'stroma','healthy',batchID,'sampleAreas',injectedAreas(j));
    i = i + 1;
end

%Plus one extra column for HNE
injectedArea = 216797.5577;
dataMatrix(:,i) = T_loaded.HA_P1_HNE_abundance;
columnIDs{i} = SampleLabel(1,'parenchyma','healthy',batchID,'treatments',{'HNE'},'sampleAreas',injectedArea);
