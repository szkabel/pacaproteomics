%batchID should come from outside

%NOTE: the script is s  pecific for the sheet that we load! REDO THIS BY COPY
%PASTE ALL THE TIME! THIS IS REQUIRED TO MANUALLY CHECK ALL INFO IN THIS
%FILE TO BE CORRESPONDANT WITH THE ACTUAL DATA.

xlsPath = 'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\';
xlsName = '20200323_Panc_Patient06-14_allPatient_Proteins.xlsx';
sheetName = 'Raw_data';

%Load in everything, maybe the identified objects can be used for filling
%out those rows.
T_loaded = readtable(fullfile(xlsPath,xlsName),'Sheet',sheetName);

protIDs = T_loaded.Accession;

nofSamples = 36;
columnIDs = cell(nofSamples,1);
dataMatrix = zeros(length(protIDs),nofSamples);

%Go through one-by-one on the 20 samples
i = 1;

%Neoplastic parenchyma
%Put something fake here, as we don't have proper info now
injectedAreas = ones(9,1);
slideNums = [1 2 1 1 2 1 1 1 2];
patientNums = 6:14;
for j=1:length(patientNums)
    dataMatrix(:,i) = T_loaded.(['NP' num2str(patientNums(j),'%02d') '_' num2str(slideNums(j),'%d')]);
    columnIDs{i} = SampleLabel(patientNums(j),'parenchyma','neoplastic',batchID,'sampleAreas',injectedAreas(j));
    i = i + 1;
end

%Healthy parenchyma
injectedAreas = ones(9,1);
slideNums = [1 2 1 2 1 2 1 1 2];
patientNums = 6:14;
for j=1:length(patientNums)
    dataMatrix(:,i) = T_loaded.(['HP' num2str(patientNums(j),'%02d') '_' num2str(slideNums(j),'%d')]);
    columnIDs{i} = SampleLabel(patientNums(j),'parenchyma','healthy',batchID,'sampleAreas',injectedAreas(j));
    i = i + 1;
end

%Neoplastic stroma
injectedAreas = ones(9,1);
slideNums = [1 2 1 1 2 1 1 1 2];
patientNums = 6:14;
for j=1:length(patientNums)
    dataMatrix(:,i) = T_loaded.(['NS' num2str(patientNums(j),'%02d') '_' num2str(slideNums(j),'%d')]);
    columnIDs{i} = SampleLabel(patientNums(j),'stroma','neoplastic',batchID,'sampleAreas',injectedAreas(j));
    i = i + 1;
end

%Healhty stroma
injectedAreas = ones(9,1);
slideNums = [1 2 1 2 1 2 1 1 2];
patientNums = 6:14;
for j=1:length(patientNums)
    dataMatrix(:,i) = T_loaded.(['HS' num2str(patientNums(j),'%02d') '_' num2str(slideNums(j),'%d')]);
    columnIDs{i} = SampleLabel(patientNums(j),'stroma','healthy',batchID,'sampleAreas',injectedAreas(j));
    i = i + 1;
end