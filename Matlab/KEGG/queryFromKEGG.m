import matlab.net.*
import matlab.net.http.*
r = RequestMessage;

% Write here the code of the pathway
% 05212 is the code for the PaCa pathway in kegg
pathwayID = 'hsa05212';
cmap = jet(128);

%Fetch the pathway name:
fprintf('Querying pathway name from KEGG...\n');
uri = URI(['http://rest.kegg.jp/list/' pathwayID ]);
resp = send(r,uri);
pathwayName = strsplitN(resp.Body.Data,'\t',2);
pathwayName = pathwayName(1:end-1); %Trim off the \n from the end
fprintf('Returned with status %d - %s\nIdentified pathway is: %s\n',resp.StartLine.StatusCode,resp.StartLine.ReasonPhrase,pathwayName);

%Fetch all genes in the pathway
fprintf('Querying %s from KEGG...\n',pathwayName);
uri = URI(['http://rest.kegg.jp/link/hsa/' pathwayID]);
resp = send(r,uri);
%Parse data. NOTE: filtering is done here to avoid pathways that are linked
%to the pathway of interest
fullEntries = strsplit(resp.Body.Data,{'\n','\t'});
notNeededIdx = strcmp(fullEntries,['path:' pathwayID]) | cellfun(@isempty,fullEntries);
protsInPath = fullEntries(~notNeededIdx)';
fprintf('Returned with status %d\nNumber of identified genes in %s is: %d\n',resp.StartLine.StatusCode,pathwayName,length(protsInPath));

%COMMENT TO BE REMOVED IF RUN FROM SCRATCH!
KEGG_IDQueries; %Run longer script

%Note: loadProtData needs to be run in advance
genesWithAbundanceWOiso = cell(length(genesWithAbundance),1);
for i=1:length(genesWithAbundance), pss = strsplit(genesWithAbundance{i},'-'); genesWithAbundanceWOiso{i} = pss{1}; end

%Get a list of the PaCa proteins that are found in our data
PaCaProteinsInOurData = intersect(genesWithAbundanceWOiso,protsInUniProt);
matchNumber = length(PaCaProteinsInOurData);
%Get the indices of these in the original dataset.
pacaIdx = find(ismember(genesWithAbundanceWOiso,PaCaProteinsInOurData));

%Get the indices of these in the PaCa protein list SO THAT THEIR ORDER IS
%MATCHED WITH THE pacaIdx!
foundPaCaIdx = zeros(matchNumber,1);
for i=1:matchNumber
    uniprotID = genesWithAbundanceWOiso{pacaIdx(i)};
    foundPaCaIdx(i) = find(strcmp(uniprotID,protsInUniProt));
end
%THIS IS IMPORTANT

PaCaProteinsInOurData_humanReadable = humanReadable(foundPaCaIdx);
realHumanReadableLegend = cell(matchNumber,1);
for i=1:matchNumber
    splitComma = strsplit(PaCaProteinsInOurData_humanReadable{i},',');
    realHumanReadableLegend{i} = [splitComma{1} ' - ' protsInUniProt{foundPaCaIdx(i)} ' - ' protsInPath{foundPaCaIdx(i)} ];
end

reducedReOrgStat = reorgStat(pacaIdx,:);
figure; imagesc(reducedReOrgStat);
colormap(cmap);
title([ pathwayName ' prots in our data']);

nofPatients = 5;
types = {'NP','HA','NS','HS'};
xlab = cell(20,1);
for i=1:nofPatients, for j=1:length(types), xlab{(i-1)*length(types)+j} = ['P' num2str(i) '-' types{j} ]; end, end
xlab(11) = []; %We are missing neoplastic stroma from patient 3
xticks(1:19); xticklabels(xlab); xtickangle(90)
yticks(1:matchNumber); yticklabels(realHumanReadableLegend);

% Create the matrix as Elina requested (i.e. ticks with human readable + reorganization by tissue type - this is the original format)
idxNames = {'NP','HA','NS','HS'};
figure;

for i=length(idxNames):-1:1
    subplot(1,length(idxNames),i);
    currentData = statMatrix(pacaIdx,eval([idxNames{i} '_statIdx']));
    imagesc(currentData,[-6,6]); %6 as a custom limit for the values
    colormap(cmap);
    title(idxNames{i});    
end
yticks(1:matchNumber); yticklabels(realHumanReadableLegend);
subplot(1,length(idxNames),length(idxNames)); colorbar;

%Create the string query for the website:
hsaIDs = cell(matchNumber,1);
fprintf('The link for the figure is:\n%s',['https://www.genome.jp/kegg-bin/show_pathway?' pathwayID]);
for i=1:matchNumber
   tmp = strsplit(protsInPath{foundPaCaIdx(i)},':');
   hsaIDs{i} = tmp{2};
   fprintf('+%s',hsaIDs{i});
end
fprintf('\n');

%% Differences plot

%Create matrix with the differences between values: manually, as there is
%one column missing
diff_reorgStat = zeros(matchNumber,nofPatients*2-1); %-1 missing column
batchNum = 1;
diff_reorgStat(:,(batchNum-1)*2+1) = minus(reducedReOrgStat(:,(batchNum-1)*4+1),reducedReOrgStat(:,(batchNum-1)*4+2));
diff_reorgStat(:,(batchNum-1)*2+2) = minus(reducedReOrgStat(:,(batchNum-1)*4+3),reducedReOrgStat(:,(batchNum-1)*4+4));
batchNum = 2;
diff_reorgStat(:,(batchNum-1)*2+1) = minus(reducedReOrgStat(:,(batchNum-1)*4+1),reducedReOrgStat(:,(batchNum-1)*4+2));
diff_reorgStat(:,(batchNum-1)*2+2) = minus(reducedReOrgStat(:,(batchNum-1)*4+3),reducedReOrgStat(:,(batchNum-1)*4+4));
batchNum = 3;
diff_reorgStat(:,(batchNum-1)*2+1) = minus(reducedReOrgStat(:,(batchNum-1)*4+1),reducedReOrgStat(:,(batchNum-1)*4+2));
batchNum = 4;
diff_reorgStat(:,(batchNum-1)*2+1-1) = minus(reducedReOrgStat(:,(batchNum-1)*4+1-1),reducedReOrgStat(:,(batchNum-1)*4+2-1));
diff_reorgStat(:,(batchNum-1)*2+2-1) = minus(reducedReOrgStat(:,(batchNum-1)*4+3-1),reducedReOrgStat(:,(batchNum-1)*4+4-1));
batchNum = 5;
diff_reorgStat(:,(batchNum-1)*2+1-1) = minus(reducedReOrgStat(:,(batchNum-1)*4+1-1),reducedReOrgStat(:,(batchNum-1)*4+2-1));
diff_reorgStat(:,(batchNum-1)*2+2-1) = minus(reducedReOrgStat(:,(batchNum-1)*4+3-1),reducedReOrgStat(:,(batchNum-1)*4+4-1));

figure; imagesc(diff_reorgStat);
xticks(1:9); xticklabels({'P1-P','P1-S','P2-P','P2-S','P3-P','P4-P','P4-S','P5-P','P5-S'}); xtickangle(90);
yticks(1:matchNumber); yticklabels(realHumanReadableLegend);
title(sprintf('Abundance difference: Neoplastic-Healthy (%s)',pathwayName));
colormap(cmap);
colorbar;

%Differences plot in another way:

%-1's are for the missing column in Patient 3
diff_stat = zeros(matchNumber,nofPatients*2-1);
diff_stat(:,1:nofPatients) = statMatrix(pacaIdx,NP_statIdx)-statMatrix(pacaIdx,HA_statIdx);
diff_stat(:,nofPatients+1:2*nofPatients-1) = statMatrix(pacaIdx,NS_statIdx)-statMatrix(pacaIdx,HS_statIdx([1 2 4 5]));
figure; imagesc(diff_stat);
xticks(1:9); xticklabels({'P1-P','P2-P','P3-P','P4-P','P5-P','P1-S','P2-S','P4-S','P5-S'}); xtickangle(90);
yticks(1:matchNumber); yticklabels(realHumanReadableLegend);
title(sprintf('Abundance difference: Neoplastic-Healthy (%s)',pathwayName));
colormap(cmap);
colorbar;



