import matlab.net.*
import matlab.net.http.*

protsInUniProt = cell(length(protsInPath),1);
humanReadable = cell(length(protsInPath),1);

%Convert all these protein IDs to the uniprot ID
h = waitbar(0,sprintf('[%.0f%%] - Querying UniProt IDs for\n%s\nproteins...\n',0,pathwayName));
for i=1:length(protsInPath)
    uri = URI(sprintf('http://rest.kegg.jp/conv/uniprot/%s',protsInPath{i}));    
    resp = send(r,uri);    
    %Check if the KEGG convert worked
    if length(resp.Body.Data.char)>1
        byField = strsplit(resp.Body.Data);
        inField = strsplit(byField{2},':'); %2 as the second column is the uniprot id
        protsInUniProt{i} = inField{2}; %2 becase it has the format up:ID        
    else        
        fprintf('Empty message, skipping gene #%d - %s',i,protsInPath{i});
        protsInUniProt{i} = 'Not Found';
    end
    
    if resp.StartLine.StatusCode~=200
        fprintf('Wrong answer from network:\nStatus code %d, Message:\n%s\n',resp.StartLine.StatusCode,resp.StartLine.ReasonPhrase);
    end    
    prcnt = i/length(protsInPath);
    if ishandle(h), waitbar(prcnt,h,sprintf('[%.0f%%] - Querying UniProt IDs for\n%s\nproteins...\n',prcnt*100,pathwayName)); end
end
if ishandle(h), close(h); end

%Convert also to human (more precisely biologist) readable format
h = waitbar(0,sprintf('[%.0f%%] - Querying gene names for\n%s\nproteins...\n',0,pathwayName));
for i=1:length(protsInPath)
    uri = URI(sprintf('http://rest.kegg.jp/list/%s',protsInPath{i}));
    resp = send(r,uri);        
    byField = strsplit(resp.Body.Data,'\t');
    inField = strsplit(byField{2},';'); %2 as the second column is the uniprot id
    humanReadable{i} = inField{1};    
    
    if resp.StartLine.StatusCode~=200
        fprintf('Wrong answer from network:\nStatus code %d, Message:\n%s\n',resp.StartLine.StatusCode,resp.StartLine.ReasonPhrase);
    end    
    prcnt = i/length(protsInPath);
    if ishandle(h), waitbar(prcnt,h,sprintf('[%.0f%%] - Querying gene names for\n%s\nproteins...\n',prcnt*100,pathwayName)); end
end

if ishandle(h), close(h); end
