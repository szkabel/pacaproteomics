function [fig,linkText] = queryKEGGPathway(PD,pathwayID)
%Creates an organized figure with human readable protein names filtered for
%only a specific KEGG pathway which ID can be feeded in the second
%parameter. The first parameter is a ProteinData class containing all
%proteins for our experiment.

    import matlab.net.*
    import matlab.net.http.*
    r = RequestMessage;

    % Write here the code of the pathway
    % 05212 is the code for the PaCa pathway in kegg
    if nargin<2
        pathwayID = 'hsa05212';
    end
    cmap = jet(128);

    %Fetch the pathway name:
    fprintf('Querying pathway name from KEGG...\n');
    uri = URI(['http://rest.kegg.jp/list/' pathwayID ]);
    resp = send(r,uri);
    pathwayName = strsplitN(resp.Body.Data,'\t',2);
    pathwayName = pathwayName(1:end-1); %Trim off the \n from the end
    fprintf('Returned with status %d - %s\nIdentified pathway is: %s\n',resp.StartLine.StatusCode,resp.StartLine.ReasonPhrase,pathwayName);

    %Fetch all genes in the pathway
    fprintf('Querying %s from KEGG...\n',pathwayName);
    uri = URI(['http://rest.kegg.jp/link/hsa/' pathwayID]);
    resp = send(r,uri);
    %Parse data. NOTE: filtering is done here to avoid pathways that are linked
    %to the pathway of interest
    fullEntries = strsplit(resp.Body.Data,{'\n','\t'});
    notNeededIdx = strcmp(fullEntries,['path:' pathwayID]) | cellfun(@isempty,fullEntries);
    protsInPath = fullEntries(~notNeededIdx)';
    fprintf('Returned with status %d\nNumber of identified genes in %s is: %d\n',resp.StartLine.StatusCode,pathwayName,length(protsInPath));
    
    %Fetch the indices from the PD
    [matrixOfInterest,protLabs,sampleLabs] = PD.fetchData('protIDtype','keggID','protIDs',protsInPath);
    
    %Create the requested grouping
    [~,~,~,~,~,~,matrixArray,labelArray,colIdxArray] = groupMatrixByCol(matrixOfInterest,sampleLabs,{'tissueType','state'},'groupingOrder',{'ascend','descend'});
    
    %Do the plot
    fig = figure('units','normalized','outerposition',[0 0 1 1]);
    nofCat = length(matrixArray);
    for i=nofCat:-1:1
        subplot(1,nofCat,i);
        imagesc(matrixArray{i},[-6,6]); %6 as a custom limit for the values
        colormap(cmap);
        xticks(1:length(labelArray{i}));
        xticklabels(labelArray{i});
        xtickangle(90);
    end
    matchNumber = length(protLabs);
    xlab = cell(1,matchNumber);
    for j=1:matchNumber, xlab{j} = [protLabs{j}.humanReadable '-' protLabs{j}.uniProtAccession]; end
    yticks(1:matchNumber); yticklabels(xlab);
    subplot(1,nofCat,nofCat); colorbar;
    
    sgtitle(sprintf('Proteins in our data from\n%s',pathwayName));
    %Creating the link for kegg
    
    hsaIDs = cell(matchNumber,1);
    linkText = [];
    linkText = [linkText sprintf('%s',['https://www.genome.jp/kegg-bin/show_pathway?' pathwayID])];
    fprintf('The link for the figure is:\n%s',['https://www.genome.jp/kegg-bin/show_pathway?' pathwayID]);
    for i=1:matchNumber
        tmp = strsplit(protLabs{i}.keggID,':');
        hsaIDs{i} = tmp{2};
        linkText = [linkText sprintf('+%s',hsaIDs{i})];
        fprintf('+%s',hsaIDs{i});
    end
    fprintf('\n');

        
end

