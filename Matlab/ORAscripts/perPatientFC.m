[fullMatrix,protLabs,colLabs] = PD.fetchData();

protLabsUniprotWOiso = cell(size(protLabs));

for i=1:length(protLabsUniprotWOiso)
    protLabsUniprotWOiso{i} = protLabs{i}.uniProtWOisoform;        
end

tgtDir = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\perPatientFC';

[reorgMatrix,ylabs,reorgIdx,reorgMatrixCell,yLabsCell,hierarchIdx,flatMatrixOKL,flatYlabOKL,flatIdxOKL] = groupMatrixByCol(fullMatrix,colLabs,{'patientID'});

compType = {'HP','HS'};
strType = strjoin(compType,'_');
compDir = fullfile(tgtDir,strType);
if ~exist(compDir,'dir'), mkdir(compDir); end

for i=1:length(hierarchIdx) %to nof patients
    currLabs = yLabsCell{i};
    patientID = strsplitN(currLabs{1},'-',2);
    
    %This is burnt in 2
    idx1 = find(startsWith(currLabs,compType{1}));
    idx2 = find(startsWith(currLabs,compType{2}));
    
    if ~isempty(idx1) && ~isempty(idx2) 
        colIdx1 = hierarchIdx{i}(idx1);
        colIdx2 = hierarchIdx{i}(idx2);
        
        FCvals = fullMatrix(:,colIdx1)-fullMatrix(:,colIdx2);
        filterIdx = ~isnan(FCvals);
        T = table(protLabsUniprotWOiso(filterIdx),FCvals(filterIdx));
        writetable(T,fullfile(compDir,[strType '_' patientID '.txt']),'WriteVariableNames',false);
        resDir = fullfile(compDir,[strType '_' patientID]);
        if ~exist(resDir,'dir'), mkdir(resDir); end
    end    
end

