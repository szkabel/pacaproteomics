function resTable = stringEnrichmentRetrieval(tgtDir,protList)
    %Retrieves the functional enrichment from string
    
    acceptedRange = [1,500];
    if length(protList)<acceptedRange(1) || length(protList)>acceptedRange(2)
        fprintf('SKIPPING STRING RETRIEVAL, number of altered proteins (%d) is our of range [%d,%d]\n',length(protList),acceptedRange(1),acceptedRange(2));
        resTable = [];
        return;
    end
        
    
    fnID = strsplit(tgtDir,filesep);
    fileNameID = fnID{end};
        
    if exist(fullfile(tgtDir,[fileNameID '_stringEnrichmentSystematic.csv']),'file') && ...
       exist(fullfile(tgtDir,[fileNameID '_networkImage.png']),'file')
       resTable = readtable(fullfile(tgtDir,[fileNameID '_stringEnrichmentSystematic.csv']));
       return;
    end
    
    import matlab.net.*
    import matlab.net.http.*
    r = RequestMessage;       

    %Fetch the pathway name:    
    uri = URI(['https://string-db.org/api/tsv/enrichment?identifiers=' strjoin(protList,'%0d') '&species=9606']);    
    resp = sendRequestToString(r,uri);
    if ~strcmp(resp.StatusCode,'OK')
        resTable = [];
        return;
    end
    
    byLines = strsplit(resp.Body.Data,'\n');
    resCell = cellfun(@strsplit,byLines,repmat({'\t'},1,length(byLines)),'UniformOutput',false)';
    resCell(end) = [];
    
    %Create a proper table
    initRow = repmat({{}},1,length(resCell{1}));
    resTable = table(initRow{:},'VariableNames',resCell{1});
    for i=2:length(resCell)
        resTable(i-1,:) = resCell{i};
    end
        
    writetable(resTable,fullfile(tgtDir,[fileNameID '_stringEnrichmentSystematic.csv']));
    
    %Fetching image as well
    
    uri = URI(['https://string-db.org/api/highres_image/network?identifiers=' strjoin(protList,'%0d') '&species=9606']);    
    resp = sendRequestToString(r,uri);
    if strcmp(resp.StatusCode,'OK')       
        imwrite(resp.Body.Data{1},fullfile(tgtDir,[fileNameID '_networkImage.png']), 'png', 'Alpha', resp.Body.Data{3});
    end        
        
end

function resp = sendRequestToString(r,uri)
    httpOptions = matlab.net.http.HTTPOptions('ConnectTimeout',60);
    success = false;
    currWaitTime = 2;
    while ~success
        try        
            resp = send(r,uri,httpOptions);
            pause(rand*2); %Wait a bit before shutting the STRING server down
            success = true;
        catch err
            if strcmp(err.identifier,'MATLAB:webservices:Timeout')
                fprintf('Error. Wait time: %.0f sec\n',currWaitTime);
                pause(rand*currWaitTime);
                currWaitTime = currWaitTime + 2;
            else
                retrhow(err);
            end
        end    
    end
end

