function createEmptyStringResultFolders(tgtDir)
    txts = dir(fullfile(tgtDir,'*.txt'));
    for i=1:numel(txts)
        [~,fileExEx,~] = fileparts(txts(i).name);
        if ~isfolder(fullfile(tgtDir,fileExEx))
            mkdir(fullfile(tgtDir,fileExEx));
        end
    end
end