%Perform a full-string retrieval for an ORA folder.
function getStringForFolder(wrkDir, ext)

if nargin<1
    wrkDir = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\Output\Batch2\ORA-UpDownTexts\_normType_log-Med__imputeMethod_removeEmptyRows_p.010_fldChg1.00e+00';
end
if nargin<2
    ext = 'txt';
end

createEmptyStringResultFolders(wrkDir);

txtList = dir(fullfile(wrkDir,['*.' ext]));

for i=1:numel(txtList)
    currFileName = txtList(i).name;
    [~,currExEx,~] = fileparts(currFileName);
    currResDir = fullfile(wrkDir,currExEx);
    if ~exist(currResDir,'dir'), mkdir(currResDir); end
            
    T = readtable(fullfile(wrkDir,currFileName), 'ReadVariableNames',false);
    protList = {T.Var1{:}};
    stringEnrichmentRetrieval(currResDir,protList);
end

end