function compTable = calcORAMatchScores(foldA,foldB)
%Takes 2 folders with the corresponding terms exported from string.
%Then it calculates the matches on them and returns a table.

%The 4 different case
%{
 calcMatchScores(...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\ORA_NP_HA\NoIso',...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\NP_HA_recalc_NoIso');

calcMatchScores(...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\ORA_NP_HA\raw',...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\NP_HA_recalc');

calcMatchScores(...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\ORA_NP_HA\raw',...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\ORA_NP_HA\NoIso');

calcMatchScores(...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\NP_HA_recalc',...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\NP_HA_recalc_NoIso');
%}

fileNames = {...
    'enrichment.Component.tsv';...
    'enrichment.Function.tsv';...
    'enrichment.KEGG.tsv';...
    'enrichment.Process.tsv';...
    'enrichment.RCTM.tsv';...
    };

nofDBs = length(fileNames);

termIDsA = cell(nofDBs,1);
termIDsB = cell(nofDBs,1);

variableNames = {'fileName','jacc','A_cov','A_size','B_cov','B_size'};

compTable = table({},[],[],[],[],[],'VariableNames',variableNames);

for i=1:nofDBs
    termIDsA{i} = extractTermIDs(foldA,fileNames{i});    
    termIDsB{i} = extractTermIDs(foldB,fileNames{i});
        
    [JI,covA,covB] = calcValues(termIDsA{i},termIDsB{i});    
    
    compTable(i,:) = table(fileNames(i),JI,covA,length(termIDsA{i}),covB,length(termIDsB{i}),'VariableNames',variableNames);
end

termIDsA_ALL = vertcat(termIDsA{:});
termIDsB_ALL = vertcat(termIDsB{:});

[JI,covA,covB] = calcValues(termIDsA_ALL,termIDsB_ALL);

compTable(i+1,:) = table({'Jointly'},JI,covA,length(termIDsA_ALL),covB,length(termIDsB_ALL),'VariableNames',variableNames);

end

function [JI,covA,covB] = calcValues(strA,strB)
    is = intersect(strA,strB);
    un = union(strA,strB);
    JI = length(is)/length(un);
    covA = length(is)/length(strA);
    covB = length(is)/length(strB);
end

function termIDs = extractTermIDs(fold,fileName)

termIDs = [];

if exist(fullfile(fold,fileName),'file')
    T = tdfread(fullfile(fold,fileName));
    termIDs = cell(size(T.x0x23term_ID,1),1);
    for j=1:size(T.x0x23term_ID,1)
        termIDs{j} = strtrim(T.x0x23term_ID(j,:));
    end
end
    
end