batchID = 2;
srcDir = ['D:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\Output\Batch' num2str(batchID) '\normalizationCSVs'];
tgtDir = ['D:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\Output\Batch' num2str(batchID) '\ORA-UpDownTexts'];

d = dir([srcDir '\*.csv']);
criticalP = 0.01;
%ttestSign = 1.3;
foldChangeLimit = 1;
for i=1:numel(d)
    [~,normTypeID,~] = fileparts(d(i).name);
    normTypeID = strsplit(normTypeID,'__');
    normTypeID = strjoin(normTypeID(2:end),'__');
    
    srcT = readtable(fullfile(srcDir,d(i).name));
    filterSignificantProteins(srcT,tgtDir,normTypeID,criticalP,foldChangeLimit,true);
end