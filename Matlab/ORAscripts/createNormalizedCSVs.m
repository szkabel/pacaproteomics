%Create csv-s with different normalizations.

% Requires PD a ProtData to be in the workspace.

batchID = 2;
targetPath = ['D:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\Output\Batch' num2str(batchID) '\normalizationCSVs'];

normTypes = {...
    %{'normType','log-Med','imputeMethod','removeEmptyRows'}; ...
    %{'normType','log','imputeMethod','removeEmptyRows'}; ...
    %{'normType','log-RelAbu','imputeMethod','removeEmptyRows'}; ...
    %{'normType','log-Area','imputeMethod','removeEmptyRows'}; ...
    {'normType','RelAbu-log','imputeMethod','removeEmptyRows'}; ...
    %{'normType','Area-log','imputeMethod','removeEmptyRows'}; ...
    %{'normType','Area','imputeMethod','removeEmptyRows'};...
    %{'normType','RelAbu','imputeMethod','zero'};...
    %{'normType','RelAbu','imputeMethod','removeEmptyRows'};...
        };

for i=1:length(normTypes)
    fprintf('Working on: %d ...\n',i);
    for j=1:2:length(normTypes{i}), fprintf('%s: %s\t',normTypes{i}{j},normTypes{i}{j+1}); end
    fprintf('\n');
    [dataM,protLabs,sampleLabs] = PD.fetchData(normTypes{i}{:},'batchIDs',batchID);
    
    nC = size(dataM,2);
    dataCols = cell(1,nC);
    for j=1:nC
        dataCols{j} = dataM(:,j);
    end
    
    % Conducting t-tests. Paired AND unpaired as well in EACH case.
    % Naturally lot of data may be missing, as only completely empty rows
    % are removed. This is resolved by in the following way:
    %   1) Unpaired t-test: naturally, if there is at least 1 sample from
    %   each group then it works.
    %   2) Paired t-test: we need to have pairs of data for the
    %   calculation. Only if BOTH of the samples from the same patient are
    %   in tact the sample is taken into account.
    % In case there is not enough value, then NaN is output.
    
    nofTests = 8; % {NP-HP, NS-HS, NS-NP, HS-HP}x{unpaired, paired}
    nofValsPerTest = 3; %Ttest AND PValue AND FoldChange
    ttestResults = cell(nofTests*nofValsPerTest,2);
    
    testCounter = 1;
    
    % Neoplastic vs. Healthy by Tissue type first separated
    [~,~,~,reorgMatrixCell,~,hierarchIdx,~,~,~]...
        = groupMatrixByCol(dataM,sampleLabs,{'tissueType','state'},'groupingOrder',{'ascend','descend'});        
    [ttestResults,testCounter] = performIndepTTest(reorgMatrixCell,hierarchIdx,ttestResults,testCounter,sampleLabs);
    % Parenchyma vs. stroma, so first state separated
    [~,~,~,reorgMatrixCell,~,hierarchIdx,~,~,~]...
        = groupMatrixByCol(dataM,sampleLabs,{'state','tissueType'},'groupingOrder',{'descend','ascend'});
    [ttestResults,testCounter] = performIndepTTest(reorgMatrixCell,hierarchIdx,ttestResults,testCounter,sampleLabs);
    
    % Neoplastic vs. Healthy by Tissue type first separated
    [~,~,~,reorgMatrixCell,~,hierarchIdx,~,~,~]...
        = groupMatrixByCol(dataM,sampleLabs,{'tissueType','state'},'groupingOrder',{'ascend','descend'});        
    [ttestResults,testCounter] = performPairedTTest(reorgMatrixCell,hierarchIdx,ttestResults,testCounter,sampleLabs);
    % Parenchyma vs. stroma, so first state separated
    [~,~,~,reorgMatrixCell,~,hierarchIdx,~,~,~]...
        = groupMatrixByCol(dataM,sampleLabs,{'state','tissueType'},'groupingOrder',{'descend','ascend'});
    [ttestResults,testCounter] = performPairedTTest(reorgMatrixCell,hierarchIdx,ttestResults,testCounter,sampleLabs);
    
    %Go on with paired t-tests... tougher
    
    %firstCols
    variableNames = {};
    nR = length(protLabs);
    uniProtAcc = cell(nR,1);
    for j=1:nR, uniProtAcc{j} = protLabs{j}.uniProtAccession; end
    variableNames{end+1} = 'Accession'; %#ok<*SAGROW>
    uniProtAccWOIso = cell(nR,1);
    for j=1:nR, uniProtAccWOIso{j} = protLabs{j}.uniProtWOisoform; end
    variableNames{end+1} = 'Accession-WO-iso';
    writtenProtName = cell(nR,1);
    for j=1:nR, writtenProtName{j} = protLabs{j}.writtenProtName; end
    variableNames{end+1} = 'GeneName';
    geneName = cell(nR,1);
    for j=1:nR, geneName{j} = protLabs{j}.humanReadable; end    
    variableNames{end+1} = 'WrittenName';
    description = cell(nR,1);
    for j=1:nR, description{j} = protLabs{j}.description; end
    variableNames{end+1} = 'Description';    
    
    for j=1:nC
        variableNames{end+1} = sampleLabs{j}.print();
    end
    for j=1:size(ttestResults,1)
        variableNames{end+1} = ttestResults{j,1};
    end
    % Convert to valid table name
    for j=1:length(variableNames), variableNames{j} = strrep(variableNames{j},'-','_'); end     
    
    T = table(uniProtAcc,uniProtAccWOIso,geneName,writtenProtName,description,dataCols{:},ttestResults{:,2},'VariableNames',variableNames);
    
    fileName = 'NormalizedAbundancies_';
    for j=1:2:length(normTypes{i}), fileName = [fileName '__' normTypes{i}{j} '_' normTypes{i}{j+1}]; end %#ok<AGROW> not much
    
    writetable(T,fullfile(targetPath,[fileName '.csv']));
    
    fig = plotColumnHist(dataM,sampleLabs);
    
    saveas(fig,fullfile(targetPath,['ColumnDistribution_' fileName '.png']));
    saveas(fig,fullfile(targetPath,['ColumnDistribution_' fileName '.fig']));
    
    close(fig);
end

function [ttestResults,testCounter] = performIndepTTest(reorgMatrixCell,hierarchIdx,ttestResults,testCounter,sampleLabs)

nofValsPerTest = 3;
    for j=1:length(reorgMatrixCell) %go through tissue types
        %length(reorgMatrixCell{j}) this should be two and the 2 cellarrays should have similar number of values        
        varNameTstat = ['tTest__' sampleLabs{hierarchIdx{j}{1}(1)}.print('stateAndType-short') '-' sampleLabs{hierarchIdx{j}{2}(1)}.print('stateAndType-short') '__indep_TestDiff'];
        varNamePval = ['tTest__' sampleLabs{hierarchIdx{j}{1}(1)}.print('stateAndType-short') '-' sampleLabs{hierarchIdx{j}{2}(1)}.print('stateAndType-short') '__indep_PValue'];
        varNameFoldChange = ['tTest__' sampleLabs{hierarchIdx{j}{1}(1)}.print('stateAndType-short') '-' sampleLabs{hierarchIdx{j}{2}(1)}.print('stateAndType-short') '__indep_FoldChange'];
        %Carry out the test
        nofProts = size(reorgMatrixCell{1}{1},1);
        pVals = nan(nofProts,1);
        tTests = nan(nofProts,1);
        foldChanges = nan(nofProts,1);
        for k=1:nofProts
            x = reorgMatrixCell{j}{1}(k,:);
            y = reorgMatrixCell{j}{2}(k,:);
            [~,pVals(k),~,stats] = ttest2(x,y);
            foldChanges(k) = nanmean(x)-nanmean(y);
            tTests(k) = stats.tstat;
        end
        ttestResults{(testCounter-1)*nofValsPerTest+1,1} = varNameTstat;
        ttestResults{(testCounter-1)*nofValsPerTest+1,2} = tTests;
        ttestResults{(testCounter-1)*nofValsPerTest+2,1} = varNamePval;
        ttestResults{(testCounter-1)*nofValsPerTest+2,2} = pVals;
        ttestResults{(testCounter-1)*nofValsPerTest+3,1} = varNameFoldChange;
        ttestResults{(testCounter-1)*nofValsPerTest+3,2} = foldChanges;
        testCounter = testCounter + 1;
    end
end

function [ttestResults,testCounter] = performPairedTTest(reorgMatrixCell,hierarchIdx,ttestResults,testCounter,sampleLabs)

nofValsPerTest = 3;
    for j=1:length(reorgMatrixCell) %go through tissue types
        %length(reorgMatrixCell{j}) this should be two and the 2 cellarrays should have similar number of values        
        varNameTstat = ['tTest__' sampleLabs{hierarchIdx{j}{1}(1)}.print('stateAndType-short') '-' sampleLabs{hierarchIdx{j}{2}(1)}.print('stateAndType-short') '__paired_TestDiff'];
        varNamePval = ['tTest__' sampleLabs{hierarchIdx{j}{1}(1)}.print('stateAndType-short') '-' sampleLabs{hierarchIdx{j}{2}(1)}.print('stateAndType-short') '__paired_PValue'];
        varNameFoldChange = ['tTest__' sampleLabs{hierarchIdx{j}{1}(1)}.print('stateAndType-short') '-' sampleLabs{hierarchIdx{j}{2}(1)}.print('stateAndType-short') '__paired_FoldChange'];
        %Carry out test
        
        %First identify pairs:
        XSampleIdx = hierarchIdx{j}{1};
        YSampleIdx = hierarchIdx{j}{2};
        Xidx = 1:length(XSampleIdx);
        Yidx = zeros(1,length(XSampleIdx));
        for k=1:length(XSampleIdx)
            currPatientId = sampleLabs{XSampleIdx(k)}.patientID;
            for kk=1:length(YSampleIdx)
                if currPatientId == sampleLabs{YSampleIdx(kk)}.patientID
                    Yidx(k) = kk;
                end
            end
        end
        %Remove unmatches:
        Xidx(Yidx == 0) = [];
        Yidx(Yidx == 0) = [];
        
        nofProts = size(reorgMatrixCell{1}{1},1);
        pVals = nan(nofProts,1);
        tTests = nan(nofProts,1);
        foldChanges = nan(nofProts,1);
        for k=1:nofProts                        
            x = reorgMatrixCell{j}{1}(k,Xidx);
            y = reorgMatrixCell{j}{2}(k,Yidx);
            [~,pVals(k),~,stats] = ttest(x,y);
            foldChanges(k) = nanmean(x-y);
            tTests(k) = stats.tstat;
        end
        ttestResults{(testCounter-1)*nofValsPerTest+1,1} = varNameTstat;
        ttestResults{(testCounter-1)*nofValsPerTest+1,2} = tTests;
        ttestResults{(testCounter-1)*nofValsPerTest+2,1} = varNamePval;
        ttestResults{(testCounter-1)*nofValsPerTest+2,2} = pVals;
        ttestResults{(testCounter-1)*nofValsPerTest+3,1} = varNameFoldChange;
        ttestResults{(testCounter-1)*nofValsPerTest+3,2} = foldChanges;
        testCounter = testCounter + 1;
    end
end