function [compTable,unionListALL] = calcORAMatchScoresTable(tableA,tableB)

dbIDs = {...
    'Component';...
    'Function';...
    'KEGG';...
    'Process';...
    'RCTM';...
    };

nofDBs = length(dbIDs);

termIDsA = cell(nofDBs,1);
termIDsB = cell(nofDBs,1);

variableNames = {'fileName','jacc','A_cov','A_size','B_cov','B_size'};

compTable = table({},[],[],[],[],[],'VariableNames',variableNames);

for i=1:nofDBs
    termIDsA{i} = extractTermIDs(tableA,dbIDs{i});    
    termIDsB{i} = extractTermIDs(tableB,dbIDs{i});
        
    [JI,covA,covB] = calcValues(termIDsA{i},termIDsB{i});    
    
    compTable(i,:) = table(dbIDs(i),JI,covA,length(termIDsA{i}),covB,length(termIDsB{i}),'VariableNames',variableNames);
end

termIDsA_ALL = vertcat(termIDsA{:});
termIDsB_ALL = vertcat(termIDsB{:});

[JI,covA,covB,unionListALL] = calcValues(termIDsA_ALL,termIDsB_ALL);

compTable(i+1,:) = table({'Jointly'},JI,covA,length(termIDsA_ALL),covB,length(termIDsB_ALL),'VariableNames',variableNames);

end

function [JI,covA,covB,un] = calcValues(strA,strB)
    is = intersect(strA,strB);
    un = union(strA,strB);
    JI = length(is)/length(un);
    covA = length(is)/length(strA);
    covB = length(is)/length(strB);
end

function termIDs = extractTermIDs(T,filterString)

termIDs = T.term(strcmp(T.category,filterString));

end