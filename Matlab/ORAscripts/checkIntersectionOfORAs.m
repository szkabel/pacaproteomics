baseFolder = 'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\Output\Batch2\ORA-UpDownTexts';

queryTypes = {'NP_HP','NS_HS','NP_NS','HP_HS'};
testToBePerformed = {...
    'normType_log-Med__imputeMethod_removeEmptyRows','paired',...
       'normType_log-Med__imputeMethod_removeEmptyRows','paired';...
   %'normType_log-Med__imputeMethod_removeEmptyRows','indep',...
   %    'normType_log-RelAbu__imputeMethod_removeEmptyRows','indep';...    
   %'normType_log-RelAbu__imputeMethod_removeEmptyRows','indep',...
   %    'normType_RelAbu__imputeMethod_removeEmptyRows','indep';...
   %'normType_RelAbu__imputeMethod_removeEmptyRows','indep',...
   %    'normType_RelAbu__imputeMethod_zero','indep';...
   %'normType_RelAbu__imputeMethod_zero','indep',...
   %    'normType_RelAbu__imputeMethod_zero','paired';...
...% The 2 side intersection      
   %'normType_log-Med__imputeMethod_removeEmptyRows','indep',...
   %    'normType_RelAbu__imputeMethod_zero','paired';...       
...% The other paired-independent comparison for the other meaningful way 
   %'normType_log-RelAbu__imputeMethod_removeEmptyRows','indep',...
   %     'normType_log-RelAbu__imputeMethod_removeEmptyRows','paired';...
   % 'normType_log-Med__imputeMethod_removeEmptyRows','indep',...
   %     'normType_log-Med__imputeMethod_removeEmptyRows','paired',...
...% Final comparisons:
%     'normType_log-Med__imputeMethod_removeEmptyRows','indep',...
%         'normType_RelAbu__imputeMethod_removeEmptyRows','paired';...
%     'normType_RelAbu__imputeMethod_removeEmptyRows','paired',...
%         'normType_RelAbu__imputeMethod_zero','paired';...
%     'normType_log-Med__imputeMethod_removeEmptyRows','indep',...
%         'normType_RelAbu__imputeMethod_zero','paired';...
    };

diary(fullfile(baseFolder,[timeString(5) '_outputLog.txt']));

diary on;

for ii = 1:length(queryTypes)
    for i=1:5, fprintf('****************************\n'); end
    fprintf('Comparison type: %s\n~~~~~~~~~~~~~~~~~~~~~~~\n',queryTypes{ii});
    unionUpProtList = {};
    unionDownProtList = {};
    unionUpPathList = {};
    unionDownPathList = {};

    for i=1:size(testToBePerformed,1)    
        T = readtable(fullfile(baseFolder,['_' testToBePerformed{i,1}],[queryTypes{ii} '-' testToBePerformed{i,2} '-UP.txt']), 'ReadVariableNames',false);
        listAup = {T.Var1{:}};
        T = readtable(fullfile(baseFolder,['_' testToBePerformed{i,1}],[queryTypes{ii} '-' testToBePerformed{i,2} '-DOWN.txt']),'ReadVariableNames',false);
        listAdown = {T.Var1{:}};
        T = readtable(fullfile(baseFolder,['_' testToBePerformed{i,3}],[queryTypes{ii} '-' testToBePerformed{i,4} '-UP.txt']),'ReadVariableNames',false);
        listBup = {T.Var1{:}};
        T = readtable(fullfile(baseFolder,['_' testToBePerformed{i,3}],[queryTypes{ii} '-' testToBePerformed{i,4} '-DOWN.txt']),'ReadVariableNames',false);    
        listBdown = {T.Var1{:}};
        
        %Calc union sizes
        unionUpProtList = union(unionUpProtList,listAup); unionUpProtList = union(unionUpProtList,listBup);
        unionDownProtList = union(unionDownProtList,listAdown); unionDownProtList = union(unionDownProtList,listBdown);

        fprintf('\n\n******************\nComparison between:\n%s\n%s\n----\n%s\n%s\n\n',...
            testToBePerformed{i,1},testToBePerformed{i,2},testToBePerformed{i,3},testToBePerformed{i,4});

        % creating here STRING Result folders:
        stringTgtDirA = fullfile(baseFolder,['_' testToBePerformed{i,1}]);
        createEmptyStringResultFolders(stringTgtDirA);
        stringTgtDirB = fullfile(baseFolder,['_' testToBePerformed{i,3}]);
        createEmptyStringResultFolders(stringTgtDirB);

        % Determine up-regulated
        fprintf('/\\  /\\  UP  /\\  /\\\n');
        % Calc. intersection of protein names
        calcIntersects(listAup,listBup);
        % Do string analysis
        stringResDirAUp = fullfile(baseFolder,['_' testToBePerformed{i,1}],[queryTypes{ii} '-' testToBePerformed{i,2} '-UP']);
        stringResDirBUp = fullfile(baseFolder,['_' testToBePerformed{i,3}],[queryTypes{ii} '-' testToBePerformed{i,4} '-UP']);
        AUpTable = stringEnrichmentRetrieval(stringResDirAUp,listAup);        
        BUpTable = stringEnrichmentRetrieval(stringResDirBUp,listBup);        
        if ~isempty(AUpTable) && ~isempty(BUpTable)
            [resTable,uniListUp] = localCalcTable(AUpTable,BUpTable);
            unionUpPathList = union(unionUpPathList,uniListUp);
            disp(resTable);
        end

        fprintf('\\/  \\/  DOWN  \\/  \\/\n');
        calcIntersects(listAdown,listBdown);
        stringResDirADown = fullfile(baseFolder,['_' testToBePerformed{i,1}],[queryTypes{ii} '-' testToBePerformed{i,2} '-DOWN']);
        stringResDirBDown = fullfile(baseFolder,['_' testToBePerformed{i,3}],[queryTypes{ii} '-' testToBePerformed{i,4} '-DOWN']);        
        ADownTable = stringEnrichmentRetrieval(stringResDirADown,listAdown);        
        BDownTable = stringEnrichmentRetrieval(stringResDirBDown,listBdown);        
        if ~isempty(ADownTable) && ~isempty(BDownTable)
            [resTable,uniListDown] = localCalcTable(ADownTable,BDownTable);
            unionDownPathList = union(unionDownPathList,uniListDown);
            disp(resTable);
        end
    end
    
    fprintf('\nUNION OF UP PROTEINS SIZE: %d\n',length(unionUpProtList));
    fprintf('\nUNION OF UP PATHWAYS SIZE: %d\n\n',length(unionUpPathList));
    fprintf('\nUNION OF DOWN PROTEINS SIZE %d\n',length(unionDownProtList));
    fprintf('\nUNION OF UP PATHWAYS SIZE: %d\n\n',length(unionDownPathList));
    
end

diary off;

function [numA,numB,inters] = calcIntersects(listA,listB)
    numA = length(listA);
    numB = length(listB);
    inters = length(intersect(listA,listB));

    fprintf('A size is %d\nB size is: %d\nIntersect is: %d\n',numA,numB,inters);
end

function [resTable,uniList] = localCalcTable(AUpTable,BUpTable)
    [resTable,uniList] = calcORAMatchScoresTable(AUpTable,BUpTable);
end

% function upTable = localCalcTable(stringResDirAUp,stringResDirBUp)
% 
% This old implementation was used for manual string download, however it
% was now replaced with programmatic access
% 
%     if isfolder(stringResDirAUp) && isfolder(stringResDirBUp)
%         dA = dir(stringResDirAUp);
%         dB = dir(stringResDirBUp);
%         if numel(dA)>2 && numel(dB)>2
%             upTable = calcORAMatchScores(stringResDirAUp,stringResDirBUp);         
%         end        
%     end
% end