function writeToFile(idxList,allProtLabels,targetDir,fileName,fieldName)    
    f = fopen(fullfile(targetDir,fileName),'w');
    for i=find(idxList)'
        fprintf(f,'%s\n',allProtLabels{i}.(fieldName));
    end
    fclose(f);
end