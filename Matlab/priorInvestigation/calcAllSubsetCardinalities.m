function [boolIDmatrix,cardinalities] = calcAllSubsetCardinalities(containMatrix)
% Inputs contain matrix: the columns refer to different sets (nofSet), whilst the
% rows are entities in those sets.
% Output the boolID matrix which has 2^nofSets rows (all possible
% intersections of these) and the cardinality of those in cardinalities.

nofSets = size(containMatrix,2);
nofEntries = size(containMatrix,1);

nofSubs = 2^nofSets-1;
boolIDmatrix = false(nofSubs,nofSets);
cardinalities = zeros(nofSubs,1);

for i=1:nofSubs
    currPattern = dec2bin(i,nofSets); 
    boolIDmatrix(i,currPattern == '1') = true;
    
    cardinalities(i) = sum(all(containMatrix == repmat(boolIDmatrix(i,:),nofEntries,1),2));
end

end