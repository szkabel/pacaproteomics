function [nofMissing,missIdxList] = calcNofMissingENSPs(allProtLabels,idxList)
    nofMissing = 0;
    missIdxList = false(length(idxList),1);
    for i=find(idxList)'
        if strcmp(allProtLabels{i}.ensemblP,'Not found.')
            nofMissing = nofMissing + 1;
            missIdxList(i) = true;
        end
    end
end