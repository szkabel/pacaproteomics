% Import the XPath classes
import javax.xml.xpath.*

% Import also url handling
import matlab.net.*
import matlab.net.http.*
r = RequestMessage;

%Example query link from proteomics DB
uri = URI('https://www.proteomicsdb.org/proteomicsdb/logic/api/tissuelist.xsodata/CA_AVAILABLEBIOLOGICALSOURCES_API?$select=TISSUE_ID,TISSUE_NAME,TISSUE_GROUP_NAME,TISSUE_CATEGORY,SCOPE_ID,SCOPE_NAME,QUANTIFICATION_METHOD_ID,QUANTIFICATION_METHOD_NAME,MS_LEVEL&$format=xml');

resp = send(r,uri);

% Construct the DOM.
doc = resp.Body.Data;

% Create an XPath expression.
factory = XPathFactory.newInstance;
xpath = factory.newXPath;
expression = xpath.compile('properties[TISSUE_NAME=pancreas]');

% Apply the expression to the DOM.
nodeList = expression.evaluate(doc,XPathConstants.NODESET);

% Iterate through the nodes that are returned.
for i = 1:nodeList.getLength
    node = nodeList.item(i-1);
    disp(char(node.getFirstChild.getNodeValue))
end