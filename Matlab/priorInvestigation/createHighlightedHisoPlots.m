function createHighlightedHisoPlots(nofBins,binEdges,highlightBool,ourAbuM,figTitle,histCountOur,genColor,highlightColor)
    highlightType = 'top';%'next';%'bycolor';%
    foundBarInfos = zeros(1,nofBins);
    for i=find(highlightBool)'
        for j=1:size(ourAbuM,2)
            if ~isnan(ourAbuM(i,j))
                bin = find(binEdges < log2(ourAbuM(i,j)),1,'last');
                foundBarInfos(bin) = foundBarInfos(bin)+1;
            end
        end
    end
    figure('units','normalized','outerposition',[0 0.25 1 0.75]);
    switch highlightType
        case 'next'
            %bar(foundBarInfos);
            h = bar([histCountOur; foundBarInfos]');
            h(1).CData = repmat(genColor,nofBins,1);
            h(2).CData = repmat(highlightColor,nofBins,1);
            xlabel('abundancy bins (log2)'); ylabel('# proteins');
            legend({'All proteins','Protein of interest'});        
        case 'bycolor'            
            hold on;
            for i=1:nofBins
                h = bar(i,histCountOur(i));
                if foundBarInfos(i)>0
                    set(h,'FaceColor',highlightColor);
                    text(i,histCountOur(i),num2str(foundBarInfos(i)),'vert','bottom','horiz','center');
                else
                    set(h,'FaceColor',genColor);
                end
            end
        case 'top'
            bar(histCountOur,'FaceColor',genColor);
            hold on;
            bar(foundBarInfos,'FaceColor',highlightColor);
            xlabel('abundancy bins (log2)'); ylabel('# proteins');
            legend({'All proteins','Protein of interest'});        
    end
    
    title(figTitle);

end