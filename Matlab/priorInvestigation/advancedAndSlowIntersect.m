function [priorM,ourM] = advancedAndSlowIntersect(priorList,ourList)
    %Create a useful back and fortfh list for matching.
    % NOTE: To work properly at least one of the cellarrays HAVE TO contain
    % CELLS as entries!
    nofPrios = length(priorList);
    nofOurs = length(ourList);
    priorM = cell(nofPrios,1);
    ourM = cell(nofOurs,1);
    h = waitbar(0,'Advanced and slow intersection calculation...');
    for i=1:nofPrios
        if mod(i,100)==0 && ishandle(h), waitbar(i/nofPrios,h,sprintf('Advanced and slow intersection calculation [%d/%d]',i,nofPrios)); end
        for j=1:nofOurs
            if any(ismember(priorList{i},ourList{j}))
                priorM{i}(end+1) = j;
                ourM{j}(end+1) = i;
            end
        end
    end
    if ishandle(h), close(h); end
end