function [finalIDList,containMatrix,idMatchings] = calcUnionMembership(varargin)
% Accepts any number of lists (cellarrays of strings), where entries may be
% still cellarrays for denoting multiple entries for the same record (hence
% we may have cellarrays of cellarrays of strings).
%
% First we determine the complete set of available IDs into finalIDlist.
% This is going to be the number of rows in containMatrix, which is a bool
% matrix. Each column refers to an input dataset and determines if the
% specific row entry is found in that list or not.
% Finally idMatchings is the same sized cellarray as containMatrix and
% filled with (possible multiple) indices per true entry where that can be
% actually found in the original dataset.

nofDatasets = nargin;

IDMap = containers.Map();

for i=1:nofDatasets
    currList = varargin{i};
    currLength = length(currList);
    for j=1:currLength
        if iscell(currList{j})
            for k=1:length(currList{j})
                IDMap(currList{j}{k}) = true;
            end
        else
            IDMap(currList{j}) = true;
        end
    end
end

finalIDList = IDMap.keys();
nofProts = length(finalIDList);
finIDListCell = cell(nofProts,1);
for i=1:nofProts, finIDListCell{i} = finalIDList(i); end

containMatrix = false(nofProts,nofDatasets);
idMatchings = cell(nofProts,nofDatasets);

for i=1:nofDatasets
    [refMatch,~] = advancedAndSlowIntersect(finIDListCell,varargin{i});
    idMatchings(:,i) = refMatch;
    containMatrix(:,i) = ~cellfun(@isempty,refMatch);
end


end