%custom script, should be a special case of the PCA plot etc.

% PD is required

[fullM,~,sampleL] = PD.fetchData('normType','none','batchIDs',[2]);
[fullMnorm,~,~] = PD.fetchData('normType','log','batchIDs',[2]);

[~,~,~,~,~,~,matrixArray,labelArray,colIdxArray] = groupMatrixByCol(fullM,sampleL,{'tissueType','state'},'groupingOrder',{'ascend','descend'});
[~,~,~,~,~,~,matrixArrayNorm,] = groupMatrixByCol(fullMnorm,sampleL,{'tissueType','state'},'groupingOrder',{'ascend','descend'});

colors = 'rgbcy';
for j=1:length(matrixArray)
    nC = length(labelArray{j});
    areas = zeros(1,nC);
    infoT = cell(1,nC);
    for i=1:nC
        areas(i) = sum(sampleL{colIdxArray{j}(i)}.sampleAreas);
        infoT{i} = labelArray{j}(i); 
    end
    medians = nanmedian(matrixArray{j});
    protNum = sum(~isnan(matrixArray{j}),1);
    sums = nansum(matrixArray{j});
    sumsNorm = nansum(matrixArrayNorm{j});

%     figure(1);
%     plot3(areas,protNum,sums,['*' colors(j)]);
%     xlabel('injected area'); ylabel('protNum'); zlabel('sums');
%     for i=1:nC, text(areas(i),protNum(i),sums(i),infoT{i}); end
%     grid on;    
%     hold on;
%     
    %variables = {'areas','protNum','sums','medians'};
    variables = {'sums','sumsNorm'};
    figC = 2;
    for k=1:length(variables)-1
        for kk=k+1:length(variables)
            figure(figC);
            hold on;
            grid on;
            eval(['plot(' variables{k} ',' variables{kk} ',''*' colors(j) ''');']);
            xlabel(variables{k});
            ylabel(variables{kk});
            for i=1:nC
                eval(['text(' variables{k} '(i),' variables{kk} '(i),infoT{i});']);
            end
            figC = figC + 1;
        end
    end
    
end