%Trialing with biclusetring

fabiaResult = biclust(statMatrixWithoutNans,'fabia');

%Plot one interesting bicluster that we've found.
heatmap(statMatrixWithoutNans,fabiaResult,4);

%Try with other random inits to see how consistent it is
nofTrials = 20;

fabTestResult = cell(nofTrials,1);
for i=1:nofTrials
fabiaOptions = {'bicluster_no',5,'sparseness_factor',0.1,'iteration_no',5000,...
                 'spl',0,'spz',0.5,'non_negative',0,'random',1,'center',2,'norm',1,...
                  'scale',0,'lap',1,'nL',0,'lL',0,'bL',0};
              
              fprintf('Running test %d/%d\n',i,nofTrials);
fabTestResult{i} = biclust(statMatrixWithoutNans,'fabia',fabiaOptions{:});
end

fprintf('Checking intersection...\n');
%Check one seemingly interesting cluster
classOfInterest = 4;
matchingProts = 1:size(statMatrixWithoutNans,1);
matchingCols = 1:20;
for i=1:5%nofTrials
    matchingProts = intersect(matchingProts,fabTestResult{i}.Clust(classOfInterest).rows);
    matchingCols = intersect(matchingCols,fabTestResult{i}.Clust(classOfInterest).cols);
end

% --> This (with proper nofTrails match) ends up having empty matching
% Prots set, which renders this investigation useless... :/
% Nonetheless, by running it only up till 5 there are some matches

interestingGenes = genesWithAbundanceWONan(matchingProts);
