%Extract significant proteins from the stat matrix and save it out

% PARAMETERS
compName = 'ORA_NP_HA';
colPostFix = 'NP_HA';
alpha = 0.05; %significance level

% ACTUAL CODE

if ~exist('driveLetter','var')
    driveLetter{1} = 'G';
end

tgtPath = fullfile([driveLetter{1} ':'],'Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment',compName);

if ~exist(tgtPath,'dir'), mkdir(tgtPath); end

pvalColName = ['Student_sT_testP_value' colPostFix ];
tTestDiffColName = ['Student_sT_testDifference' colPostFix ];

signFilter = T_statResults.(pvalColName) < alpha;
posFilter = T_statResults.(tTestDiffColName) > 0;
negFilter = T_statResults.(tTestDiffColName) < 0;

% up
upNames = T_statResults.Accession(signFilter & posFilter);
downNames = T_statResults.Accession(signFilter & negFilter);

nofUp = length(upNames);
leading = cellfun(@strsplit,upNames,repmat({'-'},nofUp,1),'UniformOutput',false);
upNamesNoIso = cell(nofUp,1);
for i=1:nofUp, upNamesNoIso{i} = leading{i}{1}; end

nofDown = length(downNames);
leading = cellfun(@strsplit,downNames,repmat({'-'},nofDown,1),'UniformOutput',false);
downNamesNoIso = cell(nofDown,1);
for i=1:nofDown, downNamesNoIso{i} = leading{i}{1}; end

fileList = {...
    'downList.txt',downNames;...
    'upList.txt',upNames;...
    'downListNoIso.txt',downNamesNoIso;...
    'upListNoIso.txt',upNamesNoIso;...
};

for i=1:size(fileList,1)
    f = fopen(fullfile(tgtPath,fileList{i,1}),'w');
    for j=1:length(fileList{i,2})
        fprintf(f,'%s\n',fileList{i,2}{j});
    end
    fclose(f);
end