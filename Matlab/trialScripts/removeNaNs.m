function [zerodNans,rowRemoved,colRemoved,meanOfNaNsRow,meanOfNaNsCol,nanPlaceMatrix] = removeNaNs(matrixWithNans,fillType)
%Performs removal of NANs in several simple way.
% filltype: zero puts zero to the nan places
%           median-col puts the column median
%           median-row puts the row median
%           mean similarly
%
% zerosNan: replaces nan according to filltype
% rowRemoved: removes all rows which have Nan
% colRemoved: removes all cols which have Nan

zerodNans = matrixWithNans;
nanPlaceMatrix = isnan(matrixWithNans);

if nargin<2
    fillType = 'zero';
end

switch fillType
    case 'zero'
        zerodNans(nanPlaceMatrix) = 0;
    case 'median-col'
        for j=1:size(matrixWithNans,2)
            medV = nanmedian(matrixWithNans(:,j));
            zerodNans(nanPlaceMatrix(:,j),j) = medV;
        end
    case 'median-row'
        for j=1:size(matrixWithNans,1)
            medV = nanmedian(matrixWithNans(j,:));
            zerodNans(j,nanPlaceMatrix(j,:)) = medV;
        end
    case 'mean-col'
        for j=1:size(matrixWithNans,2)
            medV = nanmean(matrixWithNans(:,j));
            zerodNans(nanPlaceMatrix(:,j),j) = medV;
        end
    case 'mean-row'
        for j=1:size(matrixWithNans,1)
            medV = nanmean(matrixWithNans(j,:));
            zerodNans(j,nanPlaceMatrix(j,:)) = medV;
        end        
end
rowRemoved = matrixWithNans(all(~nanPlaceMatrix,2),:);
colRemoved = matrixWithNans(:,all(~nanPlaceMatrix,1));

rowMean = nanmean(matrixWithNans,2);
colMean = nanmean(matrixWithNans,1);

meanOfNaNsRow = matrixWithNans;
meanOfNaNsCol = matrixWithNans;

for i=1:size(matrixWithNans,1)
    for j=1:size(matrixWithNans,2)
        if nanPlaceMatrix(i,j)
            meanOfNaNsRow(i,j) = rowMean(i);
            meanOfNaNsCol(i,j) = colMean(j);            
        end
    end
end

end

