%Script to evaluate the number of distinct proteins according to the level
%of Q-value. Short summary:
%
%   Proteins are digested to peptides which are then passed through on a
%   double mass-spectrometry layer and the achieved mass spectra is then
%   searched in a database to reverse engineer the original protein.
%
%   However this is clearly an ill-posed task (there might be several
%   solutions consistent with the data) hence statistics came in. There
%   should be a threshold set which then identifies the so called False
%   Discovery Rate (FDR): the proportion of false detections that are still
%   detected over the threshold. The Q-value for a given protein says that
%   what is the minimal false discovery rate at which it is still
%   considered as a significant hit.
%
%   This script counts the identified proteins for the different samples
%   (19 currently) with a given confidence (FDR) level.

threshold = 1; %This threshold seems to be the one also for the Medium/Low label in the front of the table

allSampleIdx = 15:34;

sampleLabels = T_noAbundance.Properties.VariableNames(allSampleIdx);
sampleLabelsCat = categorical(sampleLabels);
sampleLabelsCat = reordercats(sampleLabelsCat,sampleLabels);

abuMatrix = table2array(T_noAbundance(:,allSampleIdx));

qvalues = T_noAbundance.Exp_Q_value_Combined;

proteinCounts = sum((~isnan(abuMatrix(qvalues<threshold,:))),1);
staticCountFromKim = [2397 3245 3157 2674 2692 ...%NP
                      2401 3521 3318 2917 2557  ...%HA
                      2192 ... %HA-P1 HNE
                      2515 2489 2474 2303 2432  ...%HS
                      2242 2796      2749 2153  ...%NS
                       ];

barObj = bar(sampleLabelsCat,[proteinCounts;staticCountFromKim]');
legend({'Raw','PPT'});
currAx = gca();
currAx.TickLabelInterpreter = 'none';
