function [testStat,pvalues] = ttestPlay(matrixA,matrixB)
%Each row should be an independent test, columns are for different
%observations.
% Simple implementation of two-tailed, independent, two-sample t-test as
% described in wikipedia (Welch's t-test)

testStat = zeros(size(matrixA,1),1);
pvalues = zeros(size(matrixA,1),1);
for i=1:size(matrixA,1)
    [testStat(i),pvalues(i)] = calcTTest(matrixA(i,:),matrixB(i,:));
end

end

function [testStat,pvalue] = calcTTest(rowA,rowB)
    n1 = sum(~isnan(rowA));
    n2 = sum(~isnan(rowB));
    if n1 < 2 || n2 < 2
        testStat = NaN;
        pvalue = NaN;
        return;
    end
    x1 = nanmean(rowA);
    x2 = nanmean(rowB);
    s1s = nanvar(rowA);
    s2s = nanvar(rowB);
    
    s_delta = sqrt(s1s/n1 + s2s/n2);
    testStat = (x1 - x2)/s_delta;
    
    %Degree of freedom
    df = (s1s/n1 + s2s/n2)^2 / ( (s1s/n1)^2/(n1-1) + (s2s/n2)^2/(n2-1)  );
    
    pvalue = 2*tcdf(-abs(testStat),df);
end