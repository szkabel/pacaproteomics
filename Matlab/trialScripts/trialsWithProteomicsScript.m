%Playing around

% RESULTS:
% Based on Kim's help their normalization was the abuLog2MatrixCentered.
% They used only the full rows for the PCA.

loadProtData;

NP_HA_diff = nanmean(statMatrix(:,NP_statIdx),2) - nanmean(statMatrix(:,HA_statIdx),2);
NS_HS_diff = nanmean(statMatrix(:,NS_statIdx),2) - nanmean(statMatrix(:,HS_statIdx),2);

%NP_HA evaluation
missingIdx = isnan(NP_HA_diff);
protsKept = genesWithAbundance(~missingIdx);
diffValsKept = NP_HA_diff(~missingIdx);
f = fopen('NP_HA_recalc_noIso.txt','w');
for i=1:length(protsKept)
    spltString = strsplit(protsKept{i},'-');
    fprintf(f,'%s %f\n',spltString{1},diffValsKept(i));
end
fclose(f);

% Joint Healthy vs. Neoplastic evaluation
Neo_Healthy_diff = nanmean(statMatrix(:,[NP_statIdx NS_statIdx]),2) - nanmean(statMatrix(:,[HA_statIdx HS_statIdx]),2);
missingIdx = isnan(Neo_Healthy_diff);
protsKeptNH = genesWithAbundance(~missingIdx);
diffValsKept = Neo_Healthy_diff(~missingIdx);
f = fopen('NeoHealthty_fullTTestDiff_noIso.txt','w');
for i=1:length(protsKeptNH)
    spltString = strsplit(protsKeptNH{i},'-');
    fprintf(f,'%s %f\n',spltString{1},diffValsKept(i));
end
fclose(f);


%% PCA tests
%Do some tests for PCA
figure;
subplot(1,2,1)
pcaSamples(statMatrixWithoutNans);
title('PCA plot from STAT matrix without NANs');

subplot(1,2,2)
pcaSamples(statMatrix0,true);
title('PCA plot from STAT matrix with 0-d NANs');

%% Confirming the normalization method
%Best method we could come up with
%Take the log
%Quantilnorm to each other

%{
abuNorm = quantilenorm(abuLogMatrix);
abuNormMean = nanmean(abuNorm);
abuNormCentered = abuNorm - abuNormMean;
abuNormCentered0 = removeNaNs(abuNormCentered);

%as this still produces quite a few NaN-s get rid of them
[abuNorm0,abuNormR] = removeNaNs(abuNorm);

%However it turned out that they did not quantilenorm the results
%The plot and PCA is not needed any more it was tested that these are
%exactly the same
subplot(2,2,3)
pcaSamples(abuLog2MatrixCenteredWithoutNans);
title('Normalized by us -full rows');
subplot(2,2,4)
pcaSamples(abuLog2MatrixCentered0);
title('Normalized by us - zerod');

%There is no need any more for the different quantilenorm assesment.
%Repeat the test 10 times to see the effect of randomness in the abu matrix
figure;
nofTests = 5;
subplot(2,nofTests,1)
abuNormByForcing = cell(3,nofTests);
for i=1:nofTests
    abuNormByForcing{1,i} = alignToNormalDist(abuLogMatrix);
    [abuNormByForcing{2,i},abuNormByForcing{3,i}] = removeNaNs(abuNormByForcing{1,i});
    subplot(2,nofTests,i);
    pcaSamples(abuNormByForcing{2,i});
    title(['Run #' num2str(i) ': zerod']);
    subplot(2,nofTests,nofTests+i);
    pcaSamples(abuNormByForcing{3,i},floor(i/nofTests));
    title(['Run #' num2str(i) ': full rows']);
end

%Quantitative measure for the reconstruction

fprintf('Norm value to compare reconstruction:\n');
fprintf('Simple log of abu: %f\n',norm(abuLogMatrix0 - statMatrix0));
fprintf('Quantile norm of log of abu: %f\n',norm(abuNorm0 - statMatrix0));
fprintf('Quantile norm of log of abu centered: %f\n',norm(abuNormCentered0 - statMatrix0));
%}
fprintf('Median centered of log2 of abu: %f\n',norm(abuLog2MatrixCentered0 - statMatrix0));
%{
directForceErrors = zeros(1,nofTests);
for i=1:nofTests    
    directForceErrors(i) = norm(abuNormByForcing{2,i} - statMatrix0);
    fprintf('Mean of directly forced #%d: %f\n',i,directForceErrors(i));
end
fprintf('Average of directly forced: %f\n',mean(directForceErrors));
%}

%% Check PCA coeffs (interpretation analysis)

[pcaCoeffs,pcaScores] = pca(statMatrixWithoutNans');

%This is needed to retrieve which proteins are fully represented
nanPlaceMatrix = isnan(statMatrix);
keptRows = ~any(nanPlaceMatrix,2);
proteinsKept = genesWithAbundance(keptRows);

%Reconstruction test:
statMatrixWithoutNansC = statMatrixWithoutNans - mean(statMatrixWithoutNans,2);
fprintf('The full PCA reconstruction error is %f\nMore precisely:\n',norm(statMatrixWithoutNansC - pcaCoeffs*pcaScores'));
disp(norm(statMatrixWithoutNansC - pcaCoeffs*pcaScores'));

figure;
nofProtToKeep = 100;
for i=1:3
    subplot(1,3,i);
    currCoeffs = abs(pcaCoeffs(:,i));
    histogram(currCoeffs);
    [~,idx] = sort(currCoeffs,'descend');
    title(sprintf('Absolute value distribution of PCA #%d',i));
    f = fopen(sprintf('PCA_%d_highest_%d.txt',i,nofProtToKeep),'w');
    for j=1:nofProtToKeep, fprintf(f,'%s\n',proteinsKept{idx(j)}); end
    fclose(f);
    %Put out also the actual coefficient
    f = fopen(sprintf('PCA_%d_highest_%d_coeffs.txt',i,nofProtToKeep),'w');
    for j=1:nofProtToKeep, fprintf(f,'%s %f\n',proteinsKept{idx(j)},pcaCoeffs(idx(j),i)); end
    fclose(f);
end


%% Clustergram and volcano plot reconstruction
%This is the target
figure; imagesc(statMatrix);

JuhoAnnotation = T_noAbundance.JuhoList;

matchRowIdx = ~cellfun(@isempty,T_noAbundance.JuhoList);
sampleIDs = T_noAbundance.Properties.VariableNames(propSampleIdx);
sampleIDsDash = strrep(sampleIDs,'_','-');

%Try to first filter and then z-score
juhoMatrix = statMatrix(matchRowIdx,:);
[~,~,~,~,juhoMatrixColMean] = removeNaNs(juhoMatrix);
ZscoreSmall = zscore(juhoMatrixColMean);
ZscoreSmallNans = ZscoreSmall;
ZscoreSmallNans(isnan(juhoMatrix)) = NaN;

%First Z-score and then filter
Zscore = zscore(statMatrixColMean);
ZscoreNans = Zscore;
ZscoreNans(isnan(statMatrix)) = NaN;
%Restrict only for Juho's interest
ZscoreNansJuho = ZscoreNans(matchRowIdx,:);

rowLabels = cellfun(@horzcat,genesWithAbundance(matchRowIdx),repmat({' - '},sum(matchRowIdx),1),JuhoAnnotation(matchRowIdx),'UniformOutput',false);
 
clustergram(ZscoreSmallNans,...    
    'ImputeFun','knnimpute',...
    'ColumnLabels',sampleIDsDash,...
    'RowLabels',rowLabels,...        
    'DisplayRange',100,...
    'Colormap',redgreencmap(64));

%'Symmetric',false,...    
%'RowPDist','euclidean',...
%'ColumnPDist','correlation',...   
%'linkage','average',...   



healthyStromaMatrix = statMatrix(:,ismember(propSampleIdx,HS_idx));
neoStromaMatrix= statMatrix(:,ismember(propSampleIdx,NS_idx));
healthyAcinarMatrix = statMatrix(:,ismember(propSampleIdx,HA_idx));
neoParenchymaMatrix = statMatrix(:,ismember(propSampleIdx,NP_idx));

stromaP = mattest(healthyStromaMatrix,neoStromaMatrix,'VarType','equal');
parenP = mattest(healthyAcinarMatrix,neoParenchymaMatrix,'VarType','equal');

%The original volcano plot ingores the negative values hence to produce
%something similar we need to re-write this (however this is not priority now)
mavolcanoplot(healthyAcinarMatrix,neoParenchymaMatrix,parenP,'Labels',genesWithAbundance)
title('HA vs NP');

mavolcanoplot(healthyStromaMatrix,neoStromaMatrix,stromaP,'Labels',genesWithAbundance)
title('HS vs NS');