% Script to investigate matching numbers between the PaxDB and our
% proteomics results.

[fullAbus,ourProtLabels] = PD.fetchData('normType','none');

%PaxDBIDs
baseDir = 'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\PaCaTissuePrior\PaxDB';
%Name, location
dataInfoArray = {...
    'integratedFullHuman','9606-WHOLE_ORGANISM-integrated.txt';...
    'integratedPancreas','9606-PANCREAS-integrated.txt';...
    'wilhelm2014Pancreas','9606-pancreas_Wilhelm_2014_Maxquant.txt';...
    'kim2014Pancreas','9606-Adult_Pancreas_Kim_2014_SEQUEST.txt'...
    };

nofExtData = size(dataInfoArray,1);

for i=1:nofExtData
    eval([ dataInfoArray{i,1} 'Table = readtable(''' fullfile(baseDir,dataInfoArray{i,2}) ''');']);
end
stringIDfield = 'string_external_id';
abundanceField = 'abundance';
proxDBlists = cell(nofExtData,1);
proxDBabus = cell(nofExtData,1);
for i=1:nofExtData
    eval(['proxDBlists{i} = ' dataInfoArray{i,1} 'Table.' stringIDfield ';']);
    eval(['proxDBabus{i} = ' dataInfoArray{i,1} 'Table.' abundanceField ';']);
end

ourProtList = cell(length(ourProtLabels),1);
for i=1:length(ourProtLabels), ourProtList{i} = ourProtLabels{i}.stringID; end

% Check how much we lost with not finding in string
nonMatchIdx = cellfun(@strcmp,ourProtList,repmat({ProteinLabel.NotInDBString},size(ourProtList)));
fprintf('Number of mismatches in our dataset is: %d \n',sum(nonMatchIdx));
fprintf('Removing these...\n');
ourProtLabelsFilt = ourProtLabels(~nonMatchIdx);
ourProtListFilt = ourProtList(~nonMatchIdx);
fullAbusFilt = fullAbus(~nonMatchIdx,:);

allProtLists = [{ourProtListFilt}; proxDBlists];
fprintf('Calculating size of intersections...\n');

%[finalIDList,containMatrix,idMatchings] = calcUnionMembership(allProtLists{:});
load('G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\PaCaTissuePrior\PaxDB\200304_PaxPriorIntersection.mat');
nofAllProts = length(finalIDList);

[boolIDmatrix,cardinalities] = calcAllSubsetCardinalities(containMatrix);

abundancyMatrix = zeros(nofAllProts,nofExtData+1);
%Calc first for our data:
for i=find(~cellfun(@isempty,idMatchings(:,1)))'
    abundancyMatrix(i,1) = nanmedian(nanmedian(fullAbusFilt(idMatchings{i,1},:)));
end
%Calc the remaining ones
for j=1:nofExtData
    for i=find(~cellfun(@isempty,idMatchings(:,j+1)))'
        abundancyMatrix(i,j+1) = proxDBabus{j}(idMatchings{i,j+1});
    end
end

figure;
[~,abuMatrixAxes] = plotmatrix(log(abundancyMatrix));
suptitle('Log2 of abundancies');
abuMatrixAxes(1,1).Title.String = 'Our proteins - all';
for j=1:nofExtData
    abuMatrixAxes(1,j+1).Title.String = dataInfoArray{j,1};
end

% Modify our abu matrix to contain ONLY healhty tissue

