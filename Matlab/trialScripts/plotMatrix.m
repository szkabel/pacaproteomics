function plotMatrix(M,ylabs,varargin)
% Short func to plot matrix

p = inputParser();
p.addParameter('title',[]);
p.parse(varargin{:});

figure; imagesc(M);
xticks(1:size(M,1));
if isa(ylabs{1},'SampleLabel')
    ylt = cell(1,length(ylabs));
    for i=1:length(ylt), ylt{i} = ylabs{i}.print(); end
    ylabs = ylt;
end
xticklabels(ylabs);
xtickangle(45);
if ~isempty(p.Results.title)
    title(p.Results.title);
end

end

