function compTable = calcMatchScores(foldA,foldB)
%Takes 2 folders which are up/down regulated pathways results (as
%exported from STRING). Calculates the Jaccard index (intersection over
%union) from those. It also gives back the covarage (cov) values, i.e. how
%much percent of the folder's gene is also in the other one.
% If a folder is gene enrich based then it must have an up and a down in it
% so that they can be compared properly. Otherwise this info is taken from
% the table.

%The 4 different case
%{
 calcMatchScores(...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\ORA_NP_HA\NoIso',...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\NP_HA_recalc_NoIso');

calcMatchScores(...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\ORA_NP_HA\raw',...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\NP_HA_recalc');

calcMatchScores(...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\ORA_NP_HA\raw',...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\ORA_NP_HA\NoIso');

calcMatchScores(...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\NP_HA_recalc',...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\STRING_full_enrichment\NP_HA_recalc_NoIso');
%}

fileNames = {...
    'enrichment.Component.tsv';...
    'enrichment.Function.tsv';...
    'enrichment.KEGG.tsv';...
    'enrichment.Process.tsv';...
    'enrichment.RCTM.tsv';...
    };

nofDBs = length(fileNames);

termIDsA_up = cell(nofDBs,1);
termIDsA_down = cell(nofDBs,1);
termIDsB_up = cell(nofDBs,1);
termIDsB_down = cell(nofDBs,1);

compTable = table({},[],[],[],[],[],[],'VariableNames',{'fileName','up_jacc','down_jacc','A_up_cov','B_up_cov','A_down_cov','B_down_cov'});

for i=1:nofDBs
    termIDsA_up{i} = extractTermIDs(foldA,fileNames{i},'up');
    termIDsA_down{i} = extractTermIDs(foldA,fileNames{i},'down');
    termIDsB_up{i} = extractTermIDs(foldB,fileNames{i},'up');
    termIDsB_down{i} = extractTermIDs(foldB,fileNames{i},'down');
        
    [JI_up,covA_up,covB_up] = calcValues(termIDsA_up{i},termIDsB_up{i});
    [JI_down,covA_down,covB_down] = calcValues(termIDsA_down{i},termIDsB_down{i});
    
    compTable(i,:) = table(fileNames(i),JI_up,JI_down,covA_up,covB_up,covA_down,covB_down,'VariableNames',{'fileName','up_jacc','down_jacc','A_up_cov','B_up_cov','A_down_cov','B_down_cov'});
end

termIDsA_upALL = vertcat(termIDsA_up{:});
termIDsA_downALL = vertcat(termIDsA_down{:});
termIDsB_upALL = vertcat(termIDsB_up{:});
termIDsB_downALL = vertcat(termIDsB_down{:});

[JI_up,covA_up,covB_up] = calcValues(termIDsA_upALL,termIDsB_upALL);
[JI_down,covA_down,covB_down] = calcValues(termIDsA_downALL,termIDsB_downALL);

compTable(i+1,:) = table({'Jointly'},JI_up,JI_down,covA_up,covB_up,covA_down,covB_down,'VariableNames',{'fileName','up_jacc','down_jacc','A_up_cov','B_up_cov','A_down_cov','B_down_cov'});

end

function [JI,covA,covB] = calcValues(strA,strB)
    is = intersect(strA,strB);
    un = union(strA,strB);
    JI = length(is)/length(un);
    covA = length(is)/length(strA);
    covB = length(is)/length(strB);
end

function termIDs = extractTermIDs(fold,fileName,direction)

    termIDs = [];

if exist(fullfile(fold,direction),'dir')
%in this case this is a gene set analysis result
    baseDir = fullfile(fold,direction);
    if exist(fullfile(baseDir,fileName),'file')
        T = tdfread(fullfile(baseDir,fileName));
        termIDs = cell(size(T.x0x23term_ID,1),1);
        for j=1:size(T.x0x23term_ID,1)
            termIDs{j} = strtrim(T.x0x23term_ID(j,:));
        end
    end
else
%else: this is an AFC result
    if strcmp(direction,'down')
        listText = 'top of list';
    elseif strcmp(direction,'up')
        listText = 'bottom of list';
    end
    if exist(fullfile(fold,fileName),'file')
        T = tdfread(fullfile(fold,fileName));
        k = 1;
        termIDs = cell(size(T.x0x23term_ID,1),1);
        for j=1:size(T.x0x23term_ID,1)
            if strcmp(strtrim(T.direction(j,:)),listText)
                termIDs{k} = strtrim(T.x0x23term_ID(j,:));
                k = k + 1;
            end
        end
        termIDs(k:end) = [];
    end

end

    
end