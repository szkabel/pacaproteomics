
proteinAliasMap = containers.Map();

for i=1:size(proteinAliasTable,1)
    currKey = proteinAliasTable.string_protein_id{i};
    if ~proteinAliasMap.isKey(currKey)
        proteinAliasMap(currKey) = proteinAliasTable.alias(i);
    else
        currVal = proteinAliasMap(currKey);
        currVal{end+1} = proteinAliasTable.alias{i};
        proteinAliasMap(currKey) = currVal;
    end
    if mod(i,1000) == 0, fprintf('%d/%d Done\n',i,size(proteinAliasTable,1)); end
end