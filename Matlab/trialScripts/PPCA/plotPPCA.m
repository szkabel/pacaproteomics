function plotPPCA(scoresPCA,varargin)
%Specific PCA code for the proteomics data
%   colLabels: the column labels associated with the columns (has print method to produce string)
%   putLegend bool whether to put legend

p = inputParser();
p.addParameter('title','PCA');
p.addParameter('dims',[1 2 3]);
p.addParameter('legends',[]); %legends should be a colLabels array
p.addParameter('markGroup',{'patientID','ascend'}); %this should be a possible grouping field
p.addParameter('colorGroup',{'tissueType','ascend';'state','descend'}); %this should be a possible grouping field
p.addParameter('markerSize',14);
p.addParameter('saveFig',''); %Empty string means no save, otherwise filename

p.parse(varargin{:});
legends = p.Results.legends;
if ~isempty(p.Results.legends)
    putLegend = true;
    [~,~,~,~,~,~,~,~,idxMarker] = groupMatrixByCol(...
        zeros(1,length(legends)),legends,...
                        p.Results.markGroup(:,1),...
        'groupingOrder',p.Results.markGroup(:,2));
    [~,~,~,~,~,~,~,~,idxColor] = groupMatrixByCol(...
        zeros(1,length(legends)),legends,...
                        p.Results.colorGroup(:,1),...
        'groupingOrder',p.Results.colorGroup(:,2));
else
    putLegend = false;
end
dims = p.Results.dims;
markerSize = p.Results.markerSize;

%[~,scoresPCA,latent] = pca(matrixToBePCAd');

cols = getManyColor()/255;
markSymbols = 'o+*xsdv><ph^';
%batchCols = 'cg';
nofSamples = size(scoresPCA,2);

if putLegend, legString = cell(nofSamples,1); end
f = figure;
for i=1:nofSamples
    
    currMarker = find(cellfun(@any,cellfun(@ismember,idxMarker,repmat({i},1,length(idxMarker)),'UniformOutput',false)));
    %Note: it can happen that we run out of marker sizes (as there is only
    %limited [12] of them). In this case, the size is decreased by 0.5
    markerSizeMultiplier = 0.5^(ceil(currMarker/length(markSymbols))-1);
    currMarker = mod(currMarker-1,length(markSymbols))+1;
    currMarkerSize = markerSizeMultiplier*markerSize;
    currColor  = find(cellfun(@any,cellfun(@ismember,idxColor,repmat({i},1,length(idxColor)),'UniformOutput',false)));
        
    if length(dims) == 2
        plot(scoresPCA(i,dims(1)),scoresPCA(i,dims(2)),markSymbols(currMarker),'Color',cols(currColor,:),'MarkerSize',currMarkerSize);
    elseif length(dims) == 3
        plot3(scoresPCA(i,dims(1)),scoresPCA(i,dims(2)),scoresPCA(i,dims(3)),markSymbols(currMarker),'Color',cols(currColor,:),'MarkerSize',currMarkerSize);
    end
    hold on;
    grid on;
    if putLegend
        legString{i} = legends{i}.print;
    end      
end
i = 1;
%xlabel(sprintf('PCA #%d - %4.2f%%',dims(i),latent(dims(i))/sum(latent)*100));
xlabel(sprintf('PCA #%d',dims(i)));
i = 2;
ylabel(sprintf('PCA #%d',dims(i)));
if length(dims) == 3
    i = 3;
    zlabel(sprintf('PCA #%d',dims(i)));
end

if putLegend
    legend(legString,'Location','best');
end
title(p.Results.title);

if ~isempty(p.Results.saveFig)
    saveas(f,p.Results.saveFig);
end
hold off;

end

