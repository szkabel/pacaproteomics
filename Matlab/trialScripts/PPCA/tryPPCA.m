%After a proper load of PD

[fetchedMatrix,rL,cL] = PD.fetchData('normType','log-Med');

latentSpaceDim = 40;

[C, ss, M, X, Ye ] = ppca_mv(fetchedMatrix',latentSpaceDim,false);

plotPPCA(X,'legend',cL);


