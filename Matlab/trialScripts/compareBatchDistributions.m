
listOfBatches = [1 2]; % This must be in ascending order!

basisBatch = 2; %Basis of ordering in the histogram (this is IDX WITHIN listofBatches!!!)
compareBatch = 1; %Number of proteins in the matched region (this is IDX as well!)

nofBins = 100;

[rawData,protL,samL] = PD.fetchData('batchIDs',listOfBatches);

[~,~,~,reorgMCell,yLabC,hierarchIdx] = groupMatrixByCol(rawData,samL,{'batchID'});

% Easiest way: plot Matrix

matrixToPlot = zeros(size(rawData,1),2);
for i=1:2
    matrixToPlot(:,i) = nanmean(reorgMCell{i},2);
end

figure;
[~,jointMatricesAxes] = plotmatrix(matrixToPlot);

for i=1:2
    jointMatricesAxes(1,i).Title.String = ['Batch-' num2str(samL{hierarchIdx{i}(1)}.batchID)];
end

title('Average Protein Abundances in different Batches [log-med]');

%Do histogram plots

[hist,~,origIdx] = histcounts(matrixToPlot(:,basisBatch),nofBins);

matchingCounts = zeros(1,length(hist));
for i=1:length(hist)
    matchingCounts(i) = sum(~isnan(matrixToPlot(origIdx == i,compareBatch)));
end

figure;
bar([hist;matchingCounts]');
ylabel('# proteins')
xlabel('abundancy bins (in log2 space)')
legend({['Batch-' num2str(listOfBatches(basisBatch))],['Found in batch-' num2str(listOfBatches(compareBatch))]});
title(['Protein abundancy histogram based on Batch-' num2str(listOfBatches(basisBatch))]);
