%PCAComponentsExplanation
% This is script is to retrieve the most significant (having the biggest
% weight) proteins explaining the PCA components

tgtDir = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\PCA_highlights\batch12\PCA_explained';

queryStrings = {'normType','log-Med',   'batchIDs',[1 2],   'afterFilterImpute','keepFullRows'};

[matrixToBePCAd,protLabs,colLabs] = PD.fetchData(queryStrings{:});

[pcaCoeffs,pcaScores] = pcaSamplesNew(matrixToBePCAd,'legends',colLabs);

proteinsKept = cell(length(protLabs),1);
proteinsKeptHR = cell(length(protLabs),1);
for i=1:length(protLabs)
    proteinsKept{i} = protLabs{i}.uniProtWOisoform;
    proteinsKeptHR{i} = protLabs{i}.humanReadable;
end

%Reconstruction test:
%statMatrixWithoutNansC = statMatrixWithoutNans - mean(statMatrixWithoutNans,2);
%fprintf('The full PCA reconstruction error is %f\nMore precisely:\n',norm(statMatrixWithoutNansC - pcaCoeffs*pcaScores'));
%disp(norm(statMatrixWithoutNansC - pcaCoeffs*pcaScores'));

nofProtToKeep = [10 100 length(proteinsKept)];
for kk=1:length(nofProtToKeep)
    ff = figure;
    for i=1:3
        subplot(1,3,i);
        currCoeffs = abs(pcaCoeffs(:,i));
        histogram(currCoeffs);
        [~,idx] = sort(currCoeffs,'descend');
        title(sprintf('Absolute value distribution of PCA #%d',i));
        f = fopen(sprintf('PCA_%d_highest_%d.txt',i,nofProtToKeep(kk)),'w');
        for j=1:nofProtToKeep(kk), fprintf(f,'%s\n',proteinsKept{idx(j)}); end
        fclose(f);
        %Put out also the actual coefficient
        f = fopen(sprintf('PCA_%d_highest_%d_coeffs.txt',i,nofProtToKeep(kk)),'w');
        for j=1:nofProtToKeep(kk), fprintf(f,'%s %f\n',proteinsKept{idx(j)},pcaCoeffs(idx(j),i)); end
        fclose(f);
        f = fopen(sprintf('PCA_%d_highest_%d_coeffs_HR.txt',i,nofProtToKeep(kk)),'w');
        for j=1:nofProtToKeep(kk), fprintf(f,'%s %f\n',proteinsKeptHR{idx(j)},pcaCoeffs(idx(j),i)); end
        fclose(f);
    end
    saveas(ff,sprintf('PCA_%d_highest_coeffs.fig',kk))
    close(ff);
end