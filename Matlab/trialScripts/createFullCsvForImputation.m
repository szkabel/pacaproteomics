% Create a csv for the web-based metabolomics imputation tool
% Rows are samples columns are variables
% Group can be defined (patient,batch,tissueType, state)

groupFactor = 'patientID';%'state';%'batchID';%'tissueType';
targetPath = 'G:\Abel\Research\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials';

groupIDs = PD.getStoredSampleCategory(groupFactor);

[matrix,pl,sl] = PD.fetchData;

f = fopen(fullfile(targetPath, ['fullDataGroupedBy_' groupFactor '.csv']),'w');

%Print header
fprintf(f,',group');
for i=1:length(pl)
    fprintf(f,',var_%04d',i);
end
fprintf(f,'\n');

for i=1:length(sl)
    fprintf(f,'Sample_%02d,',i);
    cgid = find(ismember(groupIDs,sl{i}.(groupFactor)));
    fprintf(f,'%d',cgid);
    for j=1:length(pl)
        if ~isnan(matrix(j,i))
            fprintf(f,',%f',matrix(j,i));
        else
            fprintf(f,', ');
        end
    end
    fprintf(f,'\n');
end

fclose(f);



