function f = plotColumnHist(mat,colLab)
%Designed for the gene data
f = figure('units','normalized','outerposition',[0 0 1 1]);
nofCol = size(mat,2);
sizeOfPlot = [4,ceil(nofCol/4)];

xmin = min(mat(:));
xmax = max(mat(:));
nofBins = 50;

for i=1:nofCol
    colVal = mat(~isnan(mat(:,i)),i);
    subplot(sizeOfPlot(1),sizeOfPlot(2),i);
    histogram(colVal,nofBins);
    ylim([0,200]);
    xlim([xmin,xmax]);
    if nargin>1
        if isa(colLab{i},'SampleLabel')
            title(colLab{i}.print());
        else
            title(colLab{i});
        end
    end
end


end

