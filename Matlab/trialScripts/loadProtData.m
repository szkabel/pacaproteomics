%% Load in matrices for the first session

%Load in values
statMatrix = table2array(T_statResults(:,1:19));
[statMatrix0,statMatrixWithoutNans,~,~,statMatrixColMean] = removeNaNs(statMatrix);
%The reorgStat matrix contains the columns organized by patiens rather than
%the tissue types
reorgStat = statMatrix(:,[1 6 16 11, 2 7 17 12, 3 8 13, 4 9 18 14, 5 10 19 15]);

genesWithAbundance = T_noAbundance.Accession;

%for WONan as well
nanPlaceMatrix = isnan(statMatrix);
genesWithAbundanceWONan = T_noAbundance.Accession(~any(nanPlaceMatrix,2));

%Healthy stroma
HS_idx = 26:30;
%Neoplastic (cancerous) stroma
NS_idx = 31:34;
%Healthy Acinar (Pancreas)
HA_idx = 20:24;
%Neoplastic (cacnerous) parenchyma (Pancreas)
NP_idx = 15:19;
propSampleIdx = sort(horzcat(HS_idx,NS_idx,HA_idx,NP_idx));

abuMatrix = table2array(T_noAbundance(:,propSampleIdx));
abuLogMatrix = log(abuMatrix);
abuLogMatrix0 = removeNaNs(abuLogMatrix);

abuLog2Matrix = log2(abuMatrix);

abuLog2MatrixCentered = abuLog2Matrix - nanmedian(abuLog2Matrix);
[abuLog2MatrixCentered0,abuLog2MatrixCenteredWithoutNans] = removeNaNs(abuLog2MatrixCentered);

% Alternative normalization (first dividing by the sum)
abuSumDivLog = log(abuMatrix./nansum(abuMatrix,1));
[~,abuSumDivLogWithoutNaNs] = removeNaNs(abuSumDivLog);

%Load ends

%% Recreating T-test difference also for those where it is missing...

NP_statIdx = 1:5;
HA_statIdx = 6:10;
HS_statIdx = 11:15;
NS_statIdx = 16:19;
