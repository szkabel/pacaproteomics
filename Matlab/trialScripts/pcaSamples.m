function [scoresPCA,coeffsPCA] = pcaSamples(matrixToBePCAd,putLegend)
%Specific PCA code for the proteomics data
%   matrixToBePCAd, columns as observations rows as variables
%   putLegend bool whether to put legend

[coeffsPCA,scoresPCA,latent] = pca(matrixToBePCAd','Algorithm','eig');

dims = [1 2 3];
markerSize = 12;

%rowIdxToPlotStat = {1:9,10:18,19:27,28:36};
rowIdxToPlotStat = {1:5,6:10,11:15,16:19};
%meaningOfStat = {'NP','HP','NS','HS'};
meaningOfStat = {'NP','HP','HS','NS'};
cols = [1 0 0; 0 1 0; 0 0 1; .75 .75 0; .75 0 .75; 0 .75 .75 ];
%patientID = {6:14,6:14,6:14,6:14};
patientID = {1:5,1:5,1:5,[1 2 4 5]};
patientMarks = 'o+*xsdv><pho+*xsdv><ph';
legString = cell(size(matrixToBePCAd,2),1);
k = 1;
for i=1:length(rowIdxToPlotStat)
    for j=1:length(patientID{i})
        currPatient = patientID{i}(j);
        if length(dims) == 2
            plot(scoresPCA(rowIdxToPlotStat{i}(j),dims(1)),scoresPCA(rowIdxToPlotStat{i}(j),dims(2)),patientMarks(currPatient),'Color',cols(i,:),'MarkerSize',markerSize);
        elseif length(dims) == 3
            plot3(scoresPCA(rowIdxToPlotStat{i}(j),dims(1)),scoresPCA(rowIdxToPlotStat{i}(j),dims(2)),scoresPCA(rowIdxToPlotStat{i}(j),dims(3)),patientMarks(currPatient),'Color',cols(i,:),'MarkerSize',markerSize);
        end
        hold on;
        grid on;
        legString{k} = [meaningOfStat{i} '-' num2str(currPatient)];
        k = k + 1;
    end
end
i = 1;
xlabel(sprintf('PCA #%d - %4.2f%%',dims(i),latent(dims(i))/sum(latent)*100));
i = 2;
ylabel(sprintf('PCA #%d - %4.2f%%',dims(i),latent(dims(i))/sum(latent)*100));
if length(dims) == 3
    i = 3;
    zlabel(sprintf('PCA #%d - %4.2f%%',dims(i),latent(dims(i))/sum(latent)*100));
end

if nargin>1 && putLegend
    legend(legString,'Location','best');
end
hold off;

end

