%Script to filter proteins from the protein table

fileID = fopen('G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\allProteinList.txt','r');
listToBeQueried = textscan(fileID,'%s');
listToBeQueried = listToBeQueried{1};
fclose(fileID);

%Get also a list without isoforms (assuming that isoforms)
listToBeQueriedIsoRemoved = listToBeQueried;
for i=1:length(listToBeQueriedIsoRemoved)
    splt = strsplit(listToBeQueriedIsoRemoved{i});
    listToBeQueriedIsoRemoved{i} = splt{1};
end

k = 1;
%There cannot be more edges then n*(n-1)/2
n = length(listToBeQueried);
indicesToBeKept = zeros(n*(n-1)/2,1);

for i=1:size(fullLinkTable,1)
    aliases1 = proteinAliasMap(fullLinkTable.protein1{i});
    aliases2 = proteinAliasMap(fullLinkTable.protein2{i});
    
    %We need both end point to be in the query list
    if ~isempty(ismember(aliases1,listToBeQueried)) && ~isempty(intersect(aliases1,listToBeQueried))
        indicesToBeKept(k) = i;
        k = k + 1;            

    %This else can be removed to NOT match for the isoformremoved string
    elseif ~isempty(intersect(aliases1,listToBeQueriedIsoRemoved)) && ~isempty(intersect(aliases1,listToBeQueriedIsoRemoved))
        indicesToBeKept(k) = i;
        k = k + 1;            
    end    
    if mod(i,1000) == 0, fprintf('%d/%d done ...\n',i,size(fullLinkTable,1)); end
end

indicesToBeKept(k:end) = [];

reducedTable = fullLinkTable(indicesToBeKept,:);


