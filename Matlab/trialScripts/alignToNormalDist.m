function quantNormed = alignToNormalDist(inputM)
%Performs quantile normalization to normal distribution for each column of
%the matrix

mu = 0;
sigm = 2.5; %just because this is how it looks on the result

nofCols = size(inputM,2);

quantNormed = inputM;

for i=1:nofCols
    nofVals = sum(~isnan(inputM(:,i)));
    normVals = normrnd(mu,sigm,1,nofVals);
    sortedNormVals = sort(normVals);
    origIdx = find(~isnan(inputM(:,i)));
    origVals = inputM(origIdx,i);
    [~,sortedOrigIdx] = sort(origVals);
    quantNormed(origIdx(sortedOrigIdx),i) = sortedNormVals;
end

end