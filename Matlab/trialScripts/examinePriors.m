%PD needs to be loaded

% NOTES: gene names are not proper IDs! Hence we need to use Ensemble IDs
% that are available in the set.

%% Load in data and do filtering

T = readtable('G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\prior\DeepProteomeAtlas\PaCaProteome.xlsx');

batchID = 2;

targetDirs = {...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\Output\priorInvestigation\DeepProteomeAtlas';...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\Output\priorInvestigation\DeepProteomeAtlasBatch2';...
};

targetDir = targetDirs{batchID};

savedFilesForBatches = {...    
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\prior\DeepProteomeAtlas\200222_PriorVsOurExtended.mat';...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\ProteomicsData\MyTrials\prior\DeepProteomeAtlas\200323_PriorVsOurBatch2.mat';...
};

% Get ensembl prot names
[ourAbuM,allProtLabels] = PD.fetchData('normType','none','batchIDs',batchID);
[ourAbuMNeo,~] = PD.fetchData('normType','none','states',{'neoplastic'});
[ourAbuMHea,~] = PD.fetchData('normType','none','states',{'healthy'});

nofFound = length(allProtLabels);
ensemblPNameList = cell(1,nofFound);
ourGNameList = cell(1,nofFound);
for i=1:nofFound, ensemblPNameList{i} = allProtLabels{i}.ensemblP; end
for i=1:nofFound, ourGNameList{i} = allProtLabels{i}.humanReadable; end

priorPList = T.ProteinID_representatives_;
priorGList = T.GeneName;
nofPrior = length(priorPList);
for i=1:nofPrior, priorPList{i} = strsplit(priorPList{i},';'); end
for i=1:nofPrior, priorGList{i} = strsplit(priorGList{i},';'); end

% Filter only for those for which the pancreas value is > 0
priorV = T.Pancreas;
foundFilter = priorV > 0;
priorPaPList = priorPList(foundFilter);
priorPaGList = priorGList(foundFilter);
priorPancV = priorV(foundFilter);
nofPaPrior = length(priorPaPList);

%% Calculating matching numbers and matching itself

%Check how unique these are
allEnsIDs = cell(sum(cellfun(@length,priorPaPList)),1);
k = 1;
for i=1:nofPaPrior, for j=1:length(priorPaPList{i}), allEnsIDs{k} = priorPaPList{i}{j}; k = k+1; end; end
fprintf('The number of Ensemble P IDs in the prior set is: %d\n',k-1);
fprintf('composed of %d unique values.\n',length(unique(allEnsIDs)));
%NIIICE, THERE ARE NO OVERLAPS THERE!
fprintf('If the above 2 is equal then at least in the PRIOR there are NO duplicate ENSP ids.\n\n');

%[priorPaPMatches,ourPaPMatches] = advancedAndSlowIntersect(priorPaPList,ensemblPNameList);
%[priorPMatches,ourPMatches] = advancedAndSlowIntersect(priorPList,ensemblPNameList);
load(savedFilesForBatches{batchID});

%Sanity check here for seeing if there were matches to/from multiple
%entires. If NOT then there results should display 0 1
fprintf('If the matching was faithful with Pancreas, then the following variables contains only 0 and 1\n');
disp(unique(cellfun(@length,priorPaPMatches)));
disp(unique(cellfun(@length,ourPaPMatches  )));

fprintf('If the matching was faithful with full proteome, then the following variables contains only 0 and 1\n');
disp(unique(cellfun(@length,priorPMatches)));
disp(unique(cellfun(@length,ourPMatches  )));

fprintf('\n***************************\n')
fprintf('After examining the data the matching should NOT be faithful.\n');
fprintf('The total number of proteins NOT uniquely matched in PRIOR')

%Seemingly the ENSP databse AND the PRIOR info is more consistent. Let's
%use those values for identifying the matches.
    
% First check: does the protein matching works - seemingly yes...?
fprintf('\nThe number of matched proteins (to our findings) in PANCREATIC PRIOR is %d/%d\n',...
    sum(~cellfun(@isempty,priorPaPMatches)),nofPaPrior...
);  
fprintf('\nThe number of matched proteins (with pancreatic prior) in OUR DATA is %d/%d\n',...
    sum(~cellfun(@isempty,ourPaPMatches)),nofFound...
);

fprintf('\nThe number of matched proteins (to our findings) in FULL PRIOR is %d/%d\n',...
    sum(~cellfun(@isempty,priorPMatches)),nofPrior...
);
fprintf('\nThe number of matched proteins (with full prior) in OUR DATA is %d/%d\n',...
    sum(~cellfun(@isempty,ourPMatches)),nofFound...
);

%% Checkup on the missing proteins
fprintf('\n***************************\n')

notInHealthyBoolIdx = cellfun(@isempty,ourPMatches); %Seems to be really missed by checking 3 of them also by UniProt and Ensemble they were not found.
% This is the list of proteins NOT found in the healthy human proteome!
writeToFile(notInHealthyBoolIdx,allProtLabels,targetDir,'listOfProteinsNOTInHealthy-noIso.txt','uniProtWOisoform');
writeToFile(notInHealthyBoolIdx,allProtLabels,targetDir,'listOfProteinsNOTInHealthy.txt','uniProtAccession');

fprintf('The number of proteins not present in healthy proteome at ALL is: %d\n',sum(notInHealthyBoolIdx));
% A bit more elaboration what is the reason for these:
% How many does not have because the entry doesn't have an ENSP value?
[nofMissingENSP,enspMissIdxList] = calcNofMissingENSPs(allProtLabels,notInHealthyBoolIdx);
fprintf('out of which %d does NOT have ENSP ID.\n',nofMissingENSP);
%Match by geneNames:
fprintf('Matching these outliers by genes...\n')
outlierGList = ourGNameList(notInHealthyBoolIdx); %Outlier refers to not found by EnsemblP at all
outlierIdx = find(notInHealthyBoolIdx);
[priorGMatch,outlierGMatch] = advancedAndSlowIntersect(priorGList,outlierGList);
fprintf('The number of matched gene names in OUR outliers to all prior is %d/%d\n',sum(~cellfun(@isempty,outlierGMatch)),sum(notInHealthyBoolIdx));
fprintf('The number of matched gene names in ALL prior to our outliers is %d\n',sum(~cellfun(@isempty,priorGMatch)));
[priorPaGMatch,outlierPaGMatch] = advancedAndSlowIntersect(priorPaGList,outlierGList);
fprintf('The number of matched gene names in OUR outliers to Pancreatic prior is %d/%d\n',sum(~cellfun(@isempty,outlierPaGMatch)),sum(notInHealthyBoolIdx));
fprintf('The number of matched gene names in PANCREATIC prior to our outliers is %d\n',sum(~cellfun(@isempty,priorPaGMatch)));

notFoundAtAll = false(length(notInHealthyBoolIdx),1);
notFoundAtAll(outlierIdx(cellfun(@isempty,outlierGMatch))) = true;

foundInPaCaByGene = false(length(notInHealthyBoolIdx),1);
foundInPaCaByGene(outlierIdx(~cellfun(@isempty,outlierPaGMatch))) = true;

foundOnlyInHealhtyByGene = false(length(notInHealthyBoolIdx),1);
foundOnlyInHealhtyByGene(outlierIdx(~( ~cellfun(@isempty,outlierPaGMatch) | cellfun(@isempty,outlierGMatch) ))) = true;

writeToFile(notFoundAtAll,allProtLabels,targetDir,'listOfProteinsNotFoundAtAll-noIso.txt','uniProtWOisoform');
writeToFile(notFoundAtAll,allProtLabels,targetDir,'listOfProteinsNotFoundAtAll.txt','uniProtWOisoform');
writeToFile(foundInPaCaByGene,allProtLabels,targetDir,'listOfProteinsFoundByGeneInPaca-noIso.txt','uniProtWOisoform');
writeToFile(foundInPaCaByGene,allProtLabels,targetDir,'listOfProteinsFoundByGeneInPaca.txt','uniProtWOisoform');
writeToFile(foundOnlyInHealhtyByGene,allProtLabels,targetDir,'listOfProteinsFoundOnlyInHealhtyByGene-noIso.txt','uniProtWOisoform');
writeToFile(foundOnlyInHealhtyByGene,allProtLabels,targetDir,'listOfProteinsFoundOnlyInHealhtyByGene.txt','uniProtWOisoform');

fprintf('\nThe number of ENSP misses in the not found at all set is: %d\n',sum(notFoundAtAll & enspMissIdxList));
fprintf('in the found only in healthy set is: %d\n',sum(foundOnlyInHealhtyByGene & enspMissIdxList));
fprintf('in the found in PaCa by gene set is: %d\n',sum(foundInPaCaByGene & enspMissIdxList));

notInHealthyPancBoolIdx = cellfun(@isempty,ourPaPMatches) & ~cellfun(@isempty,ourPMatches);
% This is the list of proteins NOT found in the healthy pancreas human
% proteome! (but found in the healthy otherwise)
writeToFile(notInHealthyPancBoolIdx,allProtLabels,targetDir,'listOfProteinsNOTInHealthyPancreas-noIso.txt','uniProtWOisoform');
writeToFile(notInHealthyPancBoolIdx,allProtLabels,targetDir,'listOfProteinsNOTInHealthyPancreas.txt','uniProtAccession');

fprintf('\n***************************\n')

fprintf('\nCheck also how many of the %d proteins matching only to healthy by ENSP\nmatches to Pancreas by Gene Name...\n',sum(~cellfun(@isempty,ourPMatches)) - sum(~cellfun(@isempty,ourPaPMatches)));
notInHealthyPaGList = ourGNameList(notInHealthyPancBoolIdx);
[~,notInHealthyPaGMatch] = advancedAndSlowIntersect(priorPaGList,notInHealthyPaGList);
fprintf('The number is: %d\n',sum(~cellfun(@isempty,notInHealthyPaGMatch)));
notInHealthyPancIdx = find(notInHealthyPancBoolIdx);
stillInHealthyPaByGeneIdx = false(length(notInHealthyPancBoolIdx),1);
stillInHealthyPaByGeneIdx(notInHealthyPancIdx(~cellfun(@isempty,notInHealthyPaGMatch))) = true;
writeToFile(stillInHealthyPaByGeneIdx,allProtLabels,targetDir,'listOfProteinsStillInHealthyPancreasByGene-noIso.txt','uniProtWOisoform');
writeToFile(stillInHealthyPaByGeneIdx,allProtLabels,targetDir,'listOfProteinsStillInHealthyPancreasByGene.txt','uniProtAccession');
writeToFile(notInHealthyPancBoolIdx & ~stillInHealthyPaByGeneIdx,...
    allProtLabels,targetDir,'listOfProteinsNOTInHealthyPancreasEvenByGene-noIso.txt','uniProtWOisoform');
writeToFile(notInHealthyPancBoolIdx & ~stillInHealthyPaByGeneIdx,...
    allProtLabels,targetDir,'listOfProteinsNOTInHealthyPancreasEvenByGene.txt','uniProtAccession');


fprintf('\n***************************\n')

%Ids with no abundance in our data:
noAbuIdx = all(isnan(ourAbuM),2);
fprintf('\nThe number of genes in our data without abundance is: %d\n',sum(noAbuIdx));
fprintf('The number of these in PaCa proteome is: %d\n',sum(~cellfun(@isempty,ourPaPMatches) & noAbuIdx) );
fprintf('The number of these in full proteome is: %d\n',sum(~cellfun(@isempty,ourPMatches) & noAbuIdx));

%% Checking the distributions
% FROM THIS POINT MOVE ON WITH PANCREAS ONLY!
%NOTE: KEEP the order ALL the time and use index arrays!

colorPriPanc = [237 125 49]*0.8/255;
colorOurs = [169 209 142]*0.8/255;
colorLeftOut = [0 176 240]/255;

[~,sortedPriorPaCaIdx] = sort(priorPancV);
x = 1:length(priorPancV);
Y = zeros(5,length(priorPancV));
Y(1,:) = priorPancV(sortedPriorPaCaIdx);

for i=1:nofPaPrior
    if ~isempty(priorPaPMatches{sortedPriorPaCaIdx(i)})
        currAbuM = ourAbuM(priorPaPMatches{sortedPriorPaCaIdx(i)},:);
        currAbuMHea = ourAbuMHea(priorPaPMatches{sortedPriorPaCaIdx(i)},:);
        currAbuMNeo = ourAbuMNeo(priorPaPMatches{sortedPriorPaCaIdx(i)},:);
                        
        Y(2,i) = nanmedian(currAbuM(:));
        Y(3,i) = sum(~isnan(currAbuM(:)));
        Y(4,i) = nanmedian(currAbuMHea(:));
        Y(5,i) = nanmedian(currAbuMNeo(:));
        
    end    
end

 % First, plot the abundances next to each other
figure('units','normalized','outerposition',[0 0.25 1 0.75]);
bar(x,Y(1:2,:)');
legend({'Prior','Median abu in our'})
set(gca, 'YScale', 'log');

figure('units','normalized','outerposition',[0 0.25 1 0.75]);
plot(x,Y(1,:),'*','Color',colorPriPanc); hold on;
plot(x,Y(2,:),'*','Color',colorOurs);
set(gca, 'YScale', 'log');
legend({'Prior','Our proteins'})
hold off;

figure('units','normalized','outerposition',[0 0.25 1 0.75]);
plot(x,Y(1,:),'*','Color',colorPriPanc); hold on;
plot(x,Y(4,:),'*','Color',colorOurs*1.2);
plot(x,Y(5,:),'*','Color',colorOurs*0.5);
set(gca, 'YScale', 'log');
legend({'Prior','Our Healthy','Our Neoplastic'})
hold off;

% Distribution analysis by histogram

%Calculate the number of proteins found in our data for a given bin
nofBins = 50;
[histCount,binEdges,priorGeneBinIdx] = histcounts(log2(priorPancV(sortedPriorPaCaIdx)),nofBins); %Sorting is needed here because Y contains them in sorted order

nofFoundProtsForBin = zeros(2,nofBins);
nofFoundProtsForBin(1,:) = histCount;
for i=1:nofBins    
    nofFoundProtsForBin(2,i) = sum(Y(2,priorGeneBinIdx == i) ~= 0);
end

figure('units','normalized','outerposition',[0 0.25 1 0.75]);
barObject = bar(nofFoundProtsForBin','FaceColor','flat');
barObject(1).CData = repmat(colorPriPanc,nofBins,1);
barObject(2).CData = repmat(colorOurs,nofBins,1);
ylabel('# proteins')
xlabel('abundancy bins (in log2 space)')
legend({'Number of proteins in prior proteome','# of them found'});
title('Protein abundancy histogram based on Prior information');

%% Check missed values location in OUR histogram

nofBins = 100;
[histCountOur,ourBinEdges] = histcounts(log2(ourAbuM(:)),nofBins);
figure('units','normalized','outerposition',[0 0.25 1 0.75]);
bar(histCountOur); xlabel('abundancy bins (log2)'); ylabel('# proteins')
title('Joint protein abundancy histogram of our results');

% Go through on interesting cases:
%No1: true outliers, not even found be gene names
createHighlightedHisoPlots(nofBins,ourBinEdges,notFoundAtAll,ourAbuM,'Not found at all',histCountOur,colorOurs,colorLeftOut);

%No2: all, not in pancreas even by gene
createHighlightedHisoPlots(nofBins,ourBinEdges,notFoundAtAll | (notInHealthyPancBoolIdx & ~stillInHealthyPaByGeneIdx) | foundOnlyInHealhtyByGene ,ourAbuM,'Not found in healthy pancreas',histCountOur,colorOurs,colorLeftOut);


