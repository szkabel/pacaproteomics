classdef ProteinLabel < handle
    
    properties
        % This is kinda like the PrimaryKey for our protein
        % According to the output from Lund it may contain isoform info
        uniProtAccession
        
        isoformInfo
        uniProtWOisoform
        
        keggID = ProteinLabel.NAstring; % The hsa: sg like code for the gene coding the protein
        keggGeneName = ProteinLabel.NAstring; % The long list of corresponding Kegg Gene Names        
        humanReadable = ProteinLabel.NAstring; % The canonical name as retrieved from UniProt        
        writtenProtName = ProteinLabel.NAstring; % The description of protein name (as also provided by Lund)        
        description = ProteinLabel.NAstring; % Longer text field, uniprot description of the protein, restricted to a single field currently.
        organism = ProteinLabel.NAstring; % The organism
        ensemblP = ProteinLabel.NAstring; % Ensemble seem to have multiple IDs structures (ENST,ENSP and ENSG). Here the P refers to ENSP. Furthermore one uniprot ID can have multiple ENSP values unfortunately.
        ensemblG = ProteinLabel.NAstring; % Ensemble G. Can have multiple entries. BIIIG NOTE! ENSEMBL is written without trailing E (why, no idea)
        stringID = ProteinLabel.NAstring; % String ID, should be rather similar to Ensembl
    end
    
    properties (Access = 'private')
        uniProtText;
    end
    
    properties (Constant)
        NAstring = 'Not queried yet';
        NotInDBString = 'Not found.';
    end
    
    methods
        function obj = ProteinLabel(uniProtAccession)
            %ProteinLabel Construct an instance of this class
            %   It is required to add the uniProtAccession as that is the
            %   Primary Key
            obj.uniProtAccession = uniProtAccession;
            
            if ismember('-',uniProtAccession)
                spltUPA = strsplit(uniProtAccession,'-');
                obj.isoformInfo = spltUPA{2};
                obj.uniProtWOisoform = spltUPA{1};
            else
                obj.isoformInfo = [];
                obj.uniProtWOisoform = uniProtAccession;
            end
        end
        
        function fillInFields(obj,fieldsToFill)
            %Query other info based on the uniprot accession ID
            %fieldsToFill is a cellarray defining which fields to query
            
            if nargin<2 || isempty(fieldsToFill) || any(strcmp(fieldsToFill,'All'))
                fieldsToFill = {'keggID','keggGeneName','humanReadable','writtenProtName','description','organism','ensemblG','ensemblP','stringID'};
            end                        
            
            import matlab.net.*
            import matlab.net.http.*
            r = RequestMessage;
            
            %Seemingly the uniprot page contains most of this info, hence
            %it is a wise thing to query that first in total anyway, and
            %then parse it as required.
            
            if isempty(obj.uniProtText)
                %Luckily this works with isoforms as well
                uri = URI(sprintf('http://www.uniprot.org/uniprot/%s.txt',obj.uniProtAccession));
                resp = send(r,uri);            

                if length(resp.Body.Data.char)>1
                    obj.uniProtText = strsplit(resp.Body.Data,'\n')';
                else        
                    fprintf('Empty message in UniProt for %s',obj.uniProtAccession);
                    obj.uniProtText = [];
                end
            end
            
            % Do here the queries one-by-one
            
            %KEGG ID (and if KEGG gene name is queried as well, as kegg gene name is queried with that)
            if ismember('keggID',fieldsToFill) || ismember('keggGeneName',fieldsToFill)
                lineIdx = startsWith(obj.uniProtText,'DR   KEGG');
                if any(lineIdx)
                    if sum(lineIdx)>1, lineIdx = find(lineIdx,1,'first'); end %There may be multiple KEGG entries connected to the same uniprot. Keep in this case the first one.
                    spltBySemiColon = strsplit(obj.uniProtText{lineIdx},';');
                    obj.keggID = strtrim(spltBySemiColon{2});

                    if ismember('keggGeneName',fieldsToFill)
                        uri = URI(sprintf('http://rest.kegg.jp/list/%s',obj.keggID));
                        resp = send(r,uri);
                        if ~isempty(resp.Body.Data)
                            byField = strsplit(resp.Body.Data,'\t');
                            inField = strsplit(byField{2},';'); %2 as the second column is the uniprot id
                            obj.keggGeneName = inField{1};
                        else
                            obj.keggGeneName = ProteinLabel.NotInDBString;
                        end
                    end
                else
                    obj.keggID = ProteinLabel.NotInDBString;
                    if ismember('keggGeneName',fieldsToFill)
                        obj.keggGeneName = ProteinLabel.NotInDBString;
                    end
                end
            end
            
            if ismember('humanReadable',fieldsToFill)
                lineIdx = startsWith(obj.uniProtText,'GN   Name');
                if sum(lineIdx)>1, lineIdx = find(lineIdx,1,'first'); end %There may be multiple used gene names connected to same entry as well.
                spltBySemiColon = strsplit(obj.uniProtText{lineIdx},';');
                %At some point I've seen that some extra info is provided,
                %which is disturbing. Keep only the first word before
                %space.
                firstSpaceIdx = find(spltBySemiColon{1}(11:end) == ' ',1);
                if firstSpaceIdx>0
                    obj.humanReadable = strtrim(spltBySemiColon{1}(11:11+firstSpaceIdx-1));
                else
                    obj.humanReadable = strtrim(spltBySemiColon{1}(11:end));
                end
            end
            
            if ismember('writtenProtName',fieldsToFill)
                lineIdx = startsWith(obj.uniProtText,'DE   RecName:');
                spltByColon = strsplit(obj.uniProtText{lineIdx},':');
                obj.writtenProtName = spltByColon{2}(7:end-1);
            end
            
            if ismember('description',fieldsToFill)
                firstLineIdx = find(startsWith(obj.uniProtText,'CC   -!- FUNCTION:'));
                if ~isempty(firstLineIdx)
                    ccLinePos = find(startsWith(obj.uniProtText,'CC   -!- '));
                    lastLineIdxs = ccLinePos(ccLinePos > max(firstLineIdx));
                    if isempty(lastLineIdxs)
                        lastLineIdxs = find(startsWith(obj.uniProtText,'CC   '),1,'last');
                    end
                    lastLineIdx = lastLineIdxs(1);
                    descLines = obj.uniProtText(min(firstLineIdx):lastLineIdx-1);
                    for j=1:length(descLines), descLines{j} = descLines{j}(10:end); end                                                
                    obj.description = strjoin(descLines,' ');
                else
                    obj.description = ProteinLabel.NotInDBString;
                end
            end
            
            if ismember('organism',fieldsToFill)
                lineIdx = startsWith(obj.uniProtText,'OS   ');                
                obj.organism = obj.uniProtText{lineIdx}(6:end-1);
            end
            
            %Matching for ensembles
            ensemblMatchingTable = {...
                'ensemblP',3,'ENSP';...
                'ensemblG',4,'ENSG'...
                };
            for i = 1:size(ensemblMatchingTable)
                if ismember(ensemblMatchingTable{i,1},fieldsToFill)
                    lineIdxs = find(startsWith(obj.uniProtText,'DR   Ensembl;'));
                    if isempty(lineIdxs)
                        obj.(ensemblMatchingTable{i,1}) = ProteinLabel.NotInDBString;                     
                    else                        
                        %NOTE sometimes isoform info is connected to the
                        %Ensembl IDs.
                        obj.(ensemblMatchingTable{i,1}) = cell(1,length(lineIdxs));
                        kk = 1;
                        for j=1:length(lineIdxs)
                            %Find the last dot in the text
                            lastDotLoc = find(obj.uniProtText{lineIdxs(j)} == '.',1,'last');
                            ensID = strtrim(strsplitN(obj.uniProtText{lineIdxs(j)}(1:lastDotLoc-1),';',ensemblMatchingTable{i,2}));
                            %Double check if this is really ENSP
                            if ~strcmp(ensID(1:4),ensemblMatchingTable{i,3}), fprintf('SPLIT ERROR IN %s\n',obj.uniProtAccession); end
                            %Check for isoform info:
                            if lastDotLoc ~= length(obj.uniProtText{lineIdxs(j)}) && ~isempty(obj.isoformInfo)
                                isoID = strsplitN(obj.uniProtText{lineIdxs(j)}(lastDotLoc+2:end-1),'-',2);
                                if strcmp(isoID,obj.isoformInfo)
                                    obj.(ensemblMatchingTable{i,1}){kk} = ensID;
                                    kk = kk + 1;
                                end
                            else
                                obj.(ensemblMatchingTable{i,1}){kk} = ensID;
                                kk = kk + 1;
                            end
                        end
                        obj.(ensemblMatchingTable{i,1})(kk:end) = [];
                        %Wrap it out if cell is not needed.
                        if length(obj.(ensemblMatchingTable{i,1})) == 1
                            obj.(ensemblMatchingTable{i,1}) = obj.(ensemblMatchingTable{i,1}){1};
                        end
                    end
                end
            end
            
            if ismember('stringID',fieldsToFill)
                lineIdx = startsWith(obj.uniProtText,'DR   STRING');
                if any(lineIdx)
                    obj.stringID = strtrim(strsplitN(obj.uniProtText{lineIdx},';',2));
                else
                    obj.stringID = ProteinLabel.NotInDBString;
                end
            end
            
            
        end
                
    end
    
    methods (Static)
        function listOfProtProperties = queryPublicFields()
            metaProtLabel = ?ProteinLabel;
            listOfProtProperties = {metaProtLabel.PropertyList.Name};
            listOfProtProperties = listOfProtProperties(strcmp({metaProtLabel.PropertyList.GetAccess},'public') & ~[metaProtLabel.PropertyList.Constant]);
        end
    end
end
